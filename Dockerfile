FROM node:6.3.1
MAINTAINER pipetc@gmail.com

RUN apt-get update && apt-get install -y \
  nginx git\
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

ENV work_dir /app

WORKDIR ${work_dir}

COPY ./docker/nginx.conf /etc/nginx/sites-enabled/default

ADD . .

RUN npm install --silent
RUN npm run build

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
