#!/bin/sh
#  {project name} {web port}

if [ -z "$1" ]
  then
    echo "No project name supplied"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Port not defined"
    exit 1
fi

PROJECT=$1
APIIMAGE=ndp-api
WEBIMAGE=ndp-web
# Containers names
APICN=ndp-$PROJECT-api
WEBCN=ndp-$PROJECT-web

# Containers ports
WEBPORT=$2

# Postgres connection
DBCN=postgres
DB=postgis://postgres:ololo@$DBCN/ndp_api_$PROJECT

# Remove containers and update images
docker kill $WEBCN
docker kill $APICN
docker rm  $WEBCN
docker rm  $APICN
docker pull $WEBIMAGE
docker pull $APIIMAGE

# Run API
docker run --name $APICN -d -v /data/$APICN/media:/env/www/media --link $DBCN -e DATABASE_URL=$DB $APIIMAGE
# Run WEB
docker run --name $WEBCN -d -p $WEBPORT:80 --link $APICN:api $WEBIMAGE
