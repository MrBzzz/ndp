module.exports = {
  port: 3000,
  publicPath: '/',
  title: 'НДП',
  description: 'НДП',
  api: 'http://128.199.36.32:8000',
  url: {
    login: '/login/',
    logout: '/logout/',
    api: '/api/',
    media: '/media/',
    register: '/register/',
    static: '/static/'
  }
};
