import React, { PropTypes, Component } from 'react';
import DropZone from 'react-dropzone';
import './AddFiles.less';

class AddFiles extends Component {

  constructor() {
    super();
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(files) {
    this.props.onDropFiles(files);
  }

  render() {
    if (!this.props.show) return null;
    return (
      <DropZone ref="dropzone" className="add-images" accept=".jpeg, .jpg, .png, .mp4" onDrop={this.onDrop}>+</DropZone>
    );
  }
}

AddFiles.propTypes = {
  onDropFiles: PropTypes.func,
  uploadRevisionMedia: PropTypes.func,
  show: PropTypes.bool,
  revision: PropTypes.object
};

export default AddFiles;

