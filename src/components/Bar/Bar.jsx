import React, { PropTypes } from 'react';
import Breadcrumbs from 'components/Breadcrumbs/Breadcrumbs';
import BtnGroup from 'components/BtnGroup/BtnGroup';

const Bar = ({ breadcrumbs = [], buttons = [] }) => (
  <div className="columns is-mobile">
    <div className="column">
      <Breadcrumbs links={breadcrumbs} />
    </div>
    <div className="column is-one-quarter has-text-right">
      <BtnGroup buttons={buttons} />
    </div>
  </div>
);

Bar.propTypes = {
  breadcrumbs: PropTypes.array,
  buttons: PropTypes.array
};

export default Bar;
