import React, { PropTypes } from 'react';

const Breadcrumbs = ({ links }) => {
  const l = links.length;
  return (
    <div className="bread-crumbs">
      {links.map((item, key) => {
        const result = (key + 1 === l) ? [item] : [item, <span> > </span>];
        return result;
      })}
    </div>
  );
};

Breadcrumbs.propTypes = {
  links: PropTypes.array
};

export default Breadcrumbs;
