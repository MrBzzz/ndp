import React, { PropTypes } from 'react';

const BtnGroup = ({ buttons }) => {
  const l = buttons.length;
  return (
    <div>{buttons.map((item, key) => {
      const result = (key + 1 === l) ? [item] : [item, <span>&nbsp;</span>];
      return result;
    })}</div>
  );
};

BtnGroup.propTypes = {
  buttons: PropTypes.array
};

export default BtnGroup;
