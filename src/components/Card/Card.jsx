import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import CutText from 'components/CutText/CutText';
import CardsImage from 'components/CardsImage/CardsImage';
import TileMeta from 'components/TileMeta/TileMeta';
import RevisionDate from 'components/RevisionDate/RevisionDate';
import './Card.less';

const Card = ({ feature }) => (
  <div className="column is-4">
    <Link to={`monument/${feature.id}`}>
      <div className="card">
        <CardsImage images={feature.images} />
        <TileMeta feature={feature} imagesCount={feature.images.length} />
        <div className="card-content">
          <div className="content">
            <div className="name"><CutText text={feature.name} max={22} /></div>
            <RevisionDate feature={feature} />
          </div>
        </div>
      </div>
    </Link>
  </div>
);

Card.propTypes = {
  feature: PropTypes.object
};

export default Card;
