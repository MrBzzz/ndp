import React, { PropTypes } from 'react';
import i18n from 'i18n/translation';
import './CardsImage.less';

const CardsImage = ({ images }) => {
  let src = '';
  if (images.length) {
    const [img] = images;
    src = img.thumbnails.length ? img.thumbnails[2].file : img.file;
  }
  return (
    <div className="card-image">
      <figure className="image" style={{ backgroundImage: `url(${src})` }}>
        {!images.length && <span className="image_no-img-text">{i18n.t('images.placeholdit')}</span>}
      </figure>
    </div>
  );
};

CardsImage.propTypes = {
  images: PropTypes.array
};

export default CardsImage;
