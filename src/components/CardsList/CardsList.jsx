import React, { PropTypes } from 'react';
import { dividedIntoColumns } from 'helpers/utils';
import Card from 'components/Card/Card';
import Message from 'components/Message/Message';
import i18n from 'i18n/translation';
import './CardsList.less';

const CardsList = ({ features, count }) => {
  if (!features.length) return <Message text={i18n.t('monuments.empty')} />;
  const rows = dividedIntoColumns(features, count);
  return (
    <div className="cards">
      {rows.map((cells, rowKey) => (
        <div key={rowKey} className="columns is-mobile">
          {cells.map(({ properties }, keyCell) => (
            <Card key={keyCell} feature={properties} />
          ))}
        </div>
      ))}
    </div>
  );
};

CardsList.propTypes = {
  features: PropTypes.array,
  count: PropTypes.number
};

export default CardsList;
