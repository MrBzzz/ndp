import React, { PropTypes, Component } from 'react';
import Portal from 'react-portal';
import Modal from 'components/Modal/Modal';
import starIconSvg from 'svg/star.svg';
import starOIconSvg from 'svg/star-o.svg';
import i18n from 'i18n/translation';
import './ChangeFileSort.less';

class ChangeFileSort extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.stopPropagation();
    const { media } = this.props;
    this.props.onChangeSortFiles(media);
  }

  render() {
    if (!this.props.show) return null;
    const { media } = this.props;
    const starNode = media.is_first ? (
      <sapn
        className="remove-file__icon"
        dangerouslySetInnerHTML={{ __html: starIconSvg }}
      ></sapn>
    ) : (
      <sapn
        className="remove-file__icon"
        dangerouslySetInnerHTML={{ __html: starOIconSvg }}
      ></sapn>
    );

    const removeIcon = (
      <div className="change-sort-file">{starNode}</div>
    );

    const removeBtn = [
      <button key="btn" className="button is-primary" onClick={this.onClick}>{i18n.t('buttons.change')}</button>
    ];

    const Node = media.is_first ? (
      <div className="change-sort-file">
        {starNode}
      </div>
    ) : (
      <Portal closeOnEsc openByClickOn={removeIcon}>
        <Modal isOpen title={i18n.t('modal.change_sort.title')} text={i18n.t('modal.change_sort.text')} buttons={removeBtn} />
      </Portal>
    );

    return Node;
  }
}

ChangeFileSort.propTypes = {
  show: PropTypes.bool,
  media: PropTypes.object,
  onChangeSortFiles: PropTypes.func,
};

export default ChangeFileSort;
