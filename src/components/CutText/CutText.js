import React, { PropTypes } from 'react';

const CutText = ({ text, max }) => {
  let cutText = text;
  if (text.length > max) cutText = `${cutText.slice(0, max)}...`;
  return (
    <sapn alt={text} title={text}>{cutText}</sapn>
  );
};

CutText.propTypes = {
  text: PropTypes.string,
  max: PropTypes.number
};

export default CutText;
