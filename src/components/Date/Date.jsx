import React, { PropTypes } from 'react';
import m from 'moment';
import 'moment/locale/ru';
import { date as dateFormat } from 'settings/settings.js';

const Date = ({ date }) => <span>{date ? m(date).format(dateFormat) : null}</span>;

Date.propTypes = {
  date: PropTypes.string
};

export default Date;
