import React, { PropTypes } from 'react';
import m from 'moment';
import 'moment/locale/ru';
import { dateTime as dateFormat } from 'settings/settings.js';

const DateTime = ({ dateTime }) => <span>{dateTime ? m(dateTime).format(dateFormat) : ''}</span>;

DateTime.propTypes = {
  dateTime: PropTypes.string
};

export default DateTime;
