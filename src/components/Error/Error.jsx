import React, { PropTypes } from 'react';
import './Error.less';

const Error = ({ error }) => {
  if (!error) return null;
  return <div className="notification is-danger notification__is-small">{error}</div>;
};

Error.propTypes = {
  error: PropTypes.string
};

export default Error;
