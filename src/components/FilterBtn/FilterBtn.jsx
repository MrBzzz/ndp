import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import i18n from 'i18n/translation';

const mapStateToProps = (state) => ({
  filters: state.filters
});

const getQueryFromFilters = (filters) => Object.keys(filters).reduce((cur, key) => {
  const tmp = cur;
  const item = filters[key] ? filters[key] : [];
  tmp[key] = item.map(({ value }) => value);
  return tmp;
}, {});

const FilterBtn = ({ filters, pathname, page }) => {
  const query = filters[page] || {};
  const to = {
    pathname,
    query: getQueryFromFilters(query)
  };
  return (
    <Link to={to} className="button is-primary">{i18n.t('buttons.show')}</Link>
  );
};

FilterBtn.propTypes = {
  page: PropTypes.string.isRequired,
  pathname: PropTypes.string.isRequired,
  filters: PropTypes.object
};

export default connect(mapStateToProps, {})(FilterBtn);
