import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { changeFilter } from 'reducers/filters';
import settings from 'settings/settings.js';

import './FilterDate.less';
import 'react-datepicker/dist/react-datepicker.css';

const mapStateToProps = (state) => ({
  filters: state.filters
});

class FilterDate extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    const { query, type, page } = this.props;
    const value = query[type];
    this.props.changeFilter({
      type,
      page,
      value: value ? [{ value }] : null
    });
  }

  handleChange(date) {
    const { type, page } = this.props;
    const value = date ? date.format(settings.formats.date) : null;
    this.props.changeFilter({
      type,
      page,
      value: value ? [{ value }] : null
    });
  }

  render() {
    const { type, placeholder, filters, page } = this.props;
    const q = filters[page] && filters[page][type];
    const value = q ? moment(q[0].value, settings.formats.date) : null;
    return (
      <div className="column">
        <div className="control">
          <DatePicker
            dateFormat={settings.formats.date}
            selected={value}
            isClearable
            className="input input_date"
            placeholderText={placeholder}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

FilterDate.propTypes = {
  page: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  changeFilter: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  query: PropTypes.object,
  filters: PropTypes.object
};

export default connect(mapStateToProps, {
  changeFilter
})(FilterDate);
