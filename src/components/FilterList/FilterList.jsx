import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import { find } from 'lodash';
import { changeFilter } from 'reducers/filters';
import './FilterList.less';

class FilterList extends Component {
  constructor(props) {
    super(props);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentWillMount() {
    const { query, type, page, list } = this.props;
    const value = find(list, ['value', query[type]]);
    this.props.changeFilter({
      type,
      page,
      value: value ? [value] : null
    });
  }

  handleSelectChange(value) {
    const { type, page } = this.props;
    this.props.changeFilter({
      type,
      page,
      value: value ? [value] : null
    });
  }

  render() {
    const { list, type, placeholder, filters, page } = this.props;
    const [value] = filters[page] && filters[page][type] ? filters[page][type] : [];
    return (
      <div className="column">
        <Select
          name={type}
          options={list}
          value={value}
          onChange={this.handleSelectChange}
          placeholder={placeholder}
        />
      </div>
    );
  }
}

FilterList.propTypes = {
  page: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  changeFilter: PropTypes.func.isRequired,
  filters: PropTypes.object,
  query: PropTypes.object,
  placeholder: PropTypes.string
};

const mapStateToProps = (state) => ({
  filters: state.filters
});

export default connect(mapStateToProps, {
  changeFilter
})(FilterList);
