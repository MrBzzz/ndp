import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import { find, isArray } from 'lodash';
import { changeFilter } from 'reducers/filters';
import './FilterMultiList.less';

class FilterMultiList extends Component {

  constructor(props) {
    super(props);
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentWillMount() {
    const { query, type, list, page } = this.props;
    const valueList = isArray(query[type]) ? query[type] : [query[type]];
    const value = valueList
      .map(val => find(list, ['value', parseInt(val, 10)]))
      .filter(val => val);
    this.props.changeFilter({
      page,
      type,
      value
    });
  }


  handleSelectChange(value) {
    const { type, page } = this.props;
    this.props.changeFilter({
      type,
      page,
      value
    });
  }

  render() {
    const { list, type, placeholder, filters, page } = this.props;
    const value = filters[page] && filters[page][type] || [];

    return (
      <div className="column column-select">
        <Select
          multi
          placeholder={placeholder}
          name={type}
          options={list.sort((a, b) => {
            const nameA = a.label.toUpperCase(); // ignore upper and lowercase
            const nameB = b.label.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            return 0;
          })}
          value={value}
          onChange={this.handleSelectChange}
        />
      </div>
    );
  }
}

FilterMultiList.propTypes = {
  page: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  changeFilter: PropTypes.func.isRequired,
  filters: PropTypes.object,
  query: PropTypes.object,
  placeholder: PropTypes.string
};

const mapStateToProps = (state) => ({
  filters: state.filters
});

export default connect(mapStateToProps, {
  changeFilter
})(FilterMultiList);
