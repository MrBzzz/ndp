import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeFilter } from 'reducers/filters';
import './FilterSearch.less';

const mapStateToProps = (state) => ({
  filters: state.filters
});

class FilterSearch extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    const { query, type, page } = this.props;
    const value = query[type];
    this.props.changeFilter({
      type,
      page,
      value: value ? [{ value }] : null
    });
  }

  handleChange() {
    const { type, page } = this.props;
    const value = this.refs.input.value;
    this.props.changeFilter({
      type,
      page,
      value: value ? [{ value }] : null
    });
  }

  render() {
    const { type, placeholder, filters, page } = this.props;
    const q = filters[page] && filters[page][type];
    const value = q ? q[0].value : '';
    return (
      <div className="column">
        <p className="control">
          <input
            type="text"
            ref="input"
            className="input input_search"
            value={value}
            name={type}
            onChange={this.handleChange}
            placeholder={placeholder}
          />
        </p>
      </div>
    );
  }
}

FilterSearch.propTypes = {
  page: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  changeFilter: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  query: PropTypes.object,
  filters: PropTypes.object
};

export default connect(mapStateToProps, {
  changeFilter
})(FilterSearch);
