import React, { PropTypes } from 'react';
import FilterList from 'components/FilterList/FilterList';
import FilterMultiList from 'components/FilterMultiList/FilterMultiList';
import FilterSearch from 'components/FilterSearch/FilterSearch';
import FilterBtn from 'components/FilterBtn/FilterBtn';
import i18n from 'i18n/translation';
import './FiltersMonuments.less';

const Filters = ({ filters, query, buttons }) => (
  <div className="filters">
    <div className="columns is-12">
      <FilterSearch
        type="q"
        page="monument"
        label={i18n.t('filter.labels.search')}
        placeholder={i18n.t('filter.placeholders.search')}
        query={query}
      />
    </div>
    <div className="columns">
      <FilterMultiList
        type="kind__in"
        page="monument"
        list={filters.kind}
        label={i18n.t('filter.labels.kind')}
        placeholder={i18n.t('filter.placeholders.kind')}
        query={query}
      />
      <FilterMultiList
        type="current_state__status__in"
        page="monument"
        list={filters.revision_status}
        label={i18n.t('filter.labels.revision_status')}
        placeholder={i18n.t('filter.placeholders.revision_status')}
        query={query}
      />
      <FilterMultiList
        type="location__in"
        page="monument"
        list={filters.location}
        label={i18n.t('filter.labels.location')}
        placeholder={i18n.t('filter.placeholders.location')}
        query={query}
      />
      <FilterList
        type="is_published_on_geoportal"
        page="monument"
        list={filters.is_published_on_geoportal}
        label={i18n.t('filter.labels.geoportal')}
        placeholder={i18n.t('filter.placeholders.geoportal')}
        query={query}
      />
    </div>
    <div className="columns has-text-right">
      <div className="column buttons">
        {buttons}
        <FilterBtn pathname="/" page="monument" />
      </div>
    </div>
  </div>
);

Filters.propTypes = {
  filters: PropTypes.object,
  query: PropTypes.object,
  buttons: PropTypes.array
};

export default Filters;
