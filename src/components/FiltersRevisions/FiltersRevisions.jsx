import React, { PropTypes } from 'react';
import FilterSearch from 'components/FilterSearch/FilterSearch';
import FilterMultiList from 'components/FilterMultiList/FilterMultiList';
import FilterDate from 'components/FilterDate/FilterDate';
import FilterBtn from 'components/FilterBtn/FilterBtn';
import i18n from 'i18n/translation';

const FiltersRevisions = ({ filters, query }) => (
  <div className="filters">
    <div className="columns">
      <FilterSearch
        type="q"
        page="revision"
        label={i18n.t('filter.labels.search')}
        placeholder={i18n.t('filter.placeholders.search')}
        query={query}
      />
      <FilterMultiList
        type="status__in"
        page="revision"
        list={filters.status}
        label={i18n.t('filter.labels.status')}
        placeholder={i18n.t('filter.placeholders.status')}
        query={query}
      />
      <FilterMultiList
        type="monument_status__in"
        page="revision"
        list={filters.monument_status}
        label={i18n.t('filter.labels.monument_status')}
        placeholder={i18n.t('filter.placeholders.monument_status')}
        query={query}
      />
    </div>
    <div className="columns">
      <FilterDate
        type="updated__gte"
        page="revision"
        label={i18n.t('filter.labels.from')}
        placeholder={i18n.t('filter.placeholders.from')}
        query={query}
      />
      <FilterDate
        type="updated__lte"
        page="revision"
        label={i18n.t('filter.labels.to')}
        placeholder={i18n.t('filter.placeholders.to')}
        query={query}
      />
      <FilterMultiList
        type="assigned_user__in"
        page="revision"
        list={filters.user}
        label={i18n.t('filter.labels.user')}
        placeholder={i18n.t('filter.placeholders.user')}
        query={query}
      />
    </div>
    <div className="columns has-text-right">
      <div className="column buttons">
        <FilterBtn pathname="/revisions/" page="revision" />
      </div>
    </div>
  </div>
);

FiltersRevisions.propTypes = {
  filters: PropTypes.object,
  query: PropTypes.object
};

export default FiltersRevisions;
