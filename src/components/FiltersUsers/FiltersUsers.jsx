import React, { PropTypes } from 'react';
import FilterSearch from 'components/FilterSearch/FilterSearch';
import FilterMultiList from 'components/FilterMultiList/FilterMultiList';
import FilterList from 'components/FilterList/FilterList';
import FilterBtn from 'components/FilterBtn/FilterBtn';
import i18n from 'i18n/translation';

const FiltersUsers = ({ filters, query }) => (
  <div className="filters">
    <div className="columns">
      <FilterSearch
        type="q"
        page="users"
        label={i18n.t('filter.labels.search')}
        placeholder={i18n.t('filter.placeholders.search')}
        query={query}
      />
    </div>
    <div className="columns">
      <FilterMultiList
        type="location__in"
        page="users"
        list={filters.locations}
        label={i18n.t('filter.labels.location')}
        placeholder={i18n.t('filter.placeholders.location')}
        query={query}
      />
      <FilterMultiList
        type="type__in"
        page="users"
        list={filters.types}
        label={i18n.t('filter.labels.user_kind')}
        placeholder={i18n.t('filter.placeholders.user_kind')}
        query={query}
      />
      <FilterList
        type="is_active"
        page="users"
        list={filters.is_active}
        label={i18n.t('filter.labels.is_active')}
        placeholder={i18n.t('filter.placeholders.is_active')}
        query={query}
      />
    </div>
    <div className="columns has-text-right">
      <div className="column buttons">
        <FilterBtn pathname="/users/" page="users" />
      </div>
    </div>
  </div>
);

FiltersUsers.propTypes = {
  filters: PropTypes.object,
  query: PropTypes.object
};

export default FiltersUsers;
