import React, { PropTypes } from 'react';
import Select from 'react-select';
import i18n from 'i18n/translation';
import classNames from 'classnames';


const FldBoolChoices = ({ field, name, label, placeholder, settings, isUpdating = false }) => {
  const choices = [
    {
      value: 0,
      label: i18n.t('fld.bool.no')
    },
    {
      value: 1,
      label: i18n.t('fld.bool.yes')
    }
  ];
  const labelCls = classNames('label', {
    strong: settings.is_required
  });
  return (
    <div className="control">
      <label className={labelCls}>{label}</label>
      <Select
        {...field}
        value={field.value}
        name={name}
        searchable
        className={'form-control'}
        placeholder={placeholder}
        options={choices}
        onBlur={() => field.onBlur({ value: field.value })}
        disabled={isUpdating || settings.readable}
      />
    </div>
  );
};

FldBoolChoices.propTypes = {
  isUpdating: PropTypes.bool,
  settings: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

export default FldBoolChoices;
