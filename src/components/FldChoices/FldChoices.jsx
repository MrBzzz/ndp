import React, { PropTypes } from 'react';
import Select from 'react-select';
import classNames from 'classnames';


const FldSelect = ({ field, name, options, label, placeholder, settings, isUpdating = false, multi = false }) => {
  const labelCls = classNames('label', {
    strong: settings.is_required
  });
  let classNameControl = 'control';

  if (field.error) {
    classNameControl += ' is-danger';
  }

  return (
    <div className={classNameControl}>
      <label className={labelCls}>{label}</label>
      <Select
        multi={multi}
        {...field}
        value={field.value}
        name={name}
        searchable
        className={'form-control'}
        placeholder={placeholder}
        options={options}
        onBlur={() => field.onBlur({ value: field.value })}
        disabled={isUpdating || settings.readable}
      />
    </div>
  );
};

FldSelect.propTypes = {
  multi: PropTypes.bool,
  isUpdating: PropTypes.bool,
  settings: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

export default FldSelect;
