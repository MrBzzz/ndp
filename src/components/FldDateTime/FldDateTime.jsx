import React, { PropTypes } from 'react';


const FldDateTime = ({ name, field, label, placeholder, settings, disabled = false }) => (
  <div className="control">
    <label className="label">{label}</label>
    <input
      {...field}
      className="input"
      type="text"
      name={name}
      placeholder={placeholder}
      disabled={disabled || settings.is_readable}
    />
  </div>
);


FldDateTime.propTypes = {
  disabled: PropTypes.bool,
  settings: PropTypes.object,
  field: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

export default FldDateTime;
