import React, { PropTypes, Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { isNaN } from 'lodash';
import L from 'leaflet';
import 'leaflet-sleep';
import filesSvg from 'svg/files-o.svg';
import { mapSettings, tileUrl, iconSettings } from 'settings/settings.js';
import Portal from 'react-portal';
import Modal from 'components/Modal/Modal';
import './FldGeometry.less';
import { moveRevisionCoordinate } from 'reducers/revisions';

const icon = new L.Icon(iconSettings);

class FldGeometry extends Component {

  constructor(props) {
    super(props);
    this.onClickMap = this.onClickMap.bind(this);
    this.onDragEndMarker = this.onDragEndMarker.bind(this);
    this.onChangeLat = this.onChangeLat.bind(this);
    this.onChangeLng = this.onChangeLng.bind(this);
    this.onMoveCords = this.onMoveCords.bind(this);
  }

  componentDidMount() {
    this.initMap();
  }

  componentWillUnmount() {
    this.removeMap();
  }

  componentWillReceiveProps(newProps) {
    const { revision } = this.props;
    const oldIsUpdating = this.props.isCopy;
    const newIsUpdating = newProps.isCopy;
    if (!oldIsUpdating && newIsUpdating && revision) {
      browserHistory.replace(`/monument/${revision.properties.monument}/`);
    }
  }

  onClickMap(e) {
    const { lat, lng } = this.props;
    const latlng = e.latlng;
    this.marker.setLatLng(latlng);

    lng.onChange(latlng.lng);
    lat.onChange(latlng.lat);
  }

  onDragEndMarker() {
    const { lat, lng } = this.props;
    const latlng = this.marker.getLatLng();
    lng.onChange(latlng.lng);
    lat.onChange(latlng.lat);
  }

  onChangeLat(e) {
    const { lat } = this.props;
    const val = parseFloat(e.target.value);
    if (e.target.value === '') {
      lat.onChange('');
    } else if (!isNaN(val) && val >= -90 && val <= 90) {
      lat.onChange(e.target.value);
    }
  }

  onChangeLng(e) {
    const { lng } = this.props;
    const val = parseFloat(e.target.value);
    if (e.target.value === '') {
      lng.onChange('');
    } else if (!isNaN(val) && val >= -180 && val <= 180) {
      lng.onChange(e.target.value);
    }
  }

  removeMap() {
    if (this.map) this.map.remove();
    delete this.map;
  }

  initMap() {
    const { lat, lng } = this.props.coordinates;
    const tiles = L.tileLayer(tileUrl);
    this.map = L.map(this.refs.map, mapSettings).addLayer(tiles);
    this.marker = L.marker([lat, lng], {
      draggable: true,
      icon
    }).addTo(this.map);

    if (lng && lat) {
      this.map.panTo(this.marker.getLatLng());
    }

    this.map.on('click', this.onClickMap);
    this.marker.on('dragend', this.onDragEndMarker);
  }


  onMoveCords() {
    const { revision: { properties: { monument } } } = this.props;
    const latlng = this.marker.getLatLng();
    this.props.moveRevisionCoordinate({
      geometry: {
        coordinates: [parseFloat(latlng.lng), parseFloat(latlng.lat)],
        type: "Point"
      },
      id: monument
    });
  }

  renderCopyCordsBtn() {
    const { copyCordsToMonument } = this.props;
    if (!copyCordsToMonument) return null;

    const { user } = this.props;
    if (!user.is_master && !user.is_superuser) return null;

    const copyCordsNode = (
      <div className="control">
        <label className="label">&nbsp;</label>
        <button className="button is-dark">
          <sapn
            className="move-file__icon"
            dangerouslySetInnerHTML={{ __html: filesSvg }}
          ></sapn>
        </button>
      </div>
    );

    const moveBtn = [
      <button key="btn" className="button is-primary" onClick={this.onMoveCords}>Изменить</button>
    ];


    return (
      <Portal closeOnEsc openByClickOn={copyCordsNode}>
        <Modal isOpen title="Вы действительно хотите изменить координаты?" text=" Текущее значение изменено." buttons={moveBtn} />
      </Portal>
    );
  }

  render() {
    const { label, lat, lng } = this.props;

    return (
      <div className="control fld-geometry">
        <div className="map" ref="map"></div>
        <div className="control is-grouped">
          <div className="control is-expanded">
            <label className="label">Широта</label>
            <input
              {...lat}
              onChange={this.onChangeLat}
              className="input"
            />
          </div>
          <div className="control is-expanded">
            <label className="label">Долгота</label>
            <input
              {...lng}
              onChange={this.onChangeLng}
              className="input"
            />
          </div>
          {this.renderCopyCordsBtn()}
        </div>
      </div>
    );
  }

}

FldGeometry.propTypes = {
  copyCordsToMonument: PropTypes.bool,
  isUpdating: PropTypes.bool,
  onChangeRevisionCoordinates: PropTypes.func.isRequired,
  coordinates: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  lat: PropTypes.object.isRequired,
  lng: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

export default connect(state => {
  return {
    user: state.auth.data.objects[0],
    isCopy: state.revisions.copy
  }
}, {
  moveRevisionCoordinate,
})(FldGeometry);
