import React, { PropTypes } from 'react';
import classNames from 'classnames';

const FldText = ({ field, name, label, placeholder, settings, isUpdating = false }) => {
  const labelCls = classNames('label', {
    strong: settings.is_required
  });
  return (
    <div className="control">
      <label className={labelCls}>{label}</label>
      <textarea
        {...field}
        className="textarea"
        name={name}
        placeholder={placeholder}
        disabled={isUpdating}
      />
    </div>
  );
};

FldText.propTypes = {
  isUpdating: PropTypes.bool.isRequired,
  field: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  settings: PropTypes.object,
};

export default FldText;
