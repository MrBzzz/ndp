import React, { PropTypes } from 'react';
import { isNull } from 'lodash';
import classNames from 'classnames';

const FldVarchar = ({ name, field, label, placeholder, settings, disabled = false, isUpdating }) => {
  const fld = field;
  const labelCls = classNames('label', {
    strong: settings.is_required
  });
  fld.value = isNull(field.value) ? '' : field.value;
  return (
    <div className="control">
      <label className={labelCls}>{label}</label>
      <input
        {...fld}
        className="input"
        type="text"
        name={name}
        placeholder={placeholder}
        disabled={disabled || settings.is_readable || isUpdating}
      />
    </div>
  );
};

FldVarchar.propTypes = {
  isUpdating: PropTypes.bool,
  disabled: PropTypes.bool,
  settings: PropTypes.object,
  field: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired
};

export default FldVarchar;
