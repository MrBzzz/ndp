import React, { Component, PropTypes } from 'react';

class Fade extends Component {

  constructor() {
    super();
    this.showElement = this.showElement.bind(this);
    this.hideElement = this.hideElement.bind(this);
  }

  componentWillAppear(callback) {
    setTimeout(callback, 1); // need at least one tick to fire transition
  }

  componentDidAppear() {
    this.showElement();
  }

  componentWillEnter(callback) {
    setTimeout(callback, 1);
  }

  componentDidEnter() {
    this.showElement();
  }

  componentWillLeave(callback) {
    this.hideElement();
    setTimeout(callback, this.props.duration);
  }

  componentDidLeave() {
  }

  showElement() {
    const el = this.refs.element;
    el.style.opacity = 1;
  }

  hideElement() {
    const el = this.refs.element;
    el.style.opacity = 0;
  }

  render() {
    const props = Object.assign({}, this.props);
    const style = {
      opacity: 0,
      WebkitTransition: `opacity ${this.props.duration}ms ease-out`,
      msTransition: `opacity ${this.props.duration}ms ease-out`,
      transition: `opacity ${this.props.duration}ms ease-out`
    };

    props.style = Object.assign(style, this.props.style);

    return React.createElement(
      this.props.component,
      props,
      this.props.children
    );
  }
}

Fade.propTypes = {
  duration: PropTypes.number,
  style: PropTypes.object,
  component: PropTypes.string,
  children: PropTypes.array
};

Fade.defaultProps = {
  component: 'div',
  duration: 200,
  ref: 'element'
};

export default Fade;
