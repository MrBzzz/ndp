import React, { Component, PropTypes } from 'react';

import ArrowLeft from 'components/Lightbox/icons/arrowLeft.svg';
import ArrowRight from 'components/Lightbox/icons/arrowRight.svg';
import Close from 'components/Lightbox/icons/close.svg';

class Icon extends Component {

  renderIcon() {
    let IconSvg = null;

    const { type } = this.props;

    switch (type) {
      case 'arrowLeft': {
        IconSvg = <span dangerouslySetInnerHTML={{ __html: ArrowLeft }}></span>;
        return IconSvg;
      }
      case 'arrowRight': {
        IconSvg = <span dangerouslySetInnerHTML={{ __html: ArrowRight }}></span>;
        return IconSvg;
      }
      case 'close': {
        IconSvg = <span dangerouslySetInnerHTML={{ __html: Close }}></span>;
        return IconSvg;
      }
      default: {
        return IconSvg;
      }
    }
  }

  render() {
    return (
      <span>
        {this.renderIcon()}
      </span>
    );
  }

}

Icon.propTypes = {
  type: PropTypes.string
};

export default Icon;
