import React, { Component, PropTypes } from 'react';
import Swipeable from 'react-swipeable';

import utils from 'components/Lightbox/utils';
import Fade from 'components/Lightbox/Fade';
import Icon from 'components/Lightbox/Icon';
import Portal from 'components/Lightbox/Portal';

import 'components/Lightbox/Lightbox.less';

class Lightbox extends Component {

  constructor(props) {
    super(props);

    this.close = this.close.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrev = this.gotoPrev.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.handleKeyboardInput = this.handleKeyboardInput.bind(this);
    this.handleResize = this.handleResize.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen && nextProps.enableKeyboardInput) {
      if (utils.canUseDOM) window.addEventListener('keydown', this.handleKeyboardInput);
      if (utils.canUseDOM) window.addEventListener('resize', this.handleResize);
      this.handleResize();
    } else {
      if (utils.canUseDOM) window.removeEventListener('keydown', this.handleKeyboardInput);
      if (utils.canUseDOM) window.removeEventListener('resize', this.handleResize);
    }

    if (nextProps.isOpen) {
      if (utils.canUseDOM) document.body.style.overflow = 'hidden';
    } else {
      if (utils.canUseDOM) document.body.style.overflow = null;
    }
  }

  close(e) {
    if (e.target.id !== 'react-images-container') return null;
    return this.props.backdropClosesModal && this.props.onClose && this.props.onClose();
  }

  gotoNext(e) {
    if (this.props.currentMedia === (this.props.medias.length - 1)) return;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    this.props.onClickNext();
  }

  gotoPrev(e) {
    if (this.props.currentMedia === 0) return;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    this.props.onClickPrev();
  }

  handleImageClick(e) {
    if (!this.props.onClickShowNextImage) return;
    this.gotoNext(e);
  }

  handleKeyboardInput(event) {
    if (event.keyCode === 37) {
      this.gotoPrev(event);
    } else if (event.keyCode === 39) {
      this.gotoNext(event);
    } else if (event.keyCode === 27) {
      this.props.onClose();
    }

    return false;
  }

  handleResize() {
    if (!utils.canUseDOM) return;
    this.setState({
      windowHeight: window.innerHeight || 0
    });
  }


  renderArrowNext() {
    if (this.props.currentMedia === (this.props.medias.length - 1)) return null;
    return (
      <button title="Next (Right arrow key)" type="button" className="arrow arrowNext" onClick={this.gotoNext} onTouchEnd={this.gotoNext}>
        <Icon type="arrowRight" />
      </button>
    );
  }

  renderArrowPrev() {
    if (this.props.currentMedia === 0) return null;

    return (
      <button title="Previous (Left arrow key)" type="button" className="arrow arrowPrev" onClick={this.gotoPrev} onTouchEnd={this.gotoPrev}>
        <Icon type="arrowLeft" />
      </button>
    );
  }

  renderCloseButton() {
    if (!this.props.showCloseButton) return null;

    return (
      <div className="closeBar">
        <button title="Close (Esc)" className="closeButton" onClick={this.props.onClose}>
          <Icon type="close" />
        </button>
      </div>
    );
  }

  renderDialog() {
    if (!this.props.isOpen) return null;

    return (
      <Fade id="react-images-container" key="dialog" duration={250} className="lightbox-container" onClick={this.close} onTouchEnd={this.close}>
        <span className="contentHeightShim" />
        <div className="content">
          {this.renderCloseButton()}
          {this.renderMedia()}
        </div>
        {this.renderArrowPrev()}
        {this.renderArrowNext()}
      </Fade>
    );
  }

  renderFooter(caption) {
    const { currentMedia, medias, showImageCount } = this.props;

    if (!caption && !showImageCount) return null;

    const imageCount = showImageCount ? <div className="footerCount">{currentMedia + 1} of {medias.length}</div> : null;
    const figcaption = caption ? <figcaption className="footerCaption">{caption}</figcaption> : null;

    return (
      <div className="footer">
        {imageCount}
        {figcaption}
      </div>
    );
  }

  renderMedia() {
    const { medias, currentMedia } = this.props;
    const media = medias[currentMedia];
    switch (media.type) {
      case 'image': {
        return this.renderImages();
      }
      case 'video': {
        return this.renderVideo();
      }
      default:
        return null;
    }
  }

  renderVideo() {
    const { medias, currentMedia } = this.props;

    if (!medias || !medias.length) return null;

    const media = medias[currentMedia];

    return (
      <figure key={`media${currentMedia}`} className="figure" style={{ maxWidth: this.props.width }}>
        <Swipeable onSwipedLeft={this.gotoNext} onSwipedRight={this.gotoPrev} >
          <video className="media" controls>
            <source src={media.src} type="video/mp4" />
          </video>
        </Swipeable>
      </figure>
    );
  }

  renderImages() {
    const { medias, currentMedia } = this.props;
    const { windowHeight } = this.state;

    if (!medias || !medias.length) return null;

    const media = medias[currentMedia];

    let sizes;
    let srcset;

    if (media.srcset) {
      srcset = media.srcset.join();
      sizes = '100vw';
    }

    return (
      <figure key={`media${currentMedia}`} className="figure" style={{ maxWidth: this.props.width }}>
        <Swipeable onSwipedLeft={this.gotoNext} onSwipedRight={this.gotoPrev} >
          <img
            className="media"
            role="presentation"
            onClick={this.handleImageClick}
            sizes={sizes}
            src={media.src}
            srcSet={srcset}
            style={{
              cursor: this.props.onClickShowNextImage ? 'pointer' : 'auto',
              maxHeight: windowHeight
            }}
          />
        </Swipeable>
        {this.renderFooter(media.caption)}
      </figure>
    );
  }

  render() {
    return (
      <Portal>
        {this.renderDialog()}
      </Portal>
    );
  }

}

Lightbox.propTypes = {
  backdropClosesModal: PropTypes.bool,
  currentMedia: PropTypes.number,
  enableKeyboardInput: PropTypes.bool,
  medias: PropTypes.arrayOf(
    PropTypes.shape({
      src: PropTypes.string.isRequired,
      type: PropTypes.string,
      srcset: PropTypes.array,
      caption: PropTypes.string
    })
  ).isRequired,
  isOpen: PropTypes.bool,
  onClickShowNextImage: PropTypes.bool,
  onClickNext: PropTypes.func.isRequired,
  onClickPrev: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  showCloseButton: PropTypes.bool,
  showImageCount: PropTypes.bool,
  width: PropTypes.number
};

Lightbox.defaultProps = {
  enableKeyboardInput: true,
  currentMedia: 0,
  onClickShowNextImage: true,
  showCloseButton: true,
  showImageCount: true,
  width: 900
};

export default Lightbox;
