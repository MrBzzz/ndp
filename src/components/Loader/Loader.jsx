import React, { PropTypes } from 'react';
import classNames from 'classnames';
import './Loader.less';

const Loader = ({ isShown = true, isStatic = false }) => {
  const loaderClass = classNames('spinner', {
    spinner_static: isStatic
  });
  return (isShown ? <div className={loaderClass}></div> : null);
};

Loader.propTypes = {
  isShown: PropTypes.bool,
  isStatic: PropTypes.bool
};

export default Loader;
