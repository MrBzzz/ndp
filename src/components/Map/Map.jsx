import React, { Component, PropTypes } from 'react';
import L from 'leaflet';
import { isEqual } from 'lodash';
import 'leaflet-sleep';
import { mapSettings, tileUrl, iconSettings } from 'settings/settings.js';
import './Map.less';

const icon = new L.Icon(iconSettings);

class Map extends Component {

  componentDidMount() {
    const { features } = this.props;
    this.initMap();
    this.addFeaturesToMap(features);
  }

  componentWillReceiveProps(newProps) {
    const oldFeatures = this.props.features;
    const newFeatures = newProps.features;
    if (!isEqual(oldFeatures, newFeatures)) {
      this.addFeaturesToMap(newFeatures);
    }
  }

  componentWillUnmount() {
    this.removeMap();
  }

  getPoints(features) {
    return features
      .filter(({ geometry }) => geometry)
      .map(({ geometry: { coordinates: [lat, lng] }, properties }) => {
        const marker = L.marker([lng, lat], { icon });
        if (properties.name && properties.id) {
          const text = `<a href="/monument/${properties.id}">${properties.name}</a>`;
          marker.bindPopup(text);
        }
        return marker;
      });
  }

  removeMap() {
    if (this.map) this.map.remove();
    delete this.map;
  }

  initMap() {
    const tiles = L.tileLayer(tileUrl);
    this.map = L.map(this.refs.map, mapSettings).addLayer(tiles);
  }

  addFeaturesToMap(features) {
    const points = this.getPoints(features);
    if (this.markers) {
      this.map.removeLayer(this.markers);
      this.map.setView(mapSettings.center, mapSettings.zoom);
    }
    if (points.length) {
      this.markers = L.featureGroup(points);
      this.map.addLayer(this.markers);
      this.map.fitBounds(this.markers.getBounds());
    }
  }

  render() {
    return <div className="map" ref="map"></div>;
  }
}

Map.propTypes = {
  features: PropTypes.array.isRequired
};

export default Map;

