import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { has } from 'lodash';
import Lightbox from 'components/Lightbox/Lightbox';
import { openLightbox, closeLightbox, prevLightbox, nextLightbox } from 'reducers/lightbox';
import { prepareMedia, prepareMediaLightBox } from 'helpers/media';
import i18n from 'i18n/translation';
import playIconSvg from 'svg/play.svg';
import AddFiles from 'components/AddFiles/AddFiles';
import Loader from 'components/Loader/Loader';
import RemoveFiles from 'components/RemoveFiles/RemoveFiles';
import ChangeFileSort from 'components/ChangeFileSort/ChangeFileSort';
import MoveFiles from 'components/MoveFiles/MoveFiles';
import SubTitle from 'components/SubTitle/SubTitle';
import './Media.less';

class Media extends Component {

  constructor(props) {
    super(props);
    this.openLightbox = this.openLightbox.bind(this);
    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
  }

  openLightbox(e, index) {
    e.preventDefault();
    this.props.openLightbox(index);
  }

  closeLightbox() {
    this.props.closeLightbox();
  }

  gotoPrevious() {
    this.props.prevLightbox();
  }

  gotoNext() {
    this.props.nextLightbox();
  }

  renderLightbox() {
    const { lightboxIsOpen, currentImage, properties: { images, videos } } = this.props;
    const media = prepareMediaLightBox(images, videos);
    return (
      <Lightbox
        currentMedia={currentImage}
        medias={media}
        isOpen={lightboxIsOpen}
        onClickPrev={this.gotoPrevious}
        onClickNext={this.gotoNext}
        onClose={this.closeLightbox}
        backdropClosesModal
      />
    );
  }

  renderThumb(item, key) {
    let thumb = null;
    if (item.type === 'image') {
      thumb = this.renderThumbImage(item, key);
    } else if (item.type === 'video') {
      thumb = this.renderThumbVideo(item, key);
    } else if (item.type === 'load') {
      thumb = this.renderLoadMedia(key);
    }
    return thumb;
  }

  renderImagesMoveNode(item) {
    const { currentUser: { objects: [userData] } } = this.props;
    if (userData.is_master || userData.is_superuser) {
      const { onMoveFile, moveFile, properties } = this.props;
      if (!has(properties, 'monument_object')) { return null; }
      return <MoveFiles media={item} show={moveFile} onMoveFile={onMoveFile} monumentFiles={properties.monument_object.images} />;
    } else {
      return null;
    }
  }

  renderVideoMoveNode(item) {
    const { onMoveFile, moveFile, properties } = this.props;
    if (!has(properties, 'monument_object')) {
      return null;
    }
    return <MoveFiles media={item} show={moveFile} onMoveFile={onMoveFile} monumentFiles={properties.monument_object.videos} />;
  }

  renderThumbImage(item, key) {
    const { editable, onRemoveFiles, onChangeSortFiles, changeSort } = this.props;
    return (
      <div
        key={key}
        className="thumbnail"
        style={{ backgroundImage: `url(${item.thumbnail})` }}
        onClick={(e) => this.openLightbox(e, key)}
      >
        {/*<span style={{ position: 'absolute', bottom: '0', color: '#fff' }}>{item.id}</span>*/}
        {changeSort && <ChangeFileSort media={item} show={editable} onChangeSortFiles={onChangeSortFiles} />}
        <RemoveFiles media={item} show={editable} onRemoveFiles={onRemoveFiles} />
        {this.renderImagesMoveNode(item)}
      </div>
    );
  }

  renderThumbVideo(item, key) {
    const { editable, onRemoveFiles } = this.props;
    return (
      <div key={key} className="thumbnail thumbnail_video" onClick={(e) => this.openLightbox(e, key)}>
        <RemoveFiles media={item} show={editable} onRemoveFiles={onRemoveFiles} />
        {this.renderVideoMoveNode(item)}
        <span
          className="play-icon"
          dangerouslySetInnerHTML={{ __html: playIconSvg }}
        ></span>
      </div>
    );
  }

  renderLoadMedia(key) {
    return (
      <div key={key} className="thumbnail thumbnail_load">
        <Loader isStatic />
      </div>
    );
  }

  renderThumbnails() {
    const { properties: { images, videos }, onDropFiles, editable = false } = this.props;
    const media = prepareMedia(images, videos);
    return (
      <div className="thumbnails">
        {media.map((item, key) => this.renderThumb(item, key))}
        <AddFiles show={editable} onDropFiles={onDropFiles} />
      </div>
    );
  }

  render() {
    const { properties: { images, videos }, editable } = this.props;
    if (!images.length && !videos.length && !editable) return null;
    return (
      <div className="media-list">
        <SubTitle text={i18n.t('media.title')} />
        {this.renderThumbnails()}
        {this.renderLightbox()}
      </div>
    );
  }

}

Media.propTypes = {
  editable: PropTypes.bool,
  changeSort: PropTypes.bool,
  properties: PropTypes.object,
  lightbox: PropTypes.object,
  openLightbox: PropTypes.func,
  closeLightbox: PropTypes.func,
  prevLightbox: PropTypes.func,
  nextLightbox: PropTypes.func,
  currentImage: PropTypes.number,
  lightboxIsOpen: PropTypes.bool,
  onDropFiles: PropTypes.func,
  onRemoveFiles: PropTypes.func,
  onChangeSortFiles: PropTypes.func,
  onMoveFile: PropTypes.func,
  moveFile: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  lightbox: state.lightbox,
  lightboxIsOpen: state.lightbox.lightboxIsOpen,
  currentImage: state.lightbox.currentImage,
  currentUser: state.auth.data,
});

export default connect(mapStateToProps, {
  openLightbox,
  closeLightbox,
  prevLightbox,
  nextLightbox
})(Media);
