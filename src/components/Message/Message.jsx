import React, { PropTypes } from 'react';

const Message = ({ text }) => (
  <div className="notification is-info">{text}</div>
);

Message.propTypes = {
  text: PropTypes.string
};

export default Message;
