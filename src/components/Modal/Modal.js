import React, { PropTypes } from 'react';
import i18n from 'i18n/translation';

const Modal = ({ title = '', text = '', buttons = [], closePortal }) => (
  <div className="modal is-active">
    <div className="modal-background" onClick={closePortal} />
    <div className="modal-card">
      <header className="modal-card-head is-danger">
        <p className="modal-card-title">{title}</p>
        <button className="delete" onClick={closePortal}></button>
      </header>
      <section className="modal-card-body">
        {text}
      </section>
      <footer className="modal-card-foot">
        {buttons}
        <button className="button" onClick={closePortal}>{i18n.t('buttons.cancel')}</button>
      </footer>
    </div>
  </div>
);

Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  buttons: PropTypes.array,
  closePortal: PropTypes.func,
};

export default Modal;
