import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import classNames from 'classnames';
import i18n from 'i18n/translation';
import FldText from 'components/FldText/FldText';
import FldVarchar from 'components/FldVarchar/FldVarchar';
import FldBoolChoices from 'components/FldBoolChoices/FldBoolChoices';
import FldChoices from 'components/FldChoices/FldChoices';
import FldGeometry from 'components/FldGeometry/FldGeometry';
import Media from 'components/Media/Media';
import './MonumentForm.less';

const form_fields = [
  {
    name: 'geometry',
    type: 'geometry',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'lat',
    type: '',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'lng',
    type: '',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'id',
    type: 'integer',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'formatted_created',
    type: 'datetime',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'formatted_updated',
    type: 'datetime',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'name',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: true,
    }
  },
  {
    name: 'code',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: true,
    }
  },
  {
    name: 'location',
    type: 'foreign_key',
    options: {
      is_readable: false,
      is_required: true,
    }
  },
  {
    name: 'kind',
    type: 'foreign_key',
    options: {
      is_readable: false,
      is_required: true,
    }
  },
  {
    name: 'is_published',
    type: 'boolean',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'is_published_on_geoportal',
    type: 'boolean',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'current_state_status',
    type: 'foreign_key',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'address',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'address_more',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'history_info',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'owner',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'owner_address',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_1',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_phone_1',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_email_1',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_2',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_phone_2',
    type: 'text',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'responsible_person_email_2',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'number_of_dead',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'number_of_dead_known',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'number_of_dead_unknown',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
];

class RevisionForm extends Component {

  renderField(fld, key) {
    const { fields, is_updating } = this.props;
    const { name, type, options } = fld;
    const field = fields[name];
    const label = i18n.t(`forms.monument.${name}.label`);
    const placeholder = i18n.t(`forms.monument.${name}.placeholder`);

    switch (type) {
      case 'geometry': {
        const { onChangeCoordinates, monument: { properties: { lat, lng } } } = this.props;
        return (
          <FldGeometry
            settings={options}
            onChangeRevisionCoordinates={onChangeCoordinates}
            name={name}
            label={label}
            placeholder={placeholder}
            lng={fields.lng}
            lat={fields.lat}
            coordinates={{ lat, lng }}
            key={key}
            copyCordsToMonument={false}
          />
        );
      }
      case 'foreign_key': {
        const { monument: { properties: { choices } } } = this.props;
        const list = choices[name];
        return (
          <FldChoices
            key={key}
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            options={list}
            isUpdating={is_updating}
          />
        );
      }
      case 'boolean': {
        return (
          <FldBoolChoices
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            key={key}
            isUpdating={is_updating}
          />
        );
      }
      case 'integer': {
        return (
          <FldVarchar
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            key={key}
            isUpdating={is_updating}
          />
        );
      }
      case 'datetime': {
        return (
          <FldVarchar
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            key={key}
            isUpdating={is_updating}
          />
        );
      }
      case 'varchar': {
        return (
          <FldVarchar
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            isUpdating={is_updating}
            key={key}
          />
        );
      }
      case 'text': {
        return (
          <FldText
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            key={key}
            isUpdating={is_updating}
          />
        );
      }
      default: {
        return null;
      }
    }
  }

  renderFields() {
    const { fields_names = [] } = this.props;
    return form_fields.filter(item => ~fields_names.indexOf(item.name)).map((fld, key) => this.renderField(fld, key));
  }

  render() {
    const { textSubmitBtn, files = true, handleSubmit, is_updating, onDropFiles, onRemoveFiles, onChangeSortFiles, monument: { properties } } = this.props;
    const btnClassName = classNames('button', 'is-info', { 'is-loading': is_updating });
    return (
      <form onSubmit={handleSubmit} className="monument-form">
        {this.renderFields()}
        {files && <Media properties={properties} onDropFiles={onDropFiles} onRemoveFiles={onRemoveFiles} onChangeSortFiles={onChangeSortFiles} changeSort editable />}
        <button className={btnClassName}>{textSubmitBtn}</button>
      </form>
    );
  }
}

RevisionForm.propTypes = {
  monument: PropTypes.object,
  onDropFiles: PropTypes.func,
  onRemoveFiles: PropTypes.func,
  onChangeSortFiles: PropTypes.func,
  fields: PropTypes.object.isRequired,
  onChangeCoordinates: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  is_updating: PropTypes.bool.isRequired,
  textSubmitBtn: PropTypes.string,
  files: PropTypes.bool,
  fields_names: PropTypes.array,
};

const mapStateToProps = (state) => ({
  initialValues: state.monuments.monument.properties
});

export default reduxForm({
  form: 'monument-form',
  fields: form_fields.map(item => item.name),
  overwriteOnInitialValuesChange: false,
}, mapStateToProps)(RevisionForm);
