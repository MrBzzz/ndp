import React, { PropTypes } from 'react';
import StatusIcon from 'components/StatusIcon/StatusIcon';
import './MonumentState.less';

const MonumentState = ({ feature: { current_state } }) => {
  const status = (current_state) ? current_state.status : 0;
  return (
    <div className="state">
      <StatusIcon status={status} className="status__icon_absolute" />
    </div>
  );
};

MonumentState.propTypes = {
  feature: PropTypes.object,
  revision: PropTypes.object
};

export default MonumentState;

