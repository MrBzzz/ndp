import React, { PropTypes, Component } from 'react';
import Portal from 'react-portal';
import Modal from 'components/Modal/Modal';
import i18n from 'i18n/translation';
import filesSvg from 'svg/files-o.svg';
import './MoveFiles.less';

class MoveFiles extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.stopPropagation();
    const { media } = this.props;
    this.props.onMoveFile(media);
  }

  render() {
    if (!this.props.show) return null;

    const { monumentFiles, media } = this.props;
    const moveIcon = (
      <div className="move-file">
        <sapn
          alt="Изображение уже скопированно в памятник." title="Изображение уже скопированно в памятник."
          className="move-file__icon"
          dangerouslySetInnerHTML={{ __html: filesSvg }}
        ></sapn>
      </div>
    );

    if (!!~monumentFiles.map(item => item.file).indexOf(media.file)) {
      return (
        <span className="bolt" alt="Изображение уже скопированно в памятник." title="Изображение уже скопированно в памятник.">
          {moveIcon}
        </span>
      );
    }

    const moveBtn = [
      <button key="btn" className="button is-primary" onClick={this.onClick}>{i18n.t('buttons.move')}</button>
    ];

    return (
      <Portal closeOnEsc openByClickOn={moveIcon}>
        <Modal isOpen title={i18n.t('modal.move_media.title')} text={i18n.t('modal.move_media.text')} buttons={moveBtn} />
      </Portal>
    );
  }
}

MoveFiles.propTypes = {
  show: PropTypes.bool,
  media: PropTypes.object,
  onMoveFile: PropTypes.func,
  monumentFiles: PropTypes.array,
};

export default MoveFiles;

