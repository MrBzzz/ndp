import React, { PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';
import i18n from 'i18n/translation';
import signOutIconSvg from 'svg/sign-out.svg';

const Navbar = ({ user: { objects: [userData] } }) => (
  <nav className="nav has-shadow">
    <div className="container">

      <div className="nav-left">
        <IndexLink className="header-item nav-item roboto roboto_ub" to="/">{i18n.t('navbar.logo')}</IndexLink>
      </div>

      <div className="nav-center nav-menu">
        <IndexLink className="nav-item is-tab" activeClassName="is-active" to="/">
          {i18n.t('navbar.links.monuments')}
          <span className="tag is-dark is-medium">{userData.monumentTotalCount}</span>
        </IndexLink>
        <Link className="nav-item is-tab" activeClassName="is-active" to="/revisions">
          {i18n.t('navbar.links.revisions')}
          <span className="tag is-dark is-medium">{userData.revisionTotalCount}</span>
        </Link>
        <Link className="nav-item is-tab" activeClassName="is-active" to="/users">
          {i18n.t('navbar.links.users')}
          <span className="tag is-dark is-medium">{userData.userTotalCount}</span>
        </Link>
        <Link className="nav-item is-tab" activeClassName="is-active" to="/monument/map">
          {i18n.t('navbar.links.monuments_map')}
        </Link>
      </div>

      <span className="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
      </span>

      <div className="nav-right">
        <Link className="nav-item" to={`/user/${userData.id}`}>{userData.first_name}&nbsp;{userData.last_name}</Link>
        <span className="nav-item">
          <Link to="/signout">
            <sapn dangerouslySetInnerHTML={{ __html: signOutIconSvg }}></sapn>
          </Link>
        </span>
      </div>

    </div>
  </nav>
);

Navbar.propTypes = {
  user: PropTypes.object.isRequired
};

export default Navbar;
