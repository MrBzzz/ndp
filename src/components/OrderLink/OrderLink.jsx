import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import i18n from 'i18n/translation';

const mapStateToProps = (state) => ({
  routing: state.routing,
  filters: state.filters,
});

const OrderLink = ({ routing, filters, pathname, page, text, field }) => {
  const currentQuery = routing.locationBeforeTransitions.query;
  const query = { ...currentQuery };

  query.order_by = (currentQuery.order_by && currentQuery.order_by === field) ? `-${field}`: `${field}`;

  const to = {
    pathname,
    query,
  };
  return (
    <Link to={to}>
      {text}
      {(currentQuery.order_by && currentQuery.order_by === field) ? <span>&#x25B2;</span> : <span>&#x25BC;</span>}
    </Link>
  );
};

OrderLink.propTypes = {
  page: PropTypes.string.isRequired,
  pathname: PropTypes.string.isRequired,
  filters: PropTypes.object
};

export default connect(mapStateToProps, {})(OrderLink);
