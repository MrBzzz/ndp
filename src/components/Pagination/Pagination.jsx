import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { times } from 'lodash';
import prevIconSvg from 'svg/angle-left.svg';
import nextIconSvg from 'svg/angle-right.svg';
import i18n from 'i18n/translation';
import './Pagination.less';

const renderPaginationCurrent = (num) => <li key={num}><a className="button is-primary is-active">{num}</a></li>;

const renderPaginationLink = (num, limit, oldQuery, pathname) => {
  const offset = limit * (num - 1);
  const to = {
    pathname,
    query: {
      ...oldQuery,
      offset
    }
  };
  if (offset === 0) delete to.query.offset;
  return <li key={num}><Link className="button" to={to}>{num}</Link></li>;
};

const renderNextLink = (offset, limit, total_count, oldQuery, pathname) => {
  const nextOffset = offset + limit;
  const isDisabled = nextOffset > total_count;
  const to = {
    pathname,
    query: {
      ...oldQuery,
      offset: nextOffset
    }
  };
  const icon = (<i
    dangerouslySetInnerHTML={{ __html: nextIconSvg }}
    className="pagination__icon"
  ></i>);
  return (
    <li>
      {!isDisabled && <Link className="button" to={to}>{icon}</Link>}
      {isDisabled && <a className="button is-disabled">{icon}</a>}
    </li>
  );
};

const renderPrevLink = (offset, limit, oldQuery, pathname) => {
  const nextOffset = offset - limit;
  const isDisabled = nextOffset < 0;
  const to = {
    pathname,
    query: {
      ...oldQuery,
      offset: nextOffset
    }
  };

  if (nextOffset === 0) delete to.query.offset;

  const icon = (
    <i
      dangerouslySetInnerHTML={{ __html: prevIconSvg }}
      className="pagination__icon"
    ></i>
  );

  return (
    <li>
      {!isDisabled && <Link className="button" to={to}>{icon}</Link>}
      {isDisabled && <a className="button is-disabled">{icon}</a>}
    </li>
  );
};

const renderFirstLink = (offset, limit, oldQuery, pathname) => {
  const nextOffset = offset - limit;
  const isDisabled = nextOffset < 0;
  const to = {
    pathname,
    query: {
      ...oldQuery,
      offset: 0
    }
  };

  delete to.query.offset;

  return (
    <li>
      {!isDisabled && <Link className="button" to={to}>{i18n.t('pagination.first')}</Link>}
      {isDisabled && <a className="button is-disabled">{i18n.t('pagination.first')}</a>}
    </li>
  );
};

const renderLastLink = (offset, limit, total_count, oldQuery, pathname) => {
  const nextOffset = offset + limit;
  const isDisabled = nextOffset > total_count;
  const to = {
    pathname,
    query: {
      ...oldQuery,
      offset: (total_count % limit === 0) ? total_count - limit : Math.floor(total_count / limit) * limit,
    }
  };

  return (
    <li>
      {!isDisabled && <Link className="button" to={to}>{i18n.t('pagination.last')}</Link>}
      {isDisabled && <a className="button is-disabled">{i18n.t('pagination.last')}</a>}
    </li>
  );
};

const Pagination = ({ meta, query, pathname }) => {
  const { limit, total_count, offset } = meta;
  const current = Math.ceil((total_count - (total_count - offset)) / limit) + 1;
  const pages_count = Math.ceil(total_count / limit);

  let pages = [];

  if (pages_count > 5) {
    let getPages = (item) => item + 1;
    if (current > 3 && current + 3 <= pages_count) {
      getPages = (item) => item + 1 + current - 3;
    } else if (current + 3 > pages_count) {
      getPages = (item) => item + 1 + pages_count - 5;
    }
    pages = times(5, getPages);
  } else {
    pages = times(pages_count, (item) => item + 1);
  }

  if (!pages.length || pages.length === 1) return null;

  return (
    <nav className="pagination">
      <ul>
        {renderFirstLink(offset, limit, query, pathname)}
        {renderPrevLink(offset, limit, query, pathname)}
        {pages.map(num => (num === current ? renderPaginationCurrent(num) : renderPaginationLink(num, limit, query, pathname)))}
        {renderNextLink(offset, limit, total_count, query, pathname)}
        {renderLastLink(offset, limit, total_count, query, pathname)}
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  pathname: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  query: PropTypes.object
};

export default Pagination;
