import React, { PropTypes } from 'react';
import cameraIconSvg from 'svg/camera.svg';
import './PhotoCount.less';

const PhotoCount = ({ count }) => {
  if (!count) return null;
  return (
    <div className="count-photo">
      <sapn
        className="count-photo__icon"
        dangerouslySetInnerHTML={{ __html: cameraIconSvg }}
      ></sapn>
      <span className="count-photo__value">{count}</span>
    </div>
  );
};

PhotoCount.propTypes = {
  count: PropTypes.number
};

export default PhotoCount;
