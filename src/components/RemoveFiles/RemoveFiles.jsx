import React, { PropTypes, Component } from 'react';
import Portal from 'react-portal';
import Modal from 'components/Modal/Modal';
import closeIconSvg from 'svg/close.svg';
import i18n from 'i18n/translation';
import './RemoveFiles.less';

class RemoveFiles extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.stopPropagation();
    const { media } = this.props;
    this.props.onRemoveFiles(media);
  }

  render() {
    if (!this.props.show) return null;

    const removeIcon = (
      <div className="remove-file">
        <sapn
          className="remove-file__icon"
          dangerouslySetInnerHTML={{ __html: closeIconSvg }}
        ></sapn>
      </div>
    );

    const removeBtn = [
      <button key="btn" className="button is-danger" onClick={this.onClick}>{i18n.t('buttons.remove')}</button>
    ];

    return (
      <Portal closeOnEsc openByClickOn={removeIcon}>
        <Modal isOpen title={i18n.t('modal.remove_media.title')} text={i18n.t('modal.remove_media.text')} buttons={removeBtn} />
      </Portal>
    );
  }
}

RemoveFiles.propTypes = {
  show: PropTypes.bool,
  media: PropTypes.object,
  onRemoveFiles: PropTypes.func,
};

export default RemoveFiles;

