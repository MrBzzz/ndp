import React, { PropTypes } from 'react';
import streetViewIconSvg from 'svg/street-view.svg';
import './RevisionCount.less';

const RevisionCount = ({ feature: { info: { revision_count } } }) => {
  if (!revision_count) return null;
  return (
    <div className="revision-count">
      <sapn
        className="revision-count__icon"
        dangerouslySetInnerHTML={{ __html: streetViewIconSvg }}
      ></sapn>
      <span className="revision-count__value">{revision_count}</span>
    </div>
  );
};

RevisionCount.propTypes = {
  feature: PropTypes.object,
  meta: PropTypes.object
};

export default RevisionCount;
