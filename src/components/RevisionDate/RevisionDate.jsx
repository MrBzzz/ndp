import React, { PropTypes } from 'react';
import i18n from 'i18n/translation';
import Date from 'components/Date/Date';

const RevisionDate = ({ feature: { last_revision } }) => {
  let result = <small>{i18n.t('revision.notWatch')}</small>;
  if (last_revision) {
    result = (
      <small>{i18n.t('revision.watch')} <Date date={last_revision.updated} /></small>
    );
  }
  return result;
};

RevisionDate.propTypes = {
  feature: PropTypes.object,
  revision: PropTypes.object
};

export default RevisionDate;
