import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import classNames from 'classnames';
import i18n from 'i18n/translation';
import FldChoices from 'components/FldChoices/FldChoices';
import FldGeometry from 'components/FldGeometry/FldGeometry';
import FldText from 'components/FldText/FldText';
import Media from 'components/Media/Media';
import './RevisionForm.less';

const fields_names = [
  'geometry',
  'lat',
  'lng',
  'status',
  'monument_status',
  'user',
  'assigned_user',
  'text',
  'comment_last'
];

const fields_settings = {
  geometry: {
    readable: false,
    is_required: false,
  },
  lat: {
    readable: false,
    is_required: false,
  },
  lng: {
    readable: false,
    is_required: false,
  },
  status: {
    readable: false,
    is_required: false,
  },
  monument_status: {
    readable: false,
    is_required: false,
  },
  user: {
    readable: true,
    is_required: false,
  },
  assigned_user: {
    readable: false,
    is_required: false,
  },
  text: {
    readable: false,
    is_required: false,
  },
  comment_last: {
    readable: false,
    is_required: false,
  }
};

class RevisionForm extends Component {

  getFlds(schema_fields, fields, isUpdating, { properties: { lat, lng } }, onChangeRevisionCoordinates) {
    const { revision } = this.props;
    return fields_names.map((name, key) => {
      const schema = schema_fields[name] || '';
      const field = fields[name];
      const settings = fields_settings[name];
      const label = i18n.t(`form.${name}.label`);
      const placeholder = i18n.t(`form.${name}.placeholder`);
      switch (schema.type) {
        case 'geometry': {
          return (
            <FldGeometry
              settings={settings}
              revision={revision}
              onChangeRevisionCoordinates={onChangeRevisionCoordinates}
              name={name}
              label={label}
              placeholder={placeholder}
              lng={fields.lng}
              lat={fields.lat}
              coordinates={{ lat, lng }}
              isUpdating={isUpdating}
              key={key}
              copyCordsToMonument
            />
          );
        }
        case 'integer': {
          return (schema.choices) ? <FldChoices
            settings={settings}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            options={schema.choices}
            isUpdating={isUpdating}
            key={key}
          /> : null;
        }
        case 'foreign_key': {
          return (
            <FldChoices
              settings={settings}
              name={name}
              field={field}
              label={label}
              placeholder={placeholder}
              options={schema.choices}
              isUpdating={isUpdating}
              key={key}
            />
          );
        }
        case 'text': {
          return (
            <FldText
              settings={settings}
              name={name}
              field={field}
              label={label}
              placeholder={placeholder}
              isUpdating={isUpdating}
              key={key}
            />
          );
        }
        default: {
          return null;
        }
      }
    });
  }

  render() {
    const {
      revision,
      revision: { properties },
      btnText,
      fields,
      handleSubmit,
      isUpdating,
      onChangeRevisionCoordinates,
      onDropFiles,
      onRemoveFiles,
      onMoveFile,
      media = false
    } = this.props;
    const schema_fields = revision.properties.revision_schema.fields;
    const flds = this.getFlds(schema_fields, fields, isUpdating, revision, onChangeRevisionCoordinates);
    const btnClassName = classNames('button', 'is-info', {
      'is-loading': isUpdating
    });

    const mediaNode = media && (
      <Media
        properties={properties}
        onDropFiles={onDropFiles}
        onRemoveFiles={onRemoveFiles}
        onMoveFile={onMoveFile}
        moveFile
        editable
      />
    );

    return (
      <form onSubmit={handleSubmit}>
        {flds}
        {mediaNode}
        <button className={btnClassName} disabled={isUpdating}>{btnText}</button>
      </form>
    );
  }
}

RevisionForm.propTypes = {
  revision: PropTypes.object,
  onDropFiles: PropTypes.func,
  onRemoveFiles: PropTypes.func,
  onMoveFile: PropTypes.func,
  onChangeRevisionCoordinates: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fields: PropTypes.object.isRequired,
  submitting: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  media: PropTypes.bool.isRequired,
  btnText: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  return {
    initialValues: state.revisions.revision.properties
  };
};

export default reduxForm({
  form: 'revisionForm',
  fields: fields_names,
  overwriteOnInitialValuesChange: false,
}, mapStateToProps)(RevisionForm);
