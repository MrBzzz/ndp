import React, { PropTypes, Component } from 'react';
import i18n from 'i18n/translation';
import Title from 'components/Title/Title';
import DateTime from 'components/DateTime/DateTime';
import UserLink from 'components/UserLink/UserLink';

class RevisionHistory extends Component {

  renderStatus(item) {
    let statusNode = null;
    if (item.from_status.label !== item.to_status.label) {
      statusNode = (
        <p>
          <strong>{i18n.t('history.status')}:</strong>&nbsp;
          {i18n.t('history.from')}&nbsp;
          <em>{item.from_status.label}</em>&nbsp;
          {i18n.t('history.to')}&nbsp;
          <em>{item.to_status.label}</em>
        </p>
      );
    }
    return statusNode;
  }

  renderMonumentStatus(item) {
    let statusNode = null;
    if (item.from_monument_status.label !== item.to_monument_status.label) {
      statusNode = (
        <p>
          <strong>{i18n.t('history.monument_status')}:</strong>&nbsp;
          {i18n.t('history.from')}&nbsp;
          <em>{item.from_monument_status.label}</em>&nbsp;
          {i18n.t('history.to')}&nbsp;
          <em>{item.to_monument_status.label}</em>
        </p>
      );
    }
    return statusNode;
  }

  renderText(item, prev) {
    let textNode = null;
    if (item.text !== prev.text) {
      textNode = (<p>
        <strong>{i18n.t('history.text')}:</strong>&nbsp;
        {i18n.t('history.from')}&nbsp;
        <em>{prev.text}</em>&nbsp;
        {i18n.t('history.to')}&nbsp;
        <em>{item.text}</em>
      </p>);
    }
    return textNode;
  }

  renderComment(item, prev) {
    let commentNode = null;
    if (item.comment !== prev.comment) {
      commentNode = (<p>
        <strong>{i18n.t('history.comment')}:</strong>&nbsp;
        {i18n.t('history.from')}&nbsp;
        <em>{prev.comment === '' ? ' "Пустой комментарий" ' : prev.comment}</em>&nbsp;
        {i18n.t('history.to')}&nbsp;
        <em>{item.comment}</em>
      </p>);
    }
    return commentNode;
  }

  renderUser(item, prev) {
    let userNode = null;
    if (item.from_assigned_user && item.to_assigned_user && item.from_assigned_user.id !== item.to_assigned_user.id) {
      userNode = (
        <p>
          <strong>{i18n.t('history.assigned_user')}:</strong>&nbsp;
          {i18n.t('history.from')}&nbsp;
          <em><UserLink user={item.from_assigned_user} /></em>&nbsp;
          {i18n.t('history.to')}&nbsp;
          <em><UserLink user={item.to_assigned_user} /></em>
        </p>
      );
    }
    return userNode;
  }

  renderItem(item, key, prev, current) {

    if (!prev) return null;

    const prevItem = prev;

    if (
      item.from_status.label === item.to_status.label &&
      item.from_monument_status.label === item.to_monument_status.label &&
      item.text === prevItem.text &&
      item.comment === prevItem.comment &&
      item.assigned_user && prevItem.assigned_user && item.assigned_user.id === prevItem.assigned_user.id
    ) {
      return null;
    }


    return (
      <article key={key} className="message">
        <div className="message-body">
          <p><strong>{i18n.t('history.edit')}:</strong> <DateTime dateTime={item.created} /></p>
          {this.renderStatus(item)}
          {this.renderMonumentStatus(item)}
          {this.renderUser(item, prevItem)}
          {this.renderText(item, prevItem)}
          {this.renderComment(item, prevItem)}
        </div>
      </article>
    );
  }

  render() {
    const { history, current } = this.props;
    const new_history = history.map((item, key) => {

      if (key === 0) {
        item.from_assigned_user = item.assigned_user;
        item.to_assigned_user = current.assigned_user_object;
      } else {
        item.from_assigned_user = item.assigned_user;
        item.to_assigned_user = history[key-1].assigned_user;
      }

      return item;
    });

    return (
      <div>
        <Title text={i18n.t('history.title')} />
        {new_history.map((item, key) => this.renderItem(item, key, history[key + 1], current))}
      </div>
    );
  }
}

RevisionHistory.propTypes = {
  history: PropTypes.array,
  current: PropTypes.object,
};

export default RevisionHistory;
