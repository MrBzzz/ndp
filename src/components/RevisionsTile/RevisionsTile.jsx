import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import i18n from 'i18n/translation';
import StatusIcon from 'components/StatusIcon/StatusIcon';
import Date from 'components/Date/Date';
import StatusTag from 'components/StatusTag/StatusTag';
import './RevisionsTile.less';

const RevisionsTile = ({ revision: { properties } }) => {
  const statusList = properties.schema.fields.status.choices;
  const userNode = !!properties.assigned_user ? (
    <td>
      <span>{properties.assigned_user_data && i18n.t('revision.assigned')}</span>
      <span>{properties.assigned_user_data && properties.assigned_user_data.first_name}</span>
      <span>{properties.assigned_user_data && properties.assigned_user_data.last_name}</span>
    </td>
  ) : (
    <td></td>
  );

  return (
    <Link to={`/revision/${properties.id}`} className="box revision">
      <table>
        <tbody>
          <tr>
            <td rowSpan="2" className="revision__state">
              <StatusIcon className="" status={properties.monument_status} />
            </td>
            <td className="revision__name"><strong>{properties.monument_object.name}</strong></td>
            <td rowSpan="2" className="revision__status">
              <StatusTag value={properties.status} list={statusList} />
            </td>
            <td className="revision__updated">{i18n.t('revision.change')}<Date date={properties.updated} /></td>
          </tr>
          <tr>
            {userNode}
            <td className="revision__created">{i18n.t('revision.created')}<Date date={properties.created} /></td>
          </tr>
        </tbody>
      </table>
    </Link>
  );
};

RevisionsTile.propTypes = {
  revision: PropTypes.object
};

export default RevisionsTile;
