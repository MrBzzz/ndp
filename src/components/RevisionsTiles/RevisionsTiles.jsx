import React, { PropTypes } from 'react';
import RevisionsTile from 'components/RevisionsTile/RevisionsTile';
import Message from 'components/Message/Message';
import i18n from 'i18n/translation';
import './RevisionsTiles.less';

const RevisionsTiles = ({ features }) => (
  <div className="revisions">
    {!features.length && <Message text={i18n.t('monuments.empty')} />}
    {!!features.length && features.map((revision, key) => <RevisionsTile key={key} revision={revision} />)}
  </div>
);

RevisionsTiles.propTypes = {
  features: PropTypes.array
};

export default RevisionsTiles;
