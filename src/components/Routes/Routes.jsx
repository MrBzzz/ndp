import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { isEmpty } from 'lodash';
import { logout } from 'reducers/auth';

import NotFound from 'containers/NotFound/NotFound';
import Signin from 'containers/Signin/Signin';
import Works from 'containers/Works/Works';

import Home from 'containers/Home/Home';
import Monuments from 'containers/Monuments/Monuments';
import Monument from 'containers/Monument/Monument';
import MonumentEdit from 'containers/MonumentEdit/MonumentEdit';
import MonumentAddRevision from 'containers/MonumentAddRevision/MonumentAddRevision';
import Revisions from 'containers/Revisions/Revisions';
import Revision from 'containers/Revision/Revision';
import RevisionEdit from 'containers/RevisionEdit/RevisionEdit';
import Users from 'containers/Users/Users';
import User from 'containers/User/User';
import UserEdit from 'containers/UserEdit/UserEdit';
import MonumentsCreate from 'containers/MonumentsCreate/MonumentsCreate';
import MonumentsMap from 'containers/MonumentsMap/MonumentsMap';

const Routes = (store) => {
  const requireLogin = (next, replace, cb) => {
    const state = store.getState();
    if (isEmpty(state.auth.data)) {
      replace({
        pathname: '/signin',
        state: { nextPathname: next.location.pathname }
      });
    }
    cb();
  };
  const redirectIfLoggedIn = (next, replace, cb) => {
    const state = store.getState();
    if (!isEmpty(state.auth.data)) {
      replace({
        pathname: '/'
      });
    }
    cb();
  };
  const signout = () => {
    store.dispatch(logout());
  };
  return (
    <Route component={Home}>
      <Route path="/signin" component={Signin} onEnter={redirectIfLoggedIn} />
      <Route path="/" onEnter={requireLogin}>
        <IndexRoute component={Monuments} />
        <Route path="monument/map" component={MonumentsMap} />
        <Route path="monument/create" component={MonumentsCreate} />
        <Route path="monument/:monumentsId" component={Monument} />
        <Route path="monument/:monumentsId/edit" component={MonumentEdit} />
        <Route path="monument/:monumentsId/add-revision" component={MonumentAddRevision} />
        <Route path="revisions" component={Revisions} />
        <Route path="revision/:revisionsId" component={Revision} />
        <Route path="revision/:revisionsId/edit" component={RevisionEdit} />
        <Route path="works" component={Works} />
        <Route path="users" component={Users} />
        <Route path="user/:userId" component={User} />
        <Route path="user/:userId/edit" component={UserEdit} />
        <Route path="signout" onEnter={signout} />
      </Route>
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};

export default Routes;
