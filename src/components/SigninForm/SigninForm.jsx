import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import classNames from 'classnames';
import i18n from 'i18n/translation';
import signinFormValidation from './SigninFormValidation';
import { login } from 'reducers/auth';
import Error from 'components/Error/Error';
import './SigninForm.less';

const form = reduxForm({
  form: 'signinForm',
  fields: ['username', 'password'],
  validate: signinFormValidation
});

class Signin extends Component {

  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(data) {
    return new Promise((resolve, reject) => {
      const loginResult = this.props.login(data);
      loginResult
        .then(() => resolve())
        .catch(() => reject({ _error: i18n.t('signin.errors.403') }));
    });
  }

  render() {
    const { fields: { username, password }, handleSubmit, submitting, error } = this.props;
    const btnClassName = classNames('button', 'is-success', {
      'is-loading': submitting,
      'is-disabled': username.error || password.error
    });

    return (
      <div className="hero is-fullheight">
        <div className="hero-body">
          <div className="container">
            <div className="columns">
              <div className="column is-one-third is-offset-one-third">
                <article className="message signin">
                  <form onSubmit={handleSubmit(this.submitForm)}>
                    <div className="message-header">{i18n.t('signin.title')}</div>
                    <div className="message-body">
                      <p className="control">
                        <input
                          className="input"
                          type="text"
                          disabled={submitting}
                          placeholder={i18n.t('signin.placeholders.username')}
                          {...username}
                        />
                      </p>
                      <p className="control">
                        <input
                          className="input"
                          type="password"
                          disabled={submitting}
                          placeholder={i18n.t('signin.placeholders.password')}
                          { ...password }
                        />
                      </p>
                      <Error error={error} />
                      <p className="control">
                        <button
                          className={btnClassName}
                          disabled={submitting || username.error || password.error}
                        >
                          {i18n.t('signin.btn.login')}
                        </button>
                      </p>
                    </div>
                  </form>

                </article>
                <div className="has-text-centered">
                  <a href="/register/" target="_blank">Регистрация</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Signin.propTypes = {
  fields: PropTypes.object.isRequired,
  resetForm: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  login: PropTypes.func,
  error: PropTypes.string
};


export default connect(() => ({}), { login })(form(Signin));
