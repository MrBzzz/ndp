import memoize from 'lru-memoize';
import { createValidator, required } from 'helpers/validation';

const surveyValidation = createValidator({
  username: [required],
  password: [required]
});

export default memoize(10)(surveyValidation);
