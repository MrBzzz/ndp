import React, { PropTypes } from 'react';
import classNames from 'classnames';
import starIconSvg from 'svg/star.svg';
import './StatusIcon.less';

const StatusIcon = ({ status, className }) => {
  const iconClass = classNames('status__icon', className, {
    status__icon_without: status === 0,
    status__icon_unsatisfactorily: status === 1,
    status__icon_satisfactorily: status === 2
  });
  return (
    <sapn
      className={iconClass}
      dangerouslySetInnerHTML={{ __html: starIconSvg }}
    ></sapn>
  );
};

StatusIcon.propTypes = {
  className: PropTypes.string,
  status: PropTypes.number
};

export default StatusIcon;
