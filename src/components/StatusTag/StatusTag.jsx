import React, { PropTypes } from 'react';
import classNames from 'classnames';

const StatusTag = ({ value, list }) => {
  const tegClass = classNames('tag', {
    'is-primary': value === 1,
    'is-success': value === 2,
    'is-info': value === 3
  });
  return <span className={tegClass}>{list[value].label}</span>;
};

StatusTag.propTypes = {
  value: PropTypes.number,
  list: PropTypes.array
};

export default StatusTag;
