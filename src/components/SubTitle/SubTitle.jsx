import React, { PropTypes } from 'react';

const SubTile = ({ text, buttons = [] }) => (
  <div className="columns is-multiline is-mobile">
    <div className="column">
      <h3 className="subtitle is-3">{text}</h3>
    </div>
    <div className="column is-one-quarter has-text-right">
      {buttons}
    </div>
  </div>
);

SubTile.propTypes = {
  text: PropTypes.string,
  buttons: PropTypes.array
};

export default SubTile;
