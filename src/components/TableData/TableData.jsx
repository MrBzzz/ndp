import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import Date from 'components/Date/Date';
import DateTime from 'components/DateTime/DateTime';
import UserLink from 'components/UserLink/UserLink';
import i18n from 'i18n/translation';

class TableData extends Component {

  renderTextRow(fld, key) {
    const { properties } = this.props;
    const value = `${properties[fld.name]}` || '';
    return this.renderCell(fld, key, value);
  }

  renderBoolRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? i18n.t('tables.variants.yes') : i18n.t('tables.variants.no');
    return this.renderCell(fld, key, value);
  }

  renderForeignKeyRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? properties[fld.name].name : '';
    return this.renderCell(fld, key, value);
  }

  renderLinkRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? <Link to={fld.to}>{properties[fld.name]}</Link> : '';
    return this.renderCell(fld, key, value);
  }

  renderIntegerChoicesRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? properties[fld.name].label : '';
    return this.renderCell(fld, key, value);
  }

  renderDateTimeRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? <DateTime dateTime={properties[fld.name]} /> : '';
    return this.renderCell(fld, key, value);
  }

  renderDateRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? <Date date={properties[fld.name]} /> : '';
    return this.renderCell(fld, key, value);
  }

  renderUserRow(fld, key) {
    const { properties } = this.props;
    const value = properties[fld.name] ? <UserLink user={properties[fld.name]} /> : '';
    return this.renderCell(fld, key, value);
  }

  renderCell(fld, key, value) {
    return (
      <tr key={key}>
        <td><strong>{fld.label}</strong>: {value}</td>
      </tr>
    );
  }

  renderRow(fld, key) {
    let row = null;
    switch (fld.type) {
      case 'datetime': {
        row = this.renderDateTimeRow(fld, key);
        break;
      }
      case 'date': {
        row = this.renderDateRow(fld, key);
        break;
      }
      case 'text': {
        row = this.renderTextRow(fld, key);
        break;
      }
      case 'bool': {
        row = this.renderBoolRow(fld, key);
        break;
      }
      case 'foreign_key': {
        row = this.renderForeignKeyRow(fld, key);
        break;
      }
      case 'link': {
        row = this.renderLinkRow(fld, key);
        break;
      }
      case 'user': {
        row = this.renderUserRow(fld, key);
        break;
      }
      case 'integer_choices': {
        row = this.renderIntegerChoicesRow(fld, key);
        break;
      }
      default: {
        break;
      }
    }
    return row;
  }

  render() {
    const { fields } = this.props;
    return (
      <table className="table is-narrow">
        <tbody>
          {fields.map((fld, key) => this.renderRow(fld, key))}
        </tbody>
      </table>
    );
  }

}

TableData.propTypes = {
  properties: PropTypes.object,
  fields: PropTypes.array,
};

export default TableData;
