import React, { PropTypes } from 'react';

import PhotoCount from 'components/PhotoCount/PhotoCount';
import MonumentState from 'components/MonumentState/MonumentState';
import RevisionCount from 'components/RevisionCount/RevisionCount';

const TileMeta = ({ imagesCount, feature }) => (
  <div className="cards-meta">
    <PhotoCount count={imagesCount} />
    <MonumentState feature={feature} />
    <RevisionCount feature={feature} />
  </div>
);

TileMeta.propTypes = {
  imagesCount: PropTypes.number,
  feature: PropTypes.object
};

export default TileMeta;
