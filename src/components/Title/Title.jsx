import React, { PropTypes } from 'react';

const Tile = ({ text, buttons = [] }) => (
  <div className="columns is-multiline is-mobile">
    <div className="column">
      <h3 className="title is-3">{text}</h3>
    </div>
    <div className="buttons column is-one-quarter has-text-right">
      {buttons}
    </div>
  </div>
);

Tile.propTypes = {
  text: PropTypes.any,
  buttons: PropTypes.array
};

export default Tile;
