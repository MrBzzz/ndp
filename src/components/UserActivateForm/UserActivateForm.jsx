import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { isNull, isUndefined } from 'lodash';
import i18n from 'i18n/translation';
import FldVarchar from 'components/FldVarchar/FldVarchar';
import FldChoices from 'components/FldChoices/FldChoices';

const form_fields = [
  {
    name: 'email',
    type: 'varchar',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'username',
    type: 'varchar',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'first_name',
    type: 'varchar',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'last_name',
    type: 'varchar',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
  {
    name: 'position',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'organization',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'phone',
    type: 'varchar',
    options: {
      is_readable: false,
      is_required: false,
    }
  },
  {
    name: 'types',
    type: 'foreign_key',
    options: {
      is_readable: false,
      is_required: true,
    }
  },
  {
    name: 'locations',
    type: 'foreign_key',
    options: {
      is_readable: true,
      is_required: false,
    }
  },
];

const validate = values => {
  const errors = {};
  if (isUndefined(values.types) || isNull(values.types) || values.types.length === 0) {
    errors.types = 'Нужно выбрать хотябы один тип пользователя!';
  }
  return errors;
};

class UserActivateForm extends Component {

  renderField(fld, key) {
    const { fields } = this.props;
    const { name, type, options } = fld;
    const field = fields[name];
    const label = i18n.t(`forms.user.${name}.label`);
    const placeholder = i18n.t(`forms.user.${name}.placeholder`);

    switch (type) {
      case 'varchar': {
        return (
          <FldVarchar
            settings={options}
            name={name}
            field={field}
            label={label}
            placeholder={placeholder}
            isUpdating={false}
            key={key}
          />
        );
      }
      case 'foreign_key': {
        const { user: { choices } } = this.props;
        const list = choices[name];
        return (
          <FldChoices
            multi
            field={field}
            key={key}
            settings={options}
            name={name}
            label={label}
            placeholder={placeholder}
            options={list}
            isUpdating={options.is_readable}
          />
        );
      }
      default: {
        return null;
      }
    }
  }

  renderFields() {
    return form_fields.map((fld, key) => this.renderField(fld, key));
  }

  render() {
    const { handleSubmit, currentUser: { objects: [currentUser] }, fields: { types } } = this.props;
    let formNode = null;
    if (currentUser.is_master || currentUser.is_superuser) {
      formNode = (
        <form onSubmit={handleSubmit} className="monument-form">
          {this.renderFields()}
          {types.touched && types.error && <div className="notification is-danger">{types.error}</div>}
          <p className="has-text-centered">
            <button className="button is-primary">Активировать</button>
          </p>
        </form>
      );
    }
    return formNode;
  }

}

UserActivateForm.propTypes = {
  user: PropTypes.object,
  currentUser: PropTypes.object,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  fields_names: PropTypes.array,
};

const mapStateToProps = state => ({
  initialValues: state.users.user
});

export default reduxForm({
  form: 'user-activate-form',
  fields: form_fields.map(item => item.name),
  validate,
}, mapStateToProps)(UserActivateForm);
