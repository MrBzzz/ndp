import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import Select from 'react-select';
import i18n from 'i18n/translation';
import './UserEditForm.less';

const form_fields = [
  'first_name',
  'last_name',
  'position',
  'organization',
  'phone',
  'is_active',
  'types',
  'locations',
  'password',
];

class UserEditForm extends Component {

  render() {
    const {
      handleSubmit, fields, fields: { types, locations },
      user, currentUser,
      user: { valid_operations: { can_update_groups, can_update_props } }
    } = this.props;

    const userActiveNode = (
      <div className="control">
        <p className="control">
          <label className="checkbox">
            <input type="checkbox" {...fields.is_active} /> Активен
          </label>
        </p>
      </div>
    );

    return (
      <form onSubmit={handleSubmit}>
        <div className="control">
          <label className="label">Электронная почта</label>
          <p className="control">
            <input className="input" type="text" disabled value={user.email} />
          </p>
        </div>
        <div className="control">
          <label className="label">Имя пользователя</label>
          <p className="control">
            <input className="input" type="text" disabled value={user.username} />
          </p>
        </div>
        <div className="control">
          <label className="label">Имя</label>
          <p className="control">
            <input className="input" type="text" {...fields.first_name} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label">Фамилия</label>
          <p className="control">
            <input className="input" type="text" {...fields.last_name} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label">Пароль</label>
          <p className="control">
            <input className="input" type="password" placeholder="Пароль" value={'пароль'} {...fields.password} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label">{i18n.t('user.organization')}</label>
          <p className="control">
            <input className="input" type="text" {...fields.organization} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label">{i18n.t('user.position')}</label>
          <p className="control">
            <input className="input" type="text" {...fields.position} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label">{i18n.t('user.phone')}</label>
          <p className="control">
            <input className="input" type="text" {...fields.phone} disabled={!can_update_props} />
          </p>
        </div>
        <div className="control">
          <label className="label strong">Тип пользователя</label>
          <Select
            multi
            clearable={false}
            searchable
            name="types"
            className="form-control"
            placeholder="Тип пользователя"
            {...types}
            value={types.value}
            options={user.choices.types}
            onBlur={() => types.onBlur({ value: types.value })}
            disabled={!can_update_groups}
          />
        </div>
        <div className="control">
          <label className="label strong">Локация</label>
          <Select
            multi
            searchable
            name="location"
            className="form-control"
            placeholder="Локация"
            {...locations}
            value={locations.value}
            options={user.choices.locations}
            onBlur={() => locations.onBlur({ value: locations.value })}
            disabled={!currentUser.is_superuser}
          />
        </div>
        {(currentUser.is_master || currentUser.is_superuser) && userActiveNode}
        <button className="button is-info">Сохранить</button>
      </form>
    );
  }
}

UserEditForm.propTypes = {
  user: PropTypes.object,
  currentUser: PropTypes.object,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  initialValues: state.users.user
});

export default reduxForm({
  form: 'revisionForm',
  fields: form_fields
}, mapStateToProps)(UserEditForm);
