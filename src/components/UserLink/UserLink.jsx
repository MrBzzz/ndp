import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const UserLink = ({ user }) => {
  let rol = '';
  if (user.is_ballansholder) {
    rol = 'Балансодержатель';
  } else if (user.is_master) {
    rol = 'Руководитель';
  } else if (user.is_moderator) {
    rol = 'Модератор';
  } else if (user.is_volunteer) {
    rol = 'Волонтер';
  } else if (user.is_superuser) {
    rol = 'Суперпользователь';
  }

  let text = '';
  text += user.first_name && `${user.first_name} `;
  text += user.last_name && `${user.last_name} `;
  text += `(${rol}) ${user.email}`;
  return (
    <Link to={`/user/${user.id}`}>{text}</Link>
  );
};

UserLink.propTypes = {
  user: PropTypes.object
};

export default UserLink;
