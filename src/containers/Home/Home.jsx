import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import Navbar from 'components/Navbar/Navbar';

class Home extends Component {
  componentWillReceiveProps(next) {
    const { currentUser } = this.props;
    if (isEmpty(currentUser) && !isEmpty(next.currentUser)) {
      const url = next.location.state && next.location.state.nextPathname || '/';
      browserHistory.replace(url);
    } else if (!isEmpty(currentUser) && isEmpty(next.currentUser)) {
      browserHistory.replace('/signin');
    }
  }
  renderNavbar() {
    const { currentUser } = this.props;
    if (isEmpty(currentUser)) return null;
    return <Navbar user={currentUser} />;
  }
  render() {
    const { children } = this.props;
    return (
      <div>
        {this.renderNavbar()}
        {children}
      </div>
    );
  }
}

Home.propTypes = {
  currentUser: PropTypes.object,
  children: PropTypes.object
};

const mapStateToProps = (state) => ({
  currentUser: state.auth.data
});

export default connect(mapStateToProps)(Home);
