import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Collapse from 'react-collapse';
import Tooltip from 'rc-tooltip';
import i18n from 'i18n/translation';
import { getMonument, removeMonument, toggleMap } from 'reducers/monuments';
import Loader from 'components/Loader/Loader';
import Bar from 'components/Bar/Bar';
import Title from 'components/Title/Title';
import Map from 'components/Map/Map';
import Media from 'components/Media/Media';
import TableData from 'components/TableData/TableData';
import SubTitle from 'components/SubTitle/SubTitle';
import RevisionsTiles from 'components/RevisionsTiles/RevisionsTiles';
import { special_kinds } from 'settings/settings.js';
import './Monument.less';

class Monument extends Component {

  constructor(props) {
    super(props);
    this.onRemoveMonument = this.onRemoveMonument.bind(this);
    this.onToggleMap = this.onToggleMap.bind(this);
  }

  componentWillMount() {
    this.props.getMonument({
      id: this.props.routeParams.monumentsId
    });
  }

  onRemoveMonument() {
    const { monument: { properties: { id } } } = this.props;
    this.props.removeMonument({ id });
  }

  onToggleMap() {
    this.props.toggleMap();
  }

  renderBar() {
    const { monument: { properties: { name } } } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <span>{name}</span>
    ];
    return <Bar breadcrumbs={breadcrumbs} />;
  }

  renderRevisions() {
    const { monument: { properties, properties: { id, valid_operations } } } = this.props;
    let addRevisionBtn = (
      <Tooltip key="add-revision" placement="leftTop" overlay={<span>{i18n.t('tooltip.add_revision')}</span>}>
        <span><button className="button is-primary is-disabled">{i18n.t('buttons.add_revision')}</button></span>
      </Tooltip>
    );
    if (valid_operations.can_create_revision) {
      addRevisionBtn = (
        <Link key="add-revision" className="button is-primary" to={`/monument/${id}/add-revision`}>
          {i18n.t('buttons.add_revision')}
        </Link>
      );
    }
    return (
      <div>
        <SubTitle text={i18n.t('revisions.title')} buttons={[addRevisionBtn]} />
        <RevisionsTiles features={properties.revisions.features} />
      </div>
    );
  }

  renderMonument() {
    const { mapIsOpen, monument, monument: { properties, properties: { id, name } } } = this.props;
    const toggleMapBtnText = mapIsOpen ? i18n.t('buttons.hide_map') : i18n.t('buttons.show_map');
    const buttons = [
      <button key="show-map" onClick={this.onToggleMap} className="button is-link">{toggleMapBtnText}</button>,
      (properties.valid_operations.can_update && <Link key="monument-edit" to={`/monument/${id}/edit`} className="button is-primary">{i18n.t('buttons.edit')}</Link>)
    ];

    let fields = [
      {
        name: 'id',
        type: 'text',
        label: i18n.t('tables.monument_id')
      },
      {
        name: 'name',
        type: 'text',
        label: i18n.t('tables.monument_name')
      },
      {
        name: 'code',
        type: 'text',
        label: i18n.t('tables.monument_code')
      },
      {
        name: 'location_object',
        type: 'foreign_key',
        label: i18n.t('tables.monument_location')
      },
      {
        name: 'kind_object',
        type: 'foreign_key',
        label: i18n.t('tables.monument_kind')
      },
      {
        name: 'created',
        type: 'datetime',
        label: i18n.t('tables.monument_created')
      },
      {
        name: 'updated',
        type: 'datetime',
        label: i18n.t('tables.monument_updated')
      },
      {
        name: 'is_published',
        type: 'bool',
        label: i18n.t('tables.monument_is_published')
      },
      {
        name: 'is_published_on_geoportal',
        type: 'bool',
        label: i18n.t('tables.monument_is_published_on_geoportal')
      },
      {
        name: 'status_object',
        type: 'foreign_key',
        label: i18n.t('tables.monument_current_state_status')
      },
      {
        name: 'address',
        type: 'text',
        label: i18n.t('tables.monument_address')
      },
      {
        name: 'address_more',
        type: 'text',
        label: i18n.t('tables.monument_address_more')
      },
      {
        name: 'history_info',
        type: 'text',
        label: i18n.t('tables.monument_history_info')
      },
      {
        name: 'owner',
        type: 'text',
        label: i18n.t('tables.monument_owner')
      },
      {
        name: 'owner_address',
        type: 'text',
        label: i18n.t('tables.monument_owner_address')
      },
      {
        name: 'responsible_person_1',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_1')
      },
      {
        name: 'responsible_person_phone_1',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_phone_1')
      },
      {
        name: 'responsible_person_email_1',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_email_1')
      },
      {
        name: 'responsible_person_2',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_2')
      },
      {
        name: 'responsible_person_phone_2',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_phone_2')
      },
      {
        name: 'responsible_person_email_2',
        type: 'text',
        label: i18n.t('tables.monument_responsible_person_email_2')
      },
    ];

    const special_fields = [
      {
        name: 'number_of_dead',
        type: 'text',
        label: i18n.t('tables.monument_number_of_dead')
      },
      {
        name: 'number_of_dead_known',
        type: 'text',
        label: i18n.t('tables.monument_number_of_dead_known')
      },
      {
        name: 'number_of_dead_unknown',
        type: 'text',
        label: i18n.t('tables.monument_number_of_dead_unknown')
      },

    ];

    if (~special_kinds.indexOf(properties.kind)) {
      fields = fields.concat(special_fields);
    }

    return (
      <div className="section">
        <div className="container">
          {this.renderBar()}
          <Title text={name} buttons={buttons} />
          <Collapse isOpened={mapIsOpen}>
            <Map features={[monument]} />
          </Collapse>
          <TableData properties={properties} fields={fields} />
          <Media properties={properties} editable={false} />
          {this.renderRevisions()}
        </div>
      </div>
    );
  }

  render() {
    const { monument } = this.props;
    if (isEmpty(monument)) return <Loader />;
    return (
      <div className="monument">
        {this.renderMonument()}
      </div>
    );
  }

}

Monument.propTypes = {
  monument: PropTypes.object,
  routeParams: PropTypes.object.isRequired,
  getMonument: PropTypes.func.isRequired,
  removeMonument: PropTypes.func.isRequired,
  toggleMap: PropTypes.func.isRequired,
  mapIsOpen: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  monument: state.monuments.monument,
  mapIsOpen: state.monuments.mapIsOpen,
});

export default connect(mapStateToProps, {
  removeMonument,
  getMonument,
  toggleMap,
})(Monument);
