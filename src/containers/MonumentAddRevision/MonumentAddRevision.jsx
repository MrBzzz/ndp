import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getDataForNewRevision, changeRevisionCoordinates, createRevision } from 'reducers/revisions';
import { isEmpty } from 'lodash';
import { Link, browserHistory } from 'react-router';
import i18n from 'i18n/translation';
import Loader from 'components/Loader/Loader';
import Bar from 'components/Bar/Bar';
import Title from 'components/Title/Title';
import TableData from 'components/TableData/TableData';
import RevisionForm from 'components/RevisionForm/RevisionForm';


class MonumentAddRevision extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeRevisionCoordinates = this.onChangeRevisionCoordinates.bind(this);
  }

  componentWillMount() {
    this.props.getDataForNewRevision({
      id: this.props.routeParams.monumentsId,
      currentUser: this.props.currentUser
    });
  }

  componentWillReceiveProps(newProps) {
    const { routeParams: { monumentsId } } = this.props;
    const oldIsUpdating = this.props.isUpdating;
    const newIsUpdating = newProps.isUpdating;
    if (oldIsUpdating && !newIsUpdating) {
      browserHistory.replace(`/monument/${monumentsId}/`);
    }
  }

  onSubmit(data) {
    const { revision: { properties: { monument_object: { id } } } } = this.props;
    const req = {
      ...data,
      monument: id,
      geometry: {
        type: 'Point',
        coordinates: [parseFloat(data.lng), parseFloat(data.lat)],
      }
    };
    this.props.createRevision(req);
  }

  onChangeRevisionCoordinates(data) {
    this.props.changeRevisionCoordinates(data);
  }

  renderAddRevision() {
    const { revision, isUpdating = false, revision: { properties: { monument_object, monument_object: { id, name } } } } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <Link to="/revisions">{i18n.t('breadcrumbs.revisions')}</Link>,
      <Link to={`/monument/${id}/`}>{name}</Link>,
      <span>{i18n.t('breadcrumbs.add_revision')}</span>
    ];
    const monument_fields = [
      {
        name: 'name',
        type: 'link',
        to: `/monument/${monument_object.id}`,
        label: i18n.t('tables.revision_monument_object_name'),
      }, {
        name: 'kind_object',
        type: 'foreign_key',
        label: i18n.t('tables.revision_monument_object_kind'),
      }, {
        name: 'revisions_total_count',
        type: 'text',
        label: i18n.t('tables.revision_monument_object_revisions_total_count'),
      },
    ];
    return (
      <div className="section">
        <div className="container">
          <Bar breadcrumbs={breadcrumbs} />
          <Title text={name} />
          <TableData properties={monument_object} fields={monument_fields} />
          <RevisionForm
            onSubmit={this.onSubmit}
            onChangeRevisionCoordinates={this.onChangeRevisionCoordinates}
            revision={revision}
            isUpdating={isUpdating}
            media={false}
            btnText={i18n.t('buttons.create')}
          />
        </div>
      </div>
    );
  }

  render() {
    const { revision } = this.props;
    if (isEmpty(revision)) return <Loader />;
    return (
      <div className="add-revision">
        {this.renderAddRevision()}
      </div>
    );
  }

}

MonumentAddRevision.propTypes = {
  revision: PropTypes.object,
  currentUser: PropTypes.object,
  isUpdating: PropTypes.bool.isRequired,
  routeParams: PropTypes.object.isRequired,
  getDataForNewRevision: PropTypes.func.isRequired,
  changeRevisionCoordinates: PropTypes.func.isRequired,
  createRevision: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  revision: state.revisions.revision,
  currentUser: state.auth.data,
  isUpdating: state.revisions.updating,
});

export default connect(mapStateToProps, {
  changeRevisionCoordinates,
  getDataForNewRevision,
  createRevision,
})(MonumentAddRevision);
