import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { getMonument, changeCoordinates, updateMonument, removeMonumentMedia, uploadMonumentMedia, onChangeSortFiles } from 'reducers/monuments';
import i18n from 'i18n/translation';
import Bar from 'components/Bar/Bar';
import Loader from 'components/Loader/Loader';
import Title from 'components/Title/Title';
import MonumentForm from 'components/MonumentForm/MonumentForm';
import { special_kinds } from 'settings/settings.js';


class MonumentEdit extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeCoordinates = this.onChangeCoordinates.bind(this);
    this.onDropFiles = this.onDropFiles.bind(this);
    this.onRemoveFiles = this.onRemoveFiles.bind(this);
    this.onChangeSortFiles = this.onChangeSortFiles.bind(this);
  }

  componentWillMount() {
    this.props.getMonument({
      id: this.props.routeParams.monumentsId
    });
  }

  componentWillReceiveProps(newProps) {
    const { monument: { properties } } = this.props;
    const oldIsUpdating = this.props.is_updating;
    const newIsUpdating = newProps.is_updating;
    if (oldIsUpdating && !newIsUpdating) {
      browserHistory.replace(`/monument/${properties.id}/`);
    }
  }

  onSubmit(data) {
    const { monument: { properties: { id, current_state: { status } } }, currentUserId } = this.props;
    const req = {
      ...data,
      user: currentUserId,
      id,
      old_current_state_status: status,
      geometry: {
        coordinates: [parseFloat(data.lng), parseFloat(data.lat)],
        type: 'Point'
      }
    };
    this.props.updateMonument(req);
  }

  onChangeCoordinates(data) {
    this.props.changeCoordinates(data);
  }

  onDropFiles(files) {
    const { monument: { properties: { id } } } = this.props;
    this.props.uploadMonumentMedia({
      files,
      id
    });
  }

  onRemoveFiles(media) {
    this.props.removeMonumentMedia(media);
  }

  onChangeSortFiles(media) {
    const { monument: { properties: { images, id } } } = this.props;
    this.props.onChangeSortFiles({
      main: media,
      all: images,
      id,
    });
  }

  renderMonument() {
    const { monument, is_updating, monument: { properties: { id, name, kind } }, } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <Link to={`/monument/${id}`}>{name}</Link>,
      <span>{i18n.t('breadcrumbs.edit')}</span>
    ];

    let fields_names = [
      'geometry',
      'lat',
      'lng',
      'id',
      'name',
      'formatted_created',
      'formatted_updated',
      'code',
      'is_published',
      'is_published_on_geoportal',
      'location',
      'current_state_status',
      'location',
      'kind',
      'address',
      'address_more',
      'build_date',
      'history_info',
      'owner',
      'owner_address',
      'responsible_person_1',
      'responsible_person_phone_1',
      'responsible_person_email_1',
      'responsible_person_2',
      'responsible_person_phone_2',
      'responsible_person_email_2',
    ];

    const special_fields_names = [
      'number_of_dead',
      'number_of_dead_known',
      'number_of_dead_unknown',
    ];

    if (~special_kinds.indexOf(kind)) {
      fields_names = fields_names.concat(special_fields_names);
    }

    return (
      <div className="section">
        <div className="container">
          <Bar breadcrumbs={breadcrumbs} />
          <Title text={name} />
          <MonumentForm
            onChangeSortFiles={this.onChangeSortFiles}
            onSubmit={this.onSubmit}
            onDropFiles={this.onDropFiles}
            onRemoveFiles={this.onRemoveFiles}
            onChangeCoordinates={this.onChangeCoordinates}
            monument={monument}
            is_updating={is_updating}
            textSubmitBtn={i18n.t('buttons.save')}
            fields_names={fields_names}
          />
        </div>
      </div>
    );
  }

  render() {
    const { monument } = this.props;
    if (isEmpty(monument)) return <Loader />;
    return (
      <div className="monument">
        {this.renderMonument()}
      </div>
    );
  }

}

MonumentEdit.propTypes = {
  monument: PropTypes.object,
  currentUserId: PropTypes.number,
  is_updating: PropTypes.bool,
  routeParams: PropTypes.object.isRequired,
  getMonument: PropTypes.func.isRequired,
  changeCoordinates: PropTypes.func.isRequired,
  updateMonument: PropTypes.func.isRequired,
  removeMonumentMedia: PropTypes.func.isRequired,
  uploadMonumentMedia: PropTypes.func.isRequired,
  onChangeSortFiles: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  monument: state.monuments.monument,
  is_updating: state.monuments.updating,
  currentUserId: state.auth.data.objects[0].id
});

export default connect(mapStateToProps, {
  onChangeSortFiles,
  uploadMonumentMedia,
  removeMonumentMedia,
  getMonument,
  changeCoordinates,
  updateMonument
})(MonumentEdit);
