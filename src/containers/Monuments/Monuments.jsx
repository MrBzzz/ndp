import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { isEmpty, isEqual } from 'lodash';
import { Link } from 'react-router';
import Collapse from 'react-collapse';
import { getMonuments, toggleMap } from 'reducers/monuments';
import i18n from 'i18n/translation';
import Loader from 'components/Loader/Loader';
import Filters from 'components/FiltersMonuments/FiltersMonuments';
import Map from 'components/Map/Map';
import CardsList from 'components/CardsList/CardsList';
import Pagination from 'components/Pagination/Pagination';
import Title from 'components/Title/Title';

class Monuments extends Component {

  constructor(props) {
    super(props);
    this.onToggleMap = this.onToggleMap.bind(this);
  }

  componentWillMount() {
    this.props.getMonuments(this.props.query);
  }

  componentWillReceiveProps(next) {
    const prevQuery = next.query;
    const currentQuery = this.props.query;
    if (!isEqual(prevQuery, currentQuery)) {
      this.props.getMonuments(next.query);
    }
  }

  onToggleMap() {
    this.props.toggleMap();
  }

  renderLoader() {
    return <Loader isShown={isEmpty(this.props.monuments)} />;
  }

  renderTitle() {
    const { monuments: { meta: { total_count } } } = this.props;

    const text = (
      <span>
        <span>{i18n.t('monuments.title')} ({total_count})&nbsp;</span>
        <Link key="monument-create" to={'/monument/create'} className="button is-link">{i18n.t('buttons.create')}</Link>
      </span>
    );

    return <Title text={text} />;
  }


  renderSection() {
    const { mapIsOpen, monuments, monuments: { features, filters, meta }, query } = this.props;
    const toggleMapBtnText = mapIsOpen ? i18n.t('buttons.hide_map') : i18n.t('buttons.show_map');
    if (isEmpty(monuments)) return null;
    return (
      <div className="section">
        <div className="container">
          {this.renderTitle()}
          <Filters
            query={query}
            filters={filters}
            buttons={[
              <button
                key="show-map"
                onClick={this.onToggleMap}
                className="button is-link"
              >
                {toggleMapBtnText}
              </button>
            ]}
          />

          {!!features.length && (<Collapse isOpened={mapIsOpen}><Map features={features} /></Collapse>)}
          <CardsList features={features} count={3} />
          {!!features.length && <Pagination meta={meta} query={query} pathname="/" />}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderSection()}
        {this.renderLoader()}
      </div>
    );
  }
}

Monuments.propTypes = {
  getMonuments: PropTypes.func.isRequired,
  monuments: PropTypes.object,
  query: PropTypes.object,
  toggleMap: PropTypes.func.isRequired,
  mapIsOpen: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => ({
  mapIsOpen: state.monuments.mapIsOpen,
  monuments: state.monuments.monuments,
  query: ownProps.location.query,
});

export default connect(mapStateToProps, {
  toggleMap,
  getMonuments,
})(Monuments);
