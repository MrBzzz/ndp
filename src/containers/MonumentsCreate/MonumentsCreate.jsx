import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import i18n from 'i18n/translation';
import { changeCoordinates, getDataForNewMonument, createMonument } from 'reducers/monuments';
import Bar from 'components/Bar/Bar';
import Loader from 'components/Loader/Loader';
import MonumentForm from 'components/MonumentForm/MonumentForm';

class MonumentsCreate extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeCoordinates = this.onChangeCoordinates.bind(this);
  }

  componentWillMount() {
    this.props.getDataForNewMonument();
  }

  componentWillReceiveProps(newProps) {
    if (!isEmpty(newProps.newMonument)) {
      browserHistory.replace(`/monument/${newProps.newMonument.properties.id}`);
    }
  }

  onSubmit(data) {
    const req = {
      ...data,
      geometry: {
        coordinates: [parseFloat(data.lng), parseFloat(data.lat)],
        type: 'Point'
      }
    };
    this.props.createMonument(req);
  }

  onChangeCoordinates(data) {
    this.props.changeCoordinates(data);
  }

  renderMonument() {
    const { monument, is_updating } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <span>{i18n.t('breadcrumbs.add_monument')}</span>
    ];
    const fields_names = [
      'geometry',
      'lat',
      'lng',
      'name',
      'code',
      'is_published',
      'is_published_on_geoportal',
      'location',
      'current_state_status',
      'location',
      'kind',
      'address',
      'address_more',
      'number_of_dead',
      'number_of_dead_known',
      'number_of_dead_unknown',
      'history_info',
      'owner',
      'owner_address',
      'responsible_person_1',
      'responsible_person_phone_1',
      'responsible_person_email_1',
      'responsible_person_2',
      'responsible_person_phone_2',
      'responsible_person_email_2',
    ];
    return (
      <div className="section">
        <div className="container">
          <Bar breadcrumbs={breadcrumbs} />
          <MonumentForm
            onSubmit={this.onSubmit}
            onChangeCoordinates={this.onChangeCoordinates}
            monument={monument}
            files={false}
            is_updating={is_updating}
            textSubmitBtn={i18n.t('buttons.create')}
            fields_names={fields_names}
          />
        </div>
      </div>
    );
  }

  render() {
    const { monument } = this.props;
    if (isEmpty(monument)) return <Loader />;
    return (
      <div className="monument">
       {this.renderMonument()}
      </div>
    );
  }

}

MonumentsCreate.propTypes = {
  getDataForNewMonument: PropTypes.func.isRequired,
  createMonument: PropTypes.func.isRequired,
  changeCoordinates: PropTypes.func.isRequired,
  monument: PropTypes.object.isRequired,
  is_updating: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  monument: state.monuments.monument,
  is_updating: state.monuments.updating,
  newMonument: state.monuments.newMonument
});

export default connect(mapStateToProps, {
  getDataForNewMonument,
  changeCoordinates,
  createMonument,
})(MonumentsCreate);
