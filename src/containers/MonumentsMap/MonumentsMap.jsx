import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { getAllMonuments } from 'reducers/monuments';
import Loader from 'components/Loader/Loader';
import Map from 'components/Map/Map';
import Title from 'components/Title/Title';
import i18n from 'i18n/translation';

class MonumentsMap extends Component {

  componentWillMount() {
    this.props.getAllMonuments();
  }

  renderLoader() {
    return <Loader isShown={isEmpty(this.props.monuments)} />;
  }

  renderTitle() {
    const text = (
      <span>
        <span>{i18n.t('monuments.title')}</span>
      </span>
    );

    return <Title text={text} />;
  }

  renderSection() {
    const { monuments, monuments: { features } } = this.props;
    if (isEmpty(monuments)) return null;
    return (
      <div className="section">
        <div className="container page-container-map">
          {this.renderTitle()}
          {!!features.length && (<Map features={features} />)}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderSection()}
        {this.renderLoader()}
      </div>
    );
  }
}

MonumentsMap.propTypes = {
  getAllMonuments: PropTypes.func.isRequired,
  monuments: PropTypes.object,
};

const mapStateToProps = (state) => ({
  monuments: state.monuments.monuments
});

export default connect(mapStateToProps, {
  getAllMonuments,
})(MonumentsMap);
