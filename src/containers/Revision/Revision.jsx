import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getRevision, removeRevision } from 'reducers/revisions';
import { isEmpty } from 'lodash';
import { Link } from 'react-router';
import i18n from 'i18n/translation';
import Loader from 'components/Loader/Loader';
import Map from 'components/Map/Map';
import Title from 'components/Title/Title';
import Bar from 'components/Bar/Bar';
import Media from 'components/Media/Media';
import TableData from 'components/TableData/TableData';
import RevisionHistory from 'components/RevisionHistory/RevisionHistory';

class Revision extends Component {

  constructor(props) {
    super(props);
    this.onRemoveRevision = this.onRemoveRevision.bind(this);
  }

  componentWillMount() {
    this.props.getRevision({
      id: this.props.routeParams.revisionsId
    });
  }

  onRemoveRevision() {
    const { revision: { properties: { id } } } = this.props;
    this.props.removeRevision({ id });
  }

  renderBar() {
    let showBtnEdit = false;
    const { revision: { properties, properties: { monument_object, assigned_user } }, currentUser: [currentUser] } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <Link to="/revisions">{i18n.t('breadcrumbs.revisions')}</Link>,
      <span>{monument_object.name}</span>
    ];

    if (currentUser.is_volunteer && assigned_user === currentUser.id) {
      showBtnEdit = true;
    }

    if (currentUser.is_ballansholder && assigned_user === currentUser.id) {
      showBtnEdit = true;
    }

    if (currentUser.is_moderator && assigned_user === currentUser.id) {
      showBtnEdit = true;
    }
    
    if (currentUser.is_master) {
      showBtnEdit = true;
    }

    if (properties.status === 2 || properties.status === 3) {
      showBtnEdit = false;
    }

    if (currentUser.is_superuser) {
      showBtnEdit = true;
    }

    const buttons = showBtnEdit ? [
      (!!properties.valid_updates.status.length && <Link to={`/revision/${properties.id}/edit`} className="button is-primary">{i18n.t('buttons.edit')}</Link>),
    ] : [];

    return <Bar buttons={buttons} breadcrumbs={breadcrumbs} />;
  }

  renderRevision() {
    const { revision, revision: { properties, properties: { monument_object } } } = this.props;
    const revision_fields = [
      {
        name: 'updated',
        type: 'datetime',
        label: i18n.t('tables.revision_updated'),
      }, {
        name: 'status_object',
        type: 'integer_choices',
        label: i18n.t('tables.revision_status'),
      }, {
        name: 'monument_status_object',
        type: 'integer_choices',
        label: i18n.t('tables.revision_monument_status'),
      }, {
        name: 'user_object',
        type: 'user',
        label: i18n.t('tables.revision_user'),
      }, {
        name: 'assigned_user_object',
        type: 'user',
        label: i18n.t('tables.revision_assigned_user'),
      }, {
        name: 'text',
        type: 'text',
        label: i18n.t('tables.revision_text'),
      }, {
        name: 'comment_last',
        type: 'text',
        label: i18n.t('tables.revision_comment_last'),
      }
    ];
    const monument_fields = [
      {
        name: 'name',
        type: 'link',
        to: `/monument/${monument_object.id}`,
        label: i18n.t('tables.revision_monument_object_name'),
      }, {
        name: 'kind_object',
        type: 'foreign_key',
        label: i18n.t('tables.revision_monument_object_kind'),
      }, {
        name: 'revisions_total_count',
        type: 'text',
        label: i18n.t('tables.revision_monument_object_revisions_total_count'),
      },
    ];
    return (
      <div className="section">
        <div className="container">
          {this.renderBar()}
          <Title text={monument_object.name} />
          <TableData properties={monument_object} fields={monument_fields} />
          <Map features={[revision]} />
          <TableData properties={properties} fields={revision_fields} />
          <Media properties={properties} editable={false} />
          <RevisionHistory history={properties.revision_changes} current={properties} />
        </div>
      </div>
    );
  }

  render() {
    const { revision } = this.props;
    if (isEmpty(revision)) return <Loader isShown={isEmpty(revision)} />;
    return (
      <div className="revision">
        {this.renderRevision()}
      </div>
    );
  }

}

Revision.propTypes = {
  routeParams: PropTypes.shape({
    revisionsId: PropTypes.string.isRequired
  }),
  getRevision: PropTypes.func.isRequired,
  removeRevision: PropTypes.func.isRequired,
  revision: PropTypes.object,
  query: PropTypes.object,
  currentUser: PropTypes.array
};

const mapStateToProps = (state) => ({
  revision: state.revisions.revision,
  currentUser: state.auth.data.objects
});

export default connect(mapStateToProps, {
  getRevision,
  removeRevision,
})(Revision);
