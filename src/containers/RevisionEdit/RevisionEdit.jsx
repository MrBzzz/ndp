import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { getRevision, updateRevision, changeRevisionCoordinates, uploadRevisionMedia, removeRevisionMedia, moveRevisionMedia } from 'reducers/revisions';
import { isEmpty } from 'lodash';
import i18n from 'i18n/translation';
import Loader from 'components/Loader/Loader';
import Title from 'components/Title/Title';
import TableData from 'components/TableData/TableData';
import Bar from 'components/Bar/Bar';
import RevisionForm from 'components/RevisionForm/RevisionForm';

class RevisionEdit extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeRevisionCoordinates = this.onChangeRevisionCoordinates.bind(this);
    this.onDropFiles = this.onDropFiles.bind(this);
    this.onRemoveFiles = this.onRemoveFiles.bind(this);
    this.onMoveFile = this.onMoveFile.bind(this);
  }

  componentWillMount() {
    this.props.getRevision({
      id: this.props.routeParams.revisionsId
    });
  }

  componentWillReceiveProps(newProps) {
    const { revision: { properties } } = this.props;
    const oldIsUpdating = this.props.isUpdating;
    const newIsUpdating = newProps.isUpdating;
    if (oldIsUpdating && !newIsUpdating) {
      browserHistory.replace(`/revision/${properties.id}/`);
    }
  }

  onSubmit(data) {
    const { revision: { properties, properties: { id } } } = this.props;
    const req = {};
    Object.keys(data).map(key => {
      if (properties[key] !== data[key]) {
        req[key] = data[key];
      }
      return null;
    });

    if (req.lat || req.lng) {
      req.geometry = {
        type: 'Point',
        coordinates: [parseFloat(data.lng), parseFloat(data.lat)],
      };
    }

    if (!isEmpty(req)) {
      req.id = id;
    }
    this.props.updateRevision(req);
  }

  onChangeRevisionCoordinates(data) {
    this.props.changeRevisionCoordinates(data);
  }

  onDropFiles(files) {
    const { revision: { properties: { id } } } = this.props;
    this.props.uploadRevisionMedia({
      files,
      id
    });
  }

  onRemoveFiles(media) {
    this.props.removeRevisionMedia(media);
  }

  onMoveFile(media) {
    const { revision: { properties: { monument } } } = this.props;
    this.props.moveRevisionMedia({
      ...media,
      monument
    });
  }

  renderRevision() {
    const { revision, revision: { properties, properties: { monument_object } }, isUpdating = false } = this.props;
    const breadcrumbs = [
      <Link to="/">{i18n.t('breadcrumbs.main')}</Link>,
      <Link to="/revisions">{i18n.t('breadcrumbs.revisions')}</Link>,
      <Link to={`/revision/${properties.id}/`}>{properties.monument_object.name}</Link>,
      <span>{i18n.t('breadcrumbs.edit')}</span>
    ];
    const monument_fields = [
      {
        name: 'name',
        type: 'link',
        to: `/monument/${monument_object.id}`,
        label: i18n.t('tables.revision_monument_object_name'),
      }, {
        name: 'kind_object',
        type: 'foreign_key',
        label: i18n.t('tables.revision_monument_object_kind'),
      }, {
        name: 'revisions_total_count',
        type: 'text',
        label: i18n.t('tables.revision_monument_object_revisions_total_count'),
      },
    ];
    return (
      <div className="section">
        <div className="container">
          <Bar breadcrumbs={breadcrumbs} />
          <Title text={properties.monument_object.name} />
          <TableData properties={monument_object} fields={monument_fields} />
          <RevisionForm
            onSubmit={this.onSubmit}
            onDropFiles={this.onDropFiles}
            onRemoveFiles={this.onRemoveFiles}
            onMoveFile={this.onMoveFile}
            onChangeRevisionCoordinates={this.onChangeRevisionCoordinates}
            revision={revision}
            isUpdating={isUpdating}
            btnText={i18n.t('buttons.save')}
            media
          />
        </div>
      </div>
    );
  }

  render() {
    const { revision } = this.props;
    if (isEmpty(revision)) return <Loader />;
    return (
      <div className="revision">
        {this.renderRevision()}
      </div>
    );
  }

}

RevisionEdit.propTypes = {
  routeParams: PropTypes.shape({
    revisionsId: PropTypes.string.isRequired
  }),
  uploadRevisionMedia: PropTypes.func.isRequired,
  removeRevisionMedia: PropTypes.func.isRequired,
  moveRevisionMedia: PropTypes.func.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  changeRevisionCoordinates: PropTypes.func.isRequired,
  getRevision: PropTypes.func.isRequired,
  updateRevision: PropTypes.func.isRequired,
  revision: PropTypes.object,
  query: PropTypes.object
};

const mapStateToProps = (state) => ({
  revision: state.revisions.revision,
  isUpdating: state.revisions.updating
});

export default connect(mapStateToProps, {
  changeRevisionCoordinates,
  getRevision,
  updateRevision,
  uploadRevisionMedia,
  moveRevisionMedia,
  removeRevisionMedia
})(RevisionEdit);
