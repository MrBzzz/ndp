import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import i18n from 'i18n/translation';
import { getRevisions } from 'reducers/revisions';
import { isEmpty, isEqual } from 'lodash';
import Loader from 'components/Loader/Loader';
import RevisionsTiles from 'components/RevisionsTiles/RevisionsTiles';
import Filters from 'components/FiltersRevisions/FiltersRevisions';
import Pagination from 'components/Pagination/Pagination';
import Title from 'components/Title/Title';

class Revisions extends Component {

  componentWillMount() {
    this.props.getRevisions(this.props.query);
  }

  componentWillReceiveProps(next) {
    const prevQuery = next.query;
    const currentQuery = this.props.query;
    if (!isEqual(prevQuery, currentQuery)) {
      this.props.getRevisions(next.query);
    }
  }

  renderLoader() {
    const { revisions } = this.props;
    if (!isEmpty(revisions)) return null;
    return <Loader isShown />;
  }

  renderTitle() {
    const { revisions: { meta: { total_count } } } = this.props;
    const text = (
      <span>{i18n.t('revisions.title')} ({total_count})&nbsp;</span>
    );
    return <Title text={text} />;
  }

  renderRevisions() {
    const { revisions, revisions: { features, filters, meta }, query } = this.props;
    if (isEmpty(revisions)) return null;
    return (
      <div className="section">
        <div className="container">
          {this.renderTitle()}
          <Filters query={query} filters={filters} />
          <RevisionsTiles features={features} />
          <Pagination meta={meta} query={query} pathname="/revisions" />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderLoader()}
        {this.renderRevisions()}
      </div>
    );
  }
}

Revisions.propTypes = {
  getRevisions: PropTypes.func.isRequired,
  revisions: PropTypes.object,
  query: PropTypes.object
};

const mapStateToProps = (state, ownProps) => ({
  revisions: state.revisions.revisions,
  query: ownProps.location.query
});

export default connect(mapStateToProps, {
  getRevisions
})(Revisions);
