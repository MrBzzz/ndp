import React, { PropTypes } from 'react';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import './Root.less';
import getRoutes from 'components/Routes/Routes';

const Root = (props) => {
  const { store } = props;
  const history = syncHistoryWithStore(browserHistory, store);
  return (
    <Provider store={store} key="provider">
      <Router history={history}>{getRoutes(store)}</Router>
    </Provider>
  );
};

Root.propTypes = {
  store: PropTypes.object.isRequired
};

export default Root;
