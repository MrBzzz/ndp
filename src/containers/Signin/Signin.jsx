import React from 'react';

import SigninForm from 'components/SigninForm/SigninForm';

const Signin = () => <SigninForm />;

export default Signin;
