import React, { Component, PropTypes } from 'react';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import Gravatar from 'react-gravatar';
import { Link } from 'react-router';
import { getUser, activateUser } from 'reducers/users';
import i18n from 'i18n/translation';
import Loader from 'components/Loader/Loader';
import UserActivateForm from 'components/UserActivateForm/UserActivateForm';
import './User.less';

class Users extends Component {

  constructor(props) {
    super(props);
    this.onSubmitActivateForm = this.onSubmitActivateForm.bind(this);
  }

  componentWillMount() {
    this.props.getUser({
      id: this.props.routeParams.userId
    });
  }

  onSubmitActivateForm(data) {
    const { user } = this.props;
    const typeIdList = data.types.filter(item => item.value).map(item => item.value);
    const locationIdList = data.locations.map(item => item.value);
    const req = {
      id: user.id,
      groups: [].concat(typeIdList, locationIdList).filter(item => item),
      is_volunteer: !!~typeIdList.indexOf(3),
      is_master: !!~typeIdList.indexOf(2),
      is_moderator: !!~typeIdList.indexOf(19),
      is_ballansholder: !!~typeIdList.indexOf(1),
    };
    this.props.activateUser(req);
  }

  renderGroups() {
    const { user } = this.props;
    if (!user.types.length) return null;
    return (
      <div className="types">
        <h4 className="subtitle">{i18n.t('user.type')}</h4>
        {user.types.map((item, typesKey) => <span className="tag" key={typesKey}>{item.label}</span>)}
      </div>
    );
  }

  renderTypes() {
    const { user } = this.props;
    if (!user.groups.length) return null;
    return (
      <div className="group-names">
        <h4 className="subtitle">{i18n.t('user.group')}</h4>
        {user.locations.map((item, key) => <span className="tag" key={key}>{item.label}</span>)}
      </div>
    );
  }

  renderMonumentsStats() {
    const { user: { stats: { monuments } } } = this.props;
    return (
      <div>
        <h3 className="stats-title">{i18n.t('user.monument')}</h3>
        <nav className="level">
          <div className="level-item has-text-left">
            <p className="title" alt={i18n.t('user.stats.monument.total.alt')} title={i18n.t('user.stats.monument.total.alt')}>
              {monuments.total}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__top level-item-sub-text__green"
              alt={i18n.t('user.stats.monument.good.alt')}
              title={i18n.t('user.stats.monument.good.alt')}
            >
              {monuments.good} {i18n.t('user.stats.monument.good.title')}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__middle level-item-sub-text__red"
              alt={i18n.t('user.stats.monument.bad.alt')}
              title={i18n.t('user.stats.monument.bad.alt')}
            >
             {monuments.bad} {i18n.t('user.stats.monument.bad.title')}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__bottom level-item-sub-text__grey"
              alt={i18n.t('user.stats.monument.unknown.alt')}
              title={i18n.t('user.stats.monument.unknown.alt')}
            >
              {monuments.unknown} {i18n.t('user.stats.monument.unknown.title')}
            </p>
            <p className="heading"></p>
          </div>
        </nav>
      </div>
    );
  }

  renderRevisionStats() {
    const { user: { stats: { revisions } } } = this.props;
    return (
      <div>
        <h3 className="stats-title">{i18n.t('user.revision')}</h3>
        <nav className="level">
          <div className="level-item has-text-left">
            <p className="title" alt={i18n.t('user.stats.revision.owner')} title={i18n.t('user.stats.revision.owner')}>
              {revisions.owner}
            </p>
            <p className="heading">Создал</p>
          </div>
          <div className="level-item has-text-left">
            <p
              className="title"
              alt={i18n.t('user.stats.revision.assigned.total')}
              title={i18n.t('user.stats.revision.assigned.total')}
            >
              {revisions.assigned.total}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__top level-item-sub-text__green"
              alt={i18n.t('user.stats.revision.assigned.pending')}
              title={i18n.t('user.stats.revision.assigned.pending')}
            >
              {revisions.assigned.pending} {i18n.t('user.stats.revision.new')}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__middle level-item-sub-text__blue"
              alt={i18n.t('user.stats.revision.assigned.assigned')}
              title={i18n.t('user.stats.revision.assigned.assigned')}
            >
             {revisions.assigned.assigned} {i18n.t('user.stats.revision.in_work')}
            </p>
            <p
              className="level-item-sub-text level-item-sub-text__bottom level-item-sub-text__grey"
              alt={i18n.t('user.stats.revision.assigned.closed')}
              title={i18n.t('user.stats.revision.assigned.closed')}
            >
              {revisions.assigned.closed} {i18n.t('user.stats.revision.done')}
            </p>

            <p className="heading">Исполнитель</p>
          </div>
        </nav>
      </div>
    );
  }

  renderEditBtn() {
    const { user, currentUser: { objects: [currentUser] } } = this.props;

    if (user.is_can_activate) {
      return null;
    }

    if (user.id === currentUser.id) {
      return <Link className="button is-primary" to={`/user/${user.id}/edit`}>Редактировать</Link>;
    }

    if (currentUser.is_superuser) {
      return <Link className="button is-primary" to={`/user/${user.id}/edit`}>Редактировать</Link>;
    }

    if (currentUser.is_master && !user.is_superuser && !user.is_master) {
      return <Link className="button is-primary" to={`/user/${user.id}/edit`}>Редактировать</Link>;
    }

    if (currentUser.is_moderator && !user.is_superuser && !user.is_master && !user.is_moderator) {
      return <Link className="button is-primary" to={`/user/${user.id}/edit`}>Редактировать</Link>;
    }

    return null;
  }

  renderUser() {
    const { user, currentUser } = this.props;

    const statNode = user.is_active && (
      <div className="column">
       {this.renderMonumentsStats()}
       {this.renderRevisionStats()}
      </div>
    );

    const metaNode = (
      <div className="column tags">
       {this.renderGroups()}
       {this.renderTypes()}
      </div>
    );

    return (
      <div className="section user-page">
        <div className="container content">
          <div className="columns is-mobile">
            <div className="column is-offset-3 is-6">
              <div className="has-text-centered">
                <Gravatar email={user.email} size={200} />
                <h1>{user.first_name} {user.last_name}</h1>
                <p>{user.position} {user.organization} {user.phone}</p>
                <p>
                  <span className="button is-white">{user.username}</span>&nbsp;
                  <a href={`mailto:${user.email}`} className="button is-primary">{i18n.t('user.write')}</a>
                </p>
                {this.renderEditBtn()}
              </div>
              {user.is_can_activate && <UserActivateForm onSubmit={this.onSubmitActivateForm} user={user} currentUser={currentUser} />}
              {!user.is_can_activate && metaNode}
              {statNode}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { user } = this.props;
    if (isEmpty(user)) return <Loader />;
    return (
      <div className="users">
        {this.renderUser()}
      </div>
    );
  }

}

Users.propTypes = {
  routeParams: PropTypes.object.isRequired,
  getUser: PropTypes.func.isRequired,
  activateUser: PropTypes.func.isRequired,
  user: PropTypes.object,
  currentUser: PropTypes.object,
};

const mapStateToProps = (state) => ({
  user: state.users.user,
  currentUser: state.auth.data,
});

export default connect(mapStateToProps, {
  getUser,
  activateUser,
})(Users);
