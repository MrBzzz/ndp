import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import Gravatar from 'react-gravatar';
import { getUser, activateUser, changeUser } from 'reducers/users';
import Loader from 'components/Loader/Loader';
import UserEditForm from 'components/UserEditForm/UserEditForm';
import './UserEdit.less';

class UsersEdit extends Component {

  constructor(props) {
    super(props);
    this.onSubmitUser = this.onSubmitUser.bind(this);
  }

  componentWillMount() {
    this.props.getUser({
      id: this.props.routeParams.userId
    });
  }

  componentWillReceiveProps(newProps) {
    const { user } = this.props;
    const oldIsUpdating = this.props.isUpdating;
    const newIsUpdating = newProps.isUpdating;
    if (oldIsUpdating && !newIsUpdating) {
      browserHistory.replace(`/user/${user.id}/`);
    }
  }

  onSubmitUser(data) {
    const locationsIdList = data.locations.map(item => item.id);
    const typesIdList = data.types.filter(item => item.value !== 0).map(item => item.value);
    const { user } = this.props;
    const req = {
      first_name: data.first_name,
      last_name: data.last_name,
      position: data.position,
      organization: data.organization,
      phone: data.phone,
      is_volunteer: !!~typesIdList.indexOf(3),
      is_master: !!~typesIdList.indexOf(2),
      is_moderator: !!~typesIdList.indexOf(19),
      is_ballansholder: !!~typesIdList.indexOf(1),
      groups: locationsIdList,
    };

    if (data.password) {
      req.password = data.password;
    }

    if (data.is_active !== user.is_active) {
      req.is_active = data.is_active;
    }

    this.props.changeUser({
      data: {
        ...req
      },
      id: this.props.routeParams.userId
    });
  }

  renderUser() {
    const { user, currentUser: { objects: [currentUser] } } = this.props;
    return (
      <div className="section user-page user-page_edit">
        <div className="container content">
          <div className="columns is-mobile">
            <div className="column is-offset-3 is-6">
              <div className="has-text-centered">
                <Gravatar email={user.email} size={200} />
              </div>
              <UserEditForm
                user={user}
                currentUser={currentUser}
                onSubmit={this.onSubmitUser}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { user } = this.props;
    if (isEmpty(user)) return <Loader />;
    return (
      <div className="users">
        {this.renderUser()}
      </div>
    );
  }

}

UsersEdit.propTypes = {
  routeParams: PropTypes.object.isRequired,
  getUser: PropTypes.func.isRequired,
  activateUser: PropTypes.func.isRequired,
  changeUser: PropTypes.func.isRequired,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  isUpdating: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  user: state.users.user,
  isUpdating: state.users.updating,
  currentUser: state.auth.data,
});

export default connect(mapStateToProps, {
  getUser,
  activateUser,
  changeUser,
})(UsersEdit);
