import React, { Component, PropTypes } from 'react';
import { take, isEmpty, isEqual } from 'lodash';
import { connect } from 'react-redux';
import { getUsers } from 'reducers/users';
import { Link } from 'react-router';
import Gravatar from 'react-gravatar';
import classNames from 'classnames';
import i18n from 'i18n/translation';
import Filters from 'components/FiltersUsers/FiltersUsers';
import Loader from 'components/Loader/Loader';
import Date from 'components/Date/Date';
import Pagination from 'components/Pagination/Pagination';
import Title from 'components/Title/Title';
import OrderLink from 'components/OrderLink/OrderLink';

import './Users.less';

class Users extends Component {

  componentWillMount() {
    this.props.getUsers(this.props.query);
  }

  componentWillReceiveProps(next) {
    const prevQuery = next.query;
    const currentQuery = this.props.query;
    if (!isEqual(prevQuery, currentQuery)) {
      this.props.getUsers(next.query);
    }
  }

  renderTitle() {
    const { users: { meta: { total_count } } } = this.props;

    const text = (
      <span>
        <span>{i18n.t('users.title')} ({total_count})&nbsp;</span>
      </span>
    );

    return <Title text={text} />;
  }

  renderUser(user, key) {
    const { monument_stats: {
      monument_total_count,
      monument_revision_total_count,
      monument_revision_owner_status_pending_count,
      monument_revision_owner_status_assigned_count,
      monument_revision_assigned_status_closed_count,
    } } = user;
    const userCls = classNames('box', 'user', 'user-item', {
      'user-item__is-active': !user.is_active
    });
    return (
      <Link to={`/user/${user.id}`} key={key} className={userCls}>
        <table className="user-list">
          <tbody>
            <tr>
              <td className="user-list_avatar"><Gravatar email={user.email} size={50} /></td>
              <td className="user-list_name">
                <p><strong>{user.first_name} {user.last_name}</strong></p>
                <p>{user.username} ({user.email})</p>
              </td>
              <td className="user-list_types" width={'100px'}>
                {user.types.map((item, typesKey) => <span className="tag" key={typesKey}>{item.label}</span>)}
                {!user.is_active && <span className="tag is-warning">- не активен -</span>}
              </td>
              <td className="user-list_groups">
                {take(user.groups, 5).map((item, groupKey) => <span className="tag" key={groupKey}>{item.label}</span>)}
                {user.groups.length > 5 && <span> ... и еще {user.groups.length - 5}</span>}
              </td>
              <td className="user-list_date">
                <Date date={user.created} />
              </td>
              <td>{ monument_revision_total_count }</td>
              <td>{ monument_revision_owner_status_pending_count }</td>
              <td>{ monument_revision_owner_status_assigned_count }</td>
              <td>{ monument_revision_assigned_status_closed_count }</td>
              <td>{ monument_total_count }</td>
            </tr>
          </tbody>
        </table>
      </Link>
    );
  }

  renderUsers() {
    const { users: { objects, meta, filters }, query } = this.props;
    return (
      <div className="section">
        <div className="container">
          {this.renderTitle()}
          <Filters query={query} filters={filters} />
          <div className="head-user">
          <table className="user-list user-list__head">
            <tbody>
              <tr>
                <td className="user-list_avatar"></td>
                <td className="user-list_name">Имя</td>
                <td className="user-list_types" width={'100px'}>Права</td>
                <td className="user-list_groups">Локация</td>
                <td className="user-list_date">Дата регистрации</td>
                <td><OrderLink field="monument_stats__monument_revision_total_count" pathname="/users/" page="users" text="Осмотров всего" /></td>
                <td><OrderLink field="monument_stats__monument_revision_owner_status_pending_count" pathname="/users/" page="users" text="Осмотров новые" /></td>
                <td><OrderLink field="monument_stats__monument_revision_owner_status_assigned_count" pathname="/users/" page="users" text="Осмотров в работе" /></td>
                <td><OrderLink field="monument_stats__monument_revision_assigned_status_closed_count" pathname="/users/" page="users" text="Осмотров завершенные" /></td>
                <td><OrderLink field="monument_stats__monument_total_count" pathname="/users/" page="users" text="Памятники всего" /></td>
              </tr>
            </tbody>
          </table>
            </div>
          {objects.map((item, key) => this.renderUser(item, key))}
          <Pagination meta={meta} query={query} pathname="/users" />
        </div>
      </div>
    );
  }

  render() {
    const { users } = this.props;
    if (isEmpty(users)) return <Loader />;
    return (
      <div className="users">
        {this.renderUsers()}
      </div>
    );
  }

}

Users.propTypes = {
  getUsers: PropTypes.func.isRequired,
  users: PropTypes.object,
  query: PropTypes.object,
};

const mapStateToProps = (state, ownProps) => ({
  users: state.users.users,
  query: ownProps.location.query,
});

export default connect(mapStateToProps, {
  getUsers,
})(Users);
