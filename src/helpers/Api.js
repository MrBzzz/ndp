import superagent from 'superagent';

const methods = ['get', 'post', 'put', 'patch', 'del'];

function formatUrl(path) {
  return path[0] !== '/' ? `/${path}` : path;
}

class Api {
  constructor() {
    methods.forEach((method) => (
      this[method] = (path, { params, data, type = '' } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));

        if (params) {
          request.query(params);
        }

        if (data) {
          switch (type) {
            case 'form': {
              request.type('form').send(data);
              break;
            }
            case 'file': {
              request.attach('file', data.file);
              Object.keys(data.fields).map((key) => request.field(key, data.fields[key]));
              break;
            }
            case 'raw': {
              request.set('Content-Type', 'application/json').send(data);
              break;
            }
            default: {
              request.send(data);
            }
          }
        }

        request.end((err, { body } = {}) => (err ? reject(body || err) : resolve(body)));
      })));
  }
}

export default Api;
