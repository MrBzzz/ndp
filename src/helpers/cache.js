import { isEmpty } from 'lodash';
import urls from 'helpers/urls';

const cache = {
  currentUser: {
    url: urls.currentUser,
    data: {},
    promise: {},
  },
  users: {
    url: urls.usersAll,
    data: {},
    promise: {},
    isLoading: false,
  },
  kind: {
    url: urls.kinds,
    data: {},
    promise: {},
    isLoading: false,
  },
  location: {
    url: urls.location,
    data: {},
    promise: {},
    isLoading: false,
  },
  stateSchema: {
    url: urls.stateSchema,
    data: {},
    promise: {},
    isLoading: false,
  },
  repairSchema: {
    url: urls.repairSchema,
    data: {},
    promise: {},
    isLoading: false,
  },
  monumentsSchema: {
    url: urls.monumentsSchema,
    data: {},
    promise: {},
    isLoading: false,
  },
  revisionsSchema: {
    url: urls.revisionsSchema,
    data: {},
    promise: {},
    isLoading: false,
  }
};

export const getCachedData = (client, name) => {
  if (!cache[name].isLoading) {
    cache[name].isLoading = true;
    cache[name].promise = new Promise((resolve, reject) => {
      if (!cache[name]) reject('not found cached data >>>', name);
      if (isEmpty(cache[name].data)) {
        client.get(cache[name].url).then(res => {
          cache[name].data = res;
          resolve(cache[name].data);
        }).catch(error => {
          cache[name].isLoading = false;
          reject(error);
        });
      } else {
        resolve(cache[name].data);
      }
    });
  }
  return cache[name].promise;
};

export const cleanCachedData = () => {
  Object.keys(cache).map(key => {
    cache[key].data = {};
    cache[key].promise = {};
    cache[key].isLoading = false;
    return false;
  });
};

export default {
  getCachedData,
  cleanCachedData
};
