import { isEmpty } from 'lodash';
import { mediaSettings } from 'settings/settings.js';

export const getImage = (image) => {
  let data = { type: 'load' };
  if (!isEmpty(image)) {
    const thumbnail = image.thumbnails.find((item) => item.name === mediaSettings.thumbnail_name);
    const src = image.thumbnails.find((item) => item.name === mediaSettings.src_name);
    data = {
      ...image,
      thumbnail: thumbnail ? thumbnail.file : image.file,
      src: src ? src.file : image.file,
      type: 'image'
    };
  }
  return data;
};

export const getVideo = (video) => {
  let data = { type: 'load' };
  if (!isEmpty(video)) {
    data = {
      ...video,
      src: video.file,
      type: 'video'
    };
  }
  return data;
};

export const prepareMedia = (images, videos) => [].concat.apply(images.map(img => getImage(img)), videos.map(video => getVideo(video)));

export const prepareMediaLightBox = (images, videos) => {
  const images_data = images.map(img => getImage(img)).filter(item => item.type === 'image');
  const videos_data = videos.map(video => getVideo(video)).filter(item => item.type === 'video');
  return [].concat.apply(images_data, videos_data);
};

export default {
  prepareMedia,
  getImage,
  getVideo
};
