import i18n from 'i18n/translation';

export const getUserGroups = (groups) => groups
  .filter(group => !~['Модераторы', 'Балансодержатели', 'Волонтеры', 'Тестирование', 'Руководители'].indexOf(group.name) && group.name !== '')
  .map(group => ({
    label: group.name.replace(' - рабочая группа', ''),
    value: group.id,
  }));

export const filterGroupFromTypes = (groups) => groups
  .filter(group => !~['Модераторы', 'Балансодержатели', 'Волонтеры', 'Тестирование', 'Руководители'].indexOf(group.name) && group.name !== '');

export const getLocationFromGroups = (groups) => groups.objects
  .filter(item => item && item.locations[0] && item.locations[0].id)
  .map(({ locations: [group] }) => ({
    label: group.name.replace(' - рабочая группа', ''),
    value: group.id,
  })).filter(group => !!group.label);

export const getUserTypes = (item) => {
  const result = [];
  if (item.is_ballansholder) {
    result.push({
      label: i18n.t('user.is_ballansholder'),
      value: 1
    });
  }
  if (item.is_master) {
    result.push({
      label: i18n.t('user.is_master'),
      value: 2
    });
  }
  if (item.is_moderator) {
    result.push({
      label: i18n.t('user.is_moderator'),
      value: 19
    });
  }
  if (item.is_superuser) {
    result.push({
      label: i18n.t('user.is_superuser'),
      value: 0,
      clearableValue: false
    });
  }
  if (item.is_volunteer) {
    result.push({
      label: i18n.t('user.is_volunteer'),
      value: 3
    });
  }
  return result;
};

export default {
  filterGroupFromTypes,
  getLocationFromGroups,
  getUserGroups,
  getUserTypes,
};
