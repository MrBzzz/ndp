import urls from 'helpers/urls';
import { getCachedData, cleanCachedData } from 'helpers/cache';

export const getCurrentUser = () => (client) => new Promise((resolve, reject) => {
  const currentUser = getCachedData(client, 'currentUser');

  Promise.all([currentUser])
    .then(([currentUserData]) => {
      const [user] = currentUserData.objects;
      const users = getCachedData(client, 'users');
      const kind = getCachedData(client, 'kind');
      const location = getCachedData(client, 'location');
      const monumentsSchema = getCachedData(client, 'monumentsSchema');
      const revisionsSchema = getCachedData(client, 'revisionsSchema');
      const repairSchema = getCachedData(client, 'repairSchema');
      const stateSchema = getCachedData(client, 'stateSchema');

      const userTotalCount = client.get(urls.userTotalCount);
      const monumentTotalCount = client.get(urls.monumentTotalCount);

      let revisionTotalCount;
      // if (user.is_volunteer) {
      //   revisionTotalCount = client.get(urls.revisionTotalCountForVolonter(user.id));
      // } else {
        revisionTotalCount = client.get(urls.revisionTotalCount);
      // }

      Promise
        .all([
          userTotalCount,
          monumentTotalCount,
          revisionTotalCount,
          users,
          kind,
          location,
          monumentsSchema,
          revisionsSchema,
          repairSchema,
          stateSchema
        ])
        .then(([
          userTotalCountData,
          monumentTotalCountData,
          revisionTotalCountData,
        ]) => {
          user.monumentTotalCount = monumentTotalCountData.meta.total_count;
          user.userTotalCount = userTotalCountData.meta.total_count;
          user.revisionTotalCount = revisionTotalCountData.meta.total_count;
          resolve(currentUserData);
        })
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const login = (data) => (client) => new Promise((resolve, reject) => {
  client.post(urls.login, { data, type: 'form' })
    .then(() => {
      getCurrentUser()(client)
        .then(res => resolve(res))
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const logout = () => (client) => new Promise((resolve, reject) => {
  cleanCachedData();
  client.get(urls.logout).then(() => resolve()).catch(error => reject(error));
});

export default {
  getCurrentUser,
  login,
  logout
};
