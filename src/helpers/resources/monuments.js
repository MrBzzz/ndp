import urls from 'helpers/urls';
import { find, findIndex, sortBy } from 'lodash';
import i18n from 'i18n/translation';
import { formatDateTime } from 'helpers/utils';
import { filterGroupFromTypes } from 'helpers/parse';
import { getCachedData } from 'helpers/cache';

export const getMonuments = (data) => (client) => new Promise((resolve, reject) => {
  const monuments = client.get(urls.monuments, { params: data });
  const kind = getCachedData(client, 'kind');
  const location = getCachedData(client, 'location');
  const stateSchema = getCachedData(client, 'stateSchema');
  const repairSchema = getCachedData(client, 'repairSchema');

  Promise.all([monuments, kind, location, stateSchema, repairSchema])
    .then(([monumentsData, kindData, locationData, stateSchemaData, repairSchemaData]) => {
      const stateStatusChoices = stateSchemaData.fields.status.choices;
      const repairStatusChoices = repairSchemaData.fields.status.choices;
      const result = {
        ...monumentsData,
        filters: {
          kind: kindData.objects.map(item => ({ value: item.id, label: item.name })),
          revision_status: stateStatusChoices,
          repair_status: repairStatusChoices,
          is_published_on_geoportal: [
            { value: '1', label: i18n.t('filter.geoportal.published') },
            { value: '0', label: i18n.t('filter.geoportal.unpublished') }
          ],
          location: locationData.objects.map(item => ({ value: item.id, label: item.name }))
        }
      };
      resolve(result);
    })
    .catch(error => reject(error));
});

export const getAllMonuments = () => (client) => new Promise((resolve, reject) => {
  const monuments = client.get(urls.monuments, { params: { limit: 0 } });
  const kind = getCachedData(client, 'kind');
  const location = getCachedData(client, 'location');
  const stateSchema = getCachedData(client, 'stateSchema');
  const repairSchema = getCachedData(client, 'repairSchema');

  Promise.all([monuments, kind, location, stateSchema, repairSchema])
    .then(([monumentsData, kindData, locationData, stateSchemaData, repairSchemaData]) => {
      const stateStatusChoices = stateSchemaData.fields.status.choices;
      const repairStatusChoices = repairSchemaData.fields.status.choices;

      const result = {
        ...monumentsData,
        filters: {
          kind: kindData.objects.map(item => ({ value: item.id, label: item.name })),
          revision_status: stateStatusChoices,
          repair_status: repairStatusChoices,
          is_published_on_geoportal: [
            { value: '1', label: i18n.t('filter.geoportal.published') },
            { value: '0', label: i18n.t('filter.geoportal.unpublished') }
          ],
          location: locationData.objects.map(item => ({ value: item.id, label: item.name }))
        }
      };
      resolve(result);
    })
    .catch(error => reject(error));
});

export const getMonument = (data) => (client) => new Promise((resolve, reject) => {
  const { id } = data;
  const monument = client.get(urls.monument(id));
  const revisions = client.get(urls.revisionByMonument(id));
  const kind = getCachedData(client, 'kind');
  const location = getCachedData(client, 'location');
  const monumentsSchema = getCachedData(client, 'monumentsSchema');
  const revisionsSchema = getCachedData(client, 'revisionsSchema');
  const stateSchema = getCachedData(client, 'stateSchema');
  const users = getCachedData(client, 'users');
  const currentUserRequest = getCachedData(client, 'currentUser');

  Promise.all([monument, revisions, monumentsSchema, revisionsSchema, stateSchema, users, kind, location, currentUserRequest])
    .then(([monumentData, revisionsData, monumentsSchemaData, revisionsSchemaData, stateSchemaData, usersData, kindData, locationData, currentUserRequestData]) => {
      const { objects: [currentUser] } = currentUserRequestData;
      const result = { ...monumentData };
      const kind_id = result.properties.kind;
      const location_id = result.properties.location;
      const status = monumentData.properties.current_state.status;
      const status_object = stateSchemaData.fields.status.choices.find(item => item.value === status);

      result.properties.kind_list = kindData.objects;
      result.properties.location_list = locationData.objects;
      result.properties.current_state_status = monumentData.properties.current_state.status;
      result.properties.choices = {
        current_state_status: stateSchemaData.fields.status.choices,
        kind: kindData.objects.map(item => ({ value: item.id, label: item.name })),
        location: locationData.objects.map(item => ({ value: item.id, label: item.name })),
      };
      result.properties.kind_object = kindData.objects.find(item => item.id === kind_id);
      result.properties.location_object = locationData.objects.find(item => item.id === location_id);
      result.properties.status_object = {
        id: status_object.value,
        name: status_object.label
      };
      result.properties.monument_schema = monumentsSchemaData;
      result.properties.load_video = [];
      result.properties.load_images = [];
      result.properties.formatted_created = formatDateTime(result.properties.created);
      result.properties.formatted_updated = formatDateTime(result.properties.updated);
      result.properties.is_published = result.properties.is_published ? 1 : 0;
      result.properties.is_published_on_geoportal = result.properties.is_published_on_geoportal ? 1 : 0;
      result.properties.lng = 0;
      result.properties.lat = 0;

      if (result.geometry) {
        const [lat, lng] = result.geometry.coordinates;
        result.properties.lng = lat;
        result.properties.lat = lng;
      }

      if (findIndex(result.properties.images, { order: 777 }) === -1 && result.properties.images.length) {
        const [firstImg] = result.properties.images;
        firstImg.order = 777;
      }

      result.properties.images = sortBy(result.properties.images, 'order');

      result.properties.revisions = {
        ...revisionsData,
        features: revisionsData.features.map(feature => {
          const tmp = feature;
          tmp.properties.assigned_user_data = find(usersData.objects, user => user.id === tmp.properties.assigned_user);
          tmp.properties.schema = revisionsSchemaData;
          return tmp;
        })
      };

      if (currentUser.is_master) {
        const locationsIdList = filterGroupFromTypes(currentUser.groups).map(({ locations: [item] }) => item.id);
        result.properties.choices.location = locationData.objects.filter(item => !!~locationsIdList.indexOf(item.id)).map(item => ({ value: item.id, label: item.name }));
      }
      resolve(result);
    })
    .catch(error => reject(error));
});

export const updateMonument = (data) => (client) => new Promise((resolve, reject) => {
  const monument = client.patch(urls.monument(data.id), { data });
  Promise.all([monument])
    .then(() => {
      if (data.current_state_status !== data.old_current_state_status) {
        client.post(urls.state, {
          data: {
            monument: data.id,
            status: data.current_state_status,
            text: '',
            user: data.user
          }
        }).then(() => resolve()).catch(error => reject(error));
      } else {
        resolve();
      }
    })
    .catch(error => reject(error));
});

export const removeMonumentMedia = (data) => client => new Promise((resolve, reject) => {
  let req;

  if (data.type === 'image') {
    req = client.del(urls.monumentImagesItem(data.id));
  } else if (data.type === 'video') {
    req = client.del(urls.monumentVideosItem(data.id));
  }

  Promise.all([req])
    .then(() => {
      client.get(urls.monument(data.monument))
        .then((monumentData) => {
          let images = monumentData.properties.images;
          if (findIndex(images, { order: 777 }) === -1 && images.length) {
            const [firstImg] = images;
            firstImg.order = 777;
          }

          images = sortBy(images, 'order');

          resolve({
            images: monumentData.properties.images,
            videos: monumentData.properties.videos
          });
        })
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const uploadMonumentMedia = (data) => client => new Promise((resolve, reject) => {
  const { files, id } = data;
  const medias = files.map(file => {
    let promise;
    if (~file.type.indexOf('image')) {
      promise = client.post(urls.monumentImages, {
        data: {
          file,
          fields: {
            monument: id,
            order: 888,
          }
        },
        type: 'file'
      });
    } else if (~file.type.indexOf('video')) {
      promise = client.post(urls.monumentVideos, {
        data: {
          file,
          fields: {
            monument: id,
            order: 888,
          }
        },
        type: 'file'
      });
    }
    return promise;
  });
  Promise.all(medias)
    .then(() => {
      client.get(urls.monument(data.id))
        .then((monumentData) => {
          let images = monumentData.properties.images;
          if (findIndex(images, { order: 777 }) === -1 && images.length) {
            const [firstImg] = images;
            firstImg.order = 777;
          }

          images = sortBy(images, 'order');

          resolve({
            images: monumentData.properties.images,
            videos: monumentData.properties.videos
          });
        })
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const removeMonument = (data) => client => new Promise((resolve, reject) => {
  client.del(urls.monument(data.id))
    .then(() => resolve())
    .catch(error => reject(error));
});

export const getDataForNewMonument = () => client => new Promise((resolve, reject) => {
  const monumentsSchema = getCachedData(client, 'monumentsSchema');
  const revisionsSchema = getCachedData(client, 'revisionsSchema');
  const kind = getCachedData(client, 'kind');
  const location = getCachedData(client, 'location');
  const stateSchema = getCachedData(client, 'stateSchema');
  const currentUserRequest = getCachedData(client, 'currentUser');

  Promise.all([monumentsSchema, revisionsSchema, stateSchema, kind, location, currentUserRequest])
    .then(([monumentsSchemaData, revisionsSchemaData, stateSchemaData, kindData, locationData, currentUserRequestData]) => {
      const { objects: [currentUser] } = currentUserRequestData;
      const result = {
        geometry: null,
        properties: {
          kind_list: kindData.objects,
          location_list: locationData.objects,
          choices: {
            current_state_status: stateSchemaData.fields.status.choices,
            kind: kindData.objects.map(item => ({ value: item.id, label: item.name })),
            location: locationData.objects.map(item => ({ value: item.id, label: item.name })),
          },
          monument_schema: { ...monumentsSchemaData },
          revisions_schema: { ...revisionsSchemaData },
          load_video: [],
          load_images: [],
          name: '',
          address: '',
          address_more: '',
          build_date: '',
          code: '',
          created: '',
          history_info: '',
          number_of_dead: '',
          number_of_dead_known: '',
          number_of_dead_unknown: '',
          owner: '',
          owner_address: '',
          responsible_person_1: '',
          responsible_person_2: '',
          responsible_person_email_1: '',
          responsible_person_email_2: '',
          responsible_person_phone_1: '',
          responsible_person_phone_2: '',
          images: [],
          videos: [],
          is_published: 0,
          is_published_on_geoportal: 0,
          kind: 0,
          lat: 0,
          lng: 0,
          location: 0,
        },
        type: 'Feature',
      };

      if (currentUser.is_master) {
        const locationsIdList = filterGroupFromTypes(currentUser.groups).map(({ locations: [item] }) => item.id);
        result.properties.choices.location = locationData.objects.filter(item => !!~locationsIdList.indexOf(item.id)).map(item => ({ value: item.id, label: item.name }));
        if (locationsIdList.length === 1) {
          result.properties.location = locationsIdList[0];
        }
      }

      resolve(result);
    })
    .catch(error => reject(error));
});

export const createMonument = (data) => client => new Promise((resolve, reject) => {
  client.post(urls.monumentNew, { data })
    .then(res => {
      resolve(res);
    }).catch(error => reject(error));
});

export const changeSortFiles = (data) => client => new Promise((resolve, reject) => {
  const mainImg = data.main;
  client.patch(urls.monumentImagesItem(mainImg.id), {
    data: {
      is_first: true
    }
  }).then(() => {
    client.get(urls.monument(data.id))
      .then((monumentData) => {
        resolve({
          images: monumentData.properties.images,
          videos: monumentData.properties.videos
        });
      })
      .catch(error => reject(error));
  }).catch(error => reject(error));
});

export default {
  getAllMonuments,
  getDataForNewMonument,
  uploadMonumentMedia,
  removeMonumentMedia,
  updateMonument,
  getMonument,
  getMonuments,
  createMonument,
  removeMonument,
  changeSortFiles,
};
