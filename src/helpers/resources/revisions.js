import { find, cloneDeep, isEmpty } from 'lodash';
import urls from 'helpers/urls';
import moment from 'moment';
import settings from 'settings/settings';
import { getCachedData } from 'helpers/cache';

export const updateRevision = (data) => client => new Promise((resolve, reject) => {
  if (isEmpty(data)) {
    resolve();
  } else {
    const revision = client.patch(urls.revision(data.id), { data });
    Promise.all([revision])
      .then(() => resolve())
      .catch(error => reject(error));
  }
});

export const getRevisions = (data) => client => new Promise((resolve, reject) => {
  const currentUser = getCachedData(client, 'currentUser');

  Promise.all([currentUser]).then(() => {
    // const userData = currentUserData.objects[0];

    const req = {
      ...data
    };

    if (data.updated__gte) {
      req.updated__gte = moment(data.updated__gte, settings.formats.date)
        .startOf('day').format(settings.formats.dateTimeServer);
    }

    if (data.updated__lte) {
      req.updated__lte = moment(data.updated__lte, settings.formats.date)
        .endOf('day').format(settings.formats.dateTimeServer);
    }

    // if (userData.is_volunteer) {
    //   revisions = client.get(urls.revisionsForVolonter(userData.id), { params: req });
    // } else {
    const revisions = client.get(urls.revisions, { params: req });
    // }

    const revisionsSchema = getCachedData(client, 'revisionsSchema');
    const users = getCachedData(client, 'users');

    Promise.all([revisions, revisionsSchema, users])
      .then(([revisionData, revisionsSchemaData, usersData]) => {
        const statusChoices = cloneDeep(revisionsSchemaData.fields.status.choices);
        const result = {
          ...revisionData,
          filters: {
            status: statusChoices,
            monument_status: revisionsSchemaData.fields.monument_status.choices,
            user: usersData.objects.map((user) => ({
              label: `${user.first_name} ${user.last_name} ${user.email}`,
              value: user.id
            }))
          },
          features: revisionData.features.map(feature => {
            const tmp = feature;
            tmp.schema = revisionsSchemaData;
            tmp.properties.schema = revisionsSchemaData;
            tmp.properties.assigned_user_data = find(usersData.objects, user => user.id === tmp.properties.assigned_user);
            return tmp;
          })
        };
        resolve(result);
      }).catch(error => reject(error));
  }).catch(error => reject(error));
});

export const getRevision = (data) => client => new Promise((resolve, reject) => {
  const revision = client.get(urls.revision(data.id));
  const revisionChanges = client.get(urls.revisionChange(data.id));
  const monumentsSchema = getCachedData(client, 'monumentsSchema');
  const revisionsSchema = getCachedData(client, 'revisionsSchema');
  const kind = getCachedData(client, 'kind');
  const users = getCachedData(client, 'users');

  Promise.all([revision, kind, revisionChanges, monumentsSchema, revisionsSchema, users])
    .then(([revisionData, kindData, revisionChangesData, monumentsSchemaData, revisionsSchemaData, usersData]) => {
      const result = { ...revisionData };
      const statusChoices = cloneDeep(revisionsSchemaData.fields.status.choices);
      const monumentStatusChoices = cloneDeep(revisionsSchemaData.fields.monument_status.choices);
      const valid_assigned_user = result.properties.valid_updates.assigned_user;
      const valid_status = result.properties.valid_updates.status;

      valid_assigned_user.push(revisionData.properties.assigned_user);
      valid_status.push(result.properties.status);

      result.properties.monument_schema = { ...monumentsSchemaData };
      result.properties.revision_schema = cloneDeep(revisionsSchemaData);
      result.properties.revision_changes = revisionChangesData.features.map(item => ({
        created: item.properties.created,
        comment: item.properties.comment,
        text: item.properties.text,
        from_status: find(statusChoices, (fld) => fld.value === item.properties.from_status),
        to_status: find(statusChoices, (fld) => fld.value === item.properties.to_status),
        from_monument_status: find(monumentStatusChoices, (fld) => fld.value === item.properties.from_monument_status),
        to_monument_status: find(monumentStatusChoices, (fld) => fld.value === item.properties.to_monument_status),
        assigned_user: find(usersData.objects, user => user.id === item.properties.assigned_user),
        user: find(usersData.objects, user => user.id === item.properties.user),
      }));

      result.properties.kind_objects = kindData.objects;
      result.properties.monument_object.kind_object = find(kindData.objects, item => item.id === result.properties.monument_object.kind);
      result.properties.status_object = find(statusChoices, (item) => item.value === result.properties.status);
      result.properties.monument_status_object = find(revisionsSchemaData.fields.monument_status.choices, (item) => item.value === result.properties.monument_status);

      result.properties.assigned_user_object = find(usersData.objects, user => user.id === result.properties.assigned_user);
      result.properties.user_object = find(usersData.objects, user => user.id === result.properties.user);
      result.properties.revision_schema.fields.status.choices = statusChoices.filter(item => ~valid_status.indexOf(item.value));

      result.properties.revision_schema.fields.user.choices = usersData.objects.map(user => ({
        value: user.id,
        label: `${user.first_name} ${user.last_name} ${user.email}`
      }));
      result.properties.revision_schema.fields.assigned_user.choices = usersData.objects.map(user => ({
        value: user.id,
        label: `${user.first_name} ${user.last_name} (${user.username}) ${user.email}`
      })).filter(item => ~valid_assigned_user.indexOf(item.value));

      result.properties.load_video = [];
      result.properties.load_images = [];
      result.properties.lng = 0;
      result.properties.lat = 0;

      if (result.geometry) {
        const [lat, lng] = result.geometry.coordinates;
        result.properties.lng = lat;
        result.properties.lat = lng;
      }

      const revisionsByMonument = client.get(urls.revisionByMonument(result.properties.monument));
      const fullMonument = client.get(urls.monument(result.properties.monument));
      Promise.all([revisionsByMonument, fullMonument])
        .then(([revisionsByMonumentData, fullMonumentData]) => {
          result.properties.monument_object.revisions_total_count = revisionsByMonumentData.meta.total_count;
          result.properties.monument_object = {
            ...result.properties.monument_object,
            ...fullMonumentData.properties,
          };

          resolve(result);
        }).catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const uploadRevisionMedia = (data) => client => new Promise((resolve, reject) => {
  const { files, id } = data;
  const medias = files.map(file => {
    let promise;
    if (~file.type.indexOf('image')) {
      promise = client.post(urls.revisionImages, {
        data: {
          file,
          fields: {
            monument_revision: id,
            order: 888
          }
        },
        type: 'file'
      });
    } else if (~file.type.indexOf('video')) {
      promise = client.post(urls.revisionVideos, {
        data: {
          file,
          fields: {
            monument_revision: id,
            order: 888
          }
        },
        type: 'file'
      });
    }
    return promise;
  });
  Promise.all(medias)
    .then(() => {
      client.get(urls.revision(data.id))
        .then((revisionData) => {
          resolve({
            images: revisionData.properties.images,
            videos: revisionData.properties.videos
          });
        })
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const removeRevisionMedia = (data) => client => new Promise((resolve, reject) => {
  let req;

  if (data.type === 'image') {
    req = client.del(urls.revisionImagesItem(data.id));
  } else if (data.type === 'video') {
    req = client.del(urls.revisionVideosItem(data.id));
  }

  Promise.all([req])
    .then(() => {
      client.get(urls.revision(data.monument_revision))
        .then((revisionData) => {
          resolve({
            images: revisionData.properties.images,
            videos: revisionData.properties.videos
          });
        })
        .catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const getDataForNewRevision = data => client => new Promise((resolve, reject) => {
  const result = {
    geometry: null,
    properties: {},
    type: 'Feature',
  };
  const { id, currentUser: { objects: [currentUser] } } = data;
  const monument = client.get(urls.monument(id));
  const monumentsSchema = getCachedData(client, 'monumentsSchema');
  const revisionsSchema = getCachedData(client, 'revisionsSchema');
  const kind = getCachedData(client, 'kind');
  const users = getCachedData(client, 'users');

  Promise.all([monument, kind, users, monumentsSchema, revisionsSchema])
    .then(([monumentData, kindData, usersData, monumentsSchemaData, revisionsSchemaData]) => {
      const properties = result.properties;

      properties.monument_schema = { ...monumentsSchemaData };
      properties.revision_schema = { ...revisionsSchemaData };
      properties.kind_objects = kindData.objects;
      properties.monument_object = {
        ...monumentData.properties,
        revisions_total_count: monumentData.properties.info.revision_count,
        kind_object: find(kindData.objects, item => item.id === monumentData.properties.kind)
      };

      properties.lng = 0;
      properties.lat = 0;
      properties.geometry = null;
      if (monumentData.geometry) {
        const [lat, lng] = monumentData.geometry.coordinates;
        properties.lng = lat;
        properties.lat = lng;
        properties.geometry = monumentData.geometry;
      }
      properties.status = null;
      properties.monument_status = 0;
      properties.user = currentUser.id;
      properties.assigned_user = currentUser.id;
      properties.text = '';
      properties.comment_last = '';
      properties.revision_schema.fields.user.choices = usersData.objects.map(user => ({
        value: user.id,
        label: `${user.first_name} ${user.last_name} ${user.email}`
      }));
      properties.revision_schema.fields.assigned_user.choices = usersData.objects.map(user => ({
        value: user.id,
        label: `${user.first_name} ${user.last_name} (${user.username}) ${user.email}`
      }))
        .filter(item => ~monumentData.properties.valid_operations.create_revision_assigned_user.indexOf(item.value));

      resolve(result);
    })
    .catch(error => reject(error));
});

export const createRevision = data => client => new Promise((resolve, reject) => {
  const revisionNew = client.post(urls.revisionNew, { data });
  Promise.all([revisionNew])
    .then(() => resolve())
    .catch(error => reject(error));
});

export const removeRevision = data => client => new Promise((resolve, reject) => {
  client.del(urls.revision(data.id))
    .then(() => resolve())
    .catch(error => reject(error));
});

export const moveRevisionCoordinate = data => client => new Promise((resolve, reject) => {
  const monument = client.patch(urls.monument(data.id), { data });
  Promise.all([monument])
    .then(() => {
      resolve();
    })
    .catch(error => reject(error));
});

export const moveRevisionMedia = data => client => new Promise((resolve, reject) => {
  let req;

  if (data.type === 'image') {
    req = client.post(urls.revisionImagesToMonument, { type: 'raw', data: { id: data.id } });
  } else if (data.type === 'video') {
    req = client.post(urls.revisionVideosToMonument, { type: 'raw', data: { id: data.id } });
  }

  Promise.all([req])
    .then(() => {
      client.get(urls.revision(data.monument_revision))
        .then((revisionData) => {
          const fullMonument = client.get(urls.monument(data.monument));
          Promise.all([fullMonument])
            .then(([fullMonumentData]) => {
              resolve({
                monument_object: {
                  ...fullMonumentData.properties,
                },
                images: revisionData.properties.images,
                videos: revisionData.properties.videos
              });
            }).catch(error => reject(error));
          }).catch(error => reject(error));
        }).catch(error => reject(error));
});

export default {
  moveRevisionCoordinate,
  getDataForNewRevision,
  removeRevisionMedia,
  uploadRevisionMedia,
  moveRevisionMedia,
  updateRevision,
  getRevision,
  getRevisions,
  createRevision,
  removeRevision,
};
