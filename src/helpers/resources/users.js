import urls from 'helpers/urls';
import { getUserGroups, getUserTypes, getLocationFromGroups } from 'helpers/parse';
import { getCachedData } from 'helpers/cache';
import { cloneDeep } from 'lodash';

export const getUsers = (data) => (client) => new Promise((resolve, reject) => {
  const req = {
    groups__in: [],
  };

  if (data.order_by) {
    req.order_by = data.order_by;
  }

  if (data.location__in && data.location__in.length) {
    if (typeof data.location__in === 'string') {
      req.groups__in.push(data.location__in);
    } else {
      req.groups__in = [].concat([], req.groups__in, data.location__in);
    }
  }

  if (data.type__in && data.type__in.length) {
    if (typeof data.type__in === 'string') {
      req.groups__in.push(data.type__in);
    } else {
      req.groups__in = [].concat([], req.groups__in, data.type__in);
    }
  }

  if (data.is_active) {
    req.is_active = data.is_active;
  }

  if (data.q) {
    req.q = data.q;
  }

  if (data.offset) {
    req.offset = data.offset;
  }

  const users = client.get(urls.users, { params: req });
  const location = getCachedData(client, 'location');
  Promise.all([users, location])
    .then(([usersData, locationData]) => {
      const locationCloneData = cloneDeep(locationData);
      const res = {
        ...usersData,
        objects: usersData.objects.map(item => ({
          ...item,
          created: item.date_joined,
          updated: item.date_updated,
          groups: getUserGroups(item.groups),
          types: getUserTypes(item)
        })),
        filters: {
          groups: [
            {
              label: 'Балансодержатель',
              value: 1,
            },
            {
              label: 'Руководитель',
              value: 2,
            },
            {
              label: 'Волонтер',
              value: 3,
            },
            {
              label: 'Модератор',
              value: 19,
            },
            ...locationCloneData.objects.filter(item => item && item.locations[0] && item.locations[0].id).map((item) => {
              return {
              label: item.name,
              value: item.locations[0].id
            };
            })
          ],
          locations: [
            ...locationCloneData.objects.filter(item => item && item.locations[0] && item.locations[0].id).map((item) => ({
              label: item.name,
              value: item.locations[0].id
            }))
          ],
          types: [
            {
              label: 'Балансодержатель',
              value: 1,
            },
            {
              label: 'Руководитель',
              value: 2,
            },
            {
              label: 'Волонтер',
              value: 3,
            },
            {
              label: 'Модератор',
              value: 19,
            },
          ],
          is_active: [
            { value: '1', label: 'Активные' },
            { value: '0', label: 'Не активные' }
          ]
        }
      };
      resolve(res);
    })
    .catch(error => reject(error));
});

export const getUser = (data) => (client) => new Promise((resolve, reject) => {
  const currentUser = getCachedData(client, 'currentUser');
  const user = client.get(urls.user(data.id));
  // const userStats = client.get(urls.userStats(data.id));
  const inactiveUsers = client.get(urls.inactiveUsers);
  const location = getCachedData(client, 'location');
  Promise.all([user, inactiveUsers, location, currentUser])
    .then(([usersData, inactiveUsersData, locationData, currentUserData]) => {
      const userStatsData = usersData.monument_stats;
      const inactiveIds = inactiveUsersData.objects.map(item => item.id);
      const locationCloneData = cloneDeep(locationData);
      const types = [
        {
          label: 'Балансодержатель',
          value: 1,
        },
        {
          label: 'Руководитель',
          value: 2,
        },
        {
          label: 'Волонтер',
          value: 3,
        },
        {
          label: 'Модератор',
          value: 19,
        }
      ];

      const locations = getLocationFromGroups(locationCloneData);
      const res = {
        ...usersData,
        created: usersData.date_joined,
        updated: usersData.date_updated,
        locations: getUserGroups(usersData.groups),
        types: getUserTypes(usersData),
        is_can_activate: !!~inactiveIds.indexOf(usersData.id),
        choices: {
          types,
          locations,
        },
        stats: {
          monuments: {
            total: userStatsData.monument_owner_total_count,
            good: userStatsData.monument_owner_status_bad_count,
            bad: userStatsData.monument_owner_status_good_count,
            unknown: userStatsData.monument_owner_status_unknown_count,
          },
          revisions: {
            owner: userStatsData.monument_revision_owner_total_count,
            assigned: {
              total: userStatsData.monument_revision_assigned_total_count,
              pending: userStatsData.monument_revision_assigned_status_pending_count,
              assigned: userStatsData.monument_revision_assigned_status_assigned_count,
              closed: userStatsData.monument_revision_assigned_status_closed_count,
            }
          }
        }
      };

      const { objects: [current_user] } = currentUserData;

      if (current_user.is_master || current_user.is_superuser) {
        if (current_user.is_master && !current_user.is_superuser) {
          res.choices.types = [
            {
              label: 'Балансодержатель',
              value: 1,
            },
            {
              label: 'Волонтер',
              value: 3,
            },
            {
              label: 'Модератор',
              value: 19,
            },
          ];
        }
      } else {
        res.is_can_activate = false;
      }

      if (res.is_can_activate && !res.types.length) {
        res.types = [
          {
            label: 'Волонтер',
            value: 3,
          }
        ];
      }

      resolve(res);
    })
    .catch(error => reject(error));
});

export const activateUser = (data) => (client) => new Promise((resolve, reject) => {
  const activate = client.patch(urls.inactiveUser(data.id), { data, type: 'raw' });
  Promise.all([activate])
    .then(() => {
      getUser(data)(client).then(res => resolve(res)).catch(error => reject(error));
    })
    .catch(error => reject(error));
});

export const changeUser = (data) => (client) => new Promise((resolve, reject) => {
  const user = client.patch(urls.user(data.id), { data: data.data });
  Promise.all([user])
    .then(() => {
      resolve();
    })
    .catch(error => reject(error));
});

export default {
  getUser,
  getUsers,
  activateUser,
  changeUser,
};
