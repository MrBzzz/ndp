export const currentUser = '/api/v1/account/current-user/';
export const user = id => `/api/v1/account/user/${id}/`;
export const userStats = id => `/api/v1/monument/user-stats/${id}/`;
export const login = '/login/';
export const logout = '/logout/';

export const monuments = '/api/v1/monument/monument/?order_by=-updated&limit=12';
export const monument = id => `/api/v1/monument/monument/${id}/`;
export const monumentNew = '/api/v1/monument/monument/';
export const monumentImages = '/api/v1/monument/image/';
export const monumentVideos = '/api/v1/monument/video/';
export const monumentImagesItem = id => `/api/v1/monument/image/${id}/`;
export const monumentVideosItem = id => `/api/v1/monument/video/${id}/`;
export const revisionByMonument = id => `/api/v1/monument/revision/?monument=${id}&limit=0`;
export const repairWorkByMonument = id => `/api/v1/monument/repair-work/?monument=${id}&limit=0`;
export const stateByMonument = id => `/api/v1/monument/state/?monument=${id}&limit=0`;

export const state = '/api/v1/monument/state/';

export const revisions = '/api/v1/monument/revision/?order_by=-updated';
export const revisionsForVolonter = user_id => `/api/v1/monument/revision/?order_by=-updated&assigned_user=${user_id}`;
export const revision = id => `/api/v1/monument/revision/${id}/`;
export const revisionNew = '/api/v1/monument/revision/';
export const revisionImages = '/api/v1/monument/revision-image/';
export const revisionVideos = '/api/v1/monument/revision-video/';
export const revisionImagesItem = id => `/api/v1/monument/revision-image/${id}/`;
export const revisionVideosItem = id => `/api/v1/monument/revision-video/${id}`;
export const revisionImagesToMonument = '/api/v1/monument/revision-image-copy-to-monument/';
export const revisionVideosToMonument = '/api/v1/monument/revision-video-copy-to-monument/';
export const revisionChange = id => `/api/v1/monument/revision-change/?monument_revision=${id}&limit=0&sort=created`;

export const usersAll = '/api/v1/account/user/?limit=0';
export const users = '/api/v1/account/user/';
export const inactiveUsers = '/api/v1/account/inactive-user/?limit=0';
export const inactiveUser = id => `/api/v1/account/inactive-user/${id}/`;
export const kinds = '/api/v1/monument/kind/?order_by=id&limit=0';
export const location = '/api/v1/monument/location/?order_by=id&limit=0';

export const monumentsSchema = '/api/v1/monument/monument/schema/';
export const revisionsSchema = '/api/v1/monument/revision/schema/';
export const stateSchema = '/api/v1/monument/state/schema/';
export const repairSchema = '/api/v1/monument/repair-work/schema/';

export const userTotalCount = '/api/v1/account/user/?limit=1';
export const monumentTotalCount = '/api/v1/monument/monument/?limit=1';
export const revisionTotalCount = '/api/v1/monument/revision/?limit=1';
export const revisionTotalCountForVolonter = (user_id) => `/api/v1/monument/revision/?limit=1&assigned_user=${user_id}`;


export default {
  revisionTotalCountForVolonter,
  revisionTotalCount,
  monumentTotalCount,
  userTotalCount,
  currentUser,
  user,
  usersAll,
  users,
  userStats,
  login,
  logout,
  monuments,
  monument,
  state,
  stateByMonument,
  monumentImagesItem,
  monumentImages,
  monumentVideos,
  monumentVideosItem,
  monumentNew,
  kinds,
  location,
  inactiveUsers,
  inactiveUser,
  revisionImagesItem,
  revisionsForVolonter,
  revisionVideosItem,
  revisionByMonument,
  revisionImagesToMonument,
  revisionVideosToMonument,
  revisionChange,
  revisionImages,
  revisionVideos,
  revision,
  revisions,
  revisionNew,
  monumentsSchema,
  revisionsSchema,
  repairSchema,
  stateSchema
};
