import m from 'moment';
import 'moment/locale/ru';
import { date as dateFormat, dateTime as dateTimeFormat } from 'settings/settings.js';

export const formatDateTime = dateTime => {
  if (!dateTime) return '';
  return m(dateTime).format(dateTimeFormat);
};

export const formatDate = date => {
  if (!date) return '';
  return m(date).format(dateFormat);
};

export const dividedIntoColumns = (array, columnsCount) => array.reduce((prev, cur, key) => {
  if (key % columnsCount === 0) prev.push([]);
  prev[prev.length - 1].push(cur);
  return prev;
}, []);

export default {
  dividedIntoColumns,
  formatDateTime,
  formatDate
};
