const isEmpty = (value) => value === undefined || value === null || value === '';
const join = (rules) => (value, data) => rules
  .map((rule) => rule(value, data))
  .filter((error) => !!error)[0];

export function required(value) {
  let error = undefined;
  if (isEmpty(value)) {
    error = 'Required';
  }
  return error;
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {};
    Object.keys(rules).forEach((key) => {
      const rule = join([].concat(rules[key]));
      const error = rule(data[key], data);
      if (error) {
        errors[key] = error;
      }
    });
    return errors;
  };
}
