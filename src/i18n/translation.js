import i18n from 'i18next';
import ru from 'settings/ru';

i18n.init({
  lng: 'ru',
  ns: [
    'auth'
  ],
  resources: {
    ru: { translation: ru }
  }
});

export default i18n;
