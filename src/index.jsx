import React from 'react';
import { render as renderDom } from 'react-dom';

import { getCurrentUser } from 'reducers/auth';
import configureStore from 'store/configureStore';
import Root from 'containers/Root/Root';
import 'images/favicon.ico';

const store = configureStore();
const render = () => renderDom(<Root store={store} />, document.getElementById('mount'));

store.dispatch(getCurrentUser()).then(render).catch(render);
