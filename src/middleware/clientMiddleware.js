export default (client) => ({ dispatch, getState }) => (next) => (action) => {
  if (typeof action === 'function') {
    return action(dispatch, getState);
  }

  const { promise, types, ...rest } = action;

  if (!promise) {
    return next(action);
  }

  const [REQUEST, SUCCESS, FAILURE] = types;
  const data = action.data || {};

  next({ ...rest, type: REQUEST, data });

  const actionPromise = promise(client);

  actionPromise.then(
    (result) => next({ ...rest, result, type: SUCCESS }),
    (error) => next({ ...rest, error, type: FAILURE })
  ).catch((error) => {
    next({ ...rest, error, type: FAILURE });
  });

  return actionPromise;
};
