import resources from 'helpers/resources/auth';

const LOAD_CURRENT_USER = 'ndp/auth/LOAD_CURRENT_USER';
const LOAD_CURRENT_USER_SUCCESS = 'ndp/auth/LOAD_CURRENT_USER_SUCCESS';
const LOAD_CURRENT_USER_FAIL = 'ndp/auth/LOAD_CURRENT_USER_FAIL';

const SEND_LOGIN = 'ndp/auth/SEND_LOGIN';
const SEND_LOGIN_SUCCESS = 'ndp/auth/SEND_LOGIN_SUCCESS';
const SEND_LOGIN_FAIL = 'ndp/auth/SEND_LOGIN_FAIL';

const SEND_LOGOUT = 'ndp/auth/SEND_LOGOUT';
const SEND_LOGOUT_SUCCESS = 'ndp/auth/SEND_LOGOUT_SUCCESS';
const SEND_LOGOUT_FAIL = 'ndp/auth/SEND_LOGOUT_FAIL';

const initialState = { data: {} };

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case LOAD_CURRENT_USER:
      return state;
    case LOAD_CURRENT_USER_SUCCESS:
      return {
        ...state,
        data: action.result
      };
    case LOAD_CURRENT_USER_FAIL:
      return {
        ...state,
        data: {}
      };

    case SEND_LOGIN:
      return state;
    case SEND_LOGIN_SUCCESS:
      return {
        ...state,
        data: action.result
      };
    case SEND_LOGIN_FAIL:
      return {
        ...state,
        data: {}
      };

    case SEND_LOGOUT:
      return state;
    case SEND_LOGOUT_SUCCESS:
      return {
        ...state,
        data: {}
      };
    case SEND_LOGOUT_FAIL:
      return {
        ...state,
        data: {}
      };

    default:
      return state;
  }
}

export const getCurrentUser = () => ({
  types: [LOAD_CURRENT_USER, LOAD_CURRENT_USER_SUCCESS, LOAD_CURRENT_USER_FAIL],
  promise: resources.getCurrentUser()
});

export const login = (data) => ({
  types: [SEND_LOGIN, SEND_LOGIN_SUCCESS, SEND_LOGIN_FAIL],
  promise: resources.login(data)
});

export const logout = () => ({
  types: [SEND_LOGOUT, SEND_LOGOUT_SUCCESS, SEND_LOGOUT_FAIL],
  promise: resources.logout()
});

