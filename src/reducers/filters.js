const CHANGE_FILTERS = 'ndp/filter/CHANGE_FILTERS';

const initialState = {
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CHANGE_FILTERS: {
      const type = action.data.type;
      const page = action.data.page;
      const value = action.data.value;
      const filter = { ...state };
      filter[page] = state[page] ? { ...state[page] } : {};
      filter[page][type] = value;
      return filter;
    }
    default:
      return state;
  }
}

export const changeFilter = (data) => ({
  type: CHANGE_FILTERS,
  data
});

