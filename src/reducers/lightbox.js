const OPEN = 'ndp/lightbox/OPEN';
const CLOSE = 'ndp/lightbox/CLOSE';
const NEXT = 'ndp/lightbox/NEXT';
const PREV = 'ndp/lightbox/PREV';

const initialState = {
  lightboxIsOpen: false,
  currentImage: 0
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case OPEN:
      return {
        ...state,
        lightboxIsOpen: true,
        currentImage: action.data.currentImage
      };
    case CLOSE:
      return {
        ...state,
        ...initialState
      };
    case NEXT:
      return {
        ...state,
        currentImage: state.currentImage + 1
      };
    case PREV:
      return {
        ...state,
        currentImage: state.currentImage - 1
      };
    default:
      return state;
  }
}


export const openLightbox = (currentImage) => ({
  type: OPEN,
  data: {
    currentImage
  }
});

export const closeLightbox = () => ({
  type: CLOSE
});

export const nextLightbox = () => ({
  type: NEXT
});

export const prevLightbox = () => ({
  type: PREV
});

