import { cloneDeep, findIndex } from 'lodash';
import resources from 'helpers/resources/monuments';

const LOAD_MONUMENTS = 'ndp/monuments/LOAD_MONUMENTS';
const LOAD_MONUMENTS_SUCCESS = 'ndp/monuments/LOAD_MONUMENTS_SUCCESS';
const LOAD_MONUMENTS_FAIL = 'ndp/monuments/LOAD_MONUMENTS_FAIL';

const LOAD_ALL_MONUMENTS = 'ndp/monuments/LOAD_ALL_MONUMENTS';
const LOAD_ALL_MONUMENTS_SUCCESS = 'ndp/monuments/LOAD_ALL_MONUMENTS_SUCCESS';
const LOAD_ALL_MONUMENTS_FAIL = 'ndp/monuments/LOAD_ALL_MONUMENTS_FAIL';

const LOAD_MONUMENT = 'ndp/monuments/LOAD_MONUMENT';
const LOAD_MONUMENT_SUCCESS = 'ndp/monuments/LOAD_MONUMENT_SUCCESS';
const LOAD_MONUMENT_FAIL = 'ndp/monuments/LOAD_MONUMENT_FAIL';

const CHANGE_COORDINATES = 'ndp/monuments/CHANGE_COORDINATES';

const UPDATE_MONUMENT = 'ndp/monuments/UPDATE_MONUMENT';
const UPDATE_MONUMENT_SUCCESS = 'ndp/monuments/UPDATE_MONUMENT_SUCCESS';
const UPDATE_MONUMENT_FAIL = 'ndp/monuments/UPDATE_MONUMENT_FAIL';

const CREATE_MONUMENT = 'ndp/monuments/CREATE_MONUMENT';
const CREATE_MONUMENT_SUCCESS = 'ndp/monuments/CREATE_MONUMENT_SUCCESS';
const CREATE_MONUMENT_FAIL = 'ndp/monuments/CREATE_MONUMENT_FAIL';

const PREPARE_CREATE_MONUMENT = 'ndp/monuments/PREPARE_CREATE_MONUMENT';
const PREPARE_CREATE_MONUMENT_SUCCESS = 'ndp/monuments/PREPARE_CREATE_MONUMENT_SUCCESS';
const PREPARE_CREATE_MONUMENT_FAIL = 'ndp/monuments/PREPARE_CREATE_MONUMENT_FAIL';

const REMOVE_MONUMENT = 'ndp/monuments/REMOVE_MONUMENT';
const REMOVE_MONUMENT_SUCCESS = 'ndp/monuments/REMOVE_MONUMENT_SUCCESS';
const REMOVE_MONUMENT_FAIL = 'ndp/monuments/REMOVE_MONUMENT_FAIL';

const UPLOAD_MONUMENT_MEDIA = 'ndp/monument/UPLOAD_MONUMENT_MEDIA';
const UPLOAD_MONUMENT_MEDIA_SUCCESS = 'ndp/monument/UPLOAD_MONUMENT_MEDIA_SUCCESS';
const UPLOAD_MONUMENT_MEDIA_FAIL = 'ndp/monument/UPLOAD_MONUMENT_MEDIA_FAIL';

const REMOVE_MONUMENT_MEDIA = 'ndp/monument/REMOVE_MONUMENT_MEDIA';
const REMOVE_MONUMENT_MEDIA_SUCCESS = 'ndp/monument/REMOVE_MONUMENT_MEDIA_SUCCESS';
const REMOVE_MONUMENT_MEDIA_FAIL = 'ndp/monument/REMOVE_MONUMENT_MEDIA_FAIL';

const CHANGE_ORDER_MONUMENT_MEDIA = 'ndp/monument/CHANGE_ORDER_MONUMENT_MEDIA';
const CHANGE_ORDER_MONUMENT_MEDIA_SUCCESS = 'ndp/monument/CHANGE_ORDER_MONUMENT_MEDIA_SUCCESS';
const CHANGE_ORDER_MONUMENT_MEDIA_FAIL = 'ndp/monument/CHANGE_ORDER_MONUMENT_MEDIA_FAIL';

const TOGGLE_MAP = 'ndp/monument/TOGGLE_MAP';

const initialState = {
  monuments: {},
  monument: {},
  updating: false,
  newMonument: {},
  mapIsOpen: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TOGGLE_MAP: {
      return {
        ...state,
        mapIsOpen: !state.mapIsOpen
      };
    }

    case LOAD_MONUMENTS: {
      return {
        ...state,
        monuments: {}
      };
    }
    case LOAD_MONUMENTS_SUCCESS: {
      return {
        ...state,
        monuments: action.result
      };
    }
    case LOAD_MONUMENTS_FAIL: {
      return {
        ...state,
        monuments: {}
      };
    }

    case LOAD_ALL_MONUMENTS: {
      return {
        ...state,
        monuments: {}
      };
    }
    case LOAD_ALL_MONUMENTS_SUCCESS: {
      return {
        ...state,
        monuments: action.result
      };
    }
    case LOAD_ALL_MONUMENTS_FAIL: {
      return {
        ...state,
        monuments: {}
      };
    }

    case LOAD_MONUMENT: {
      return {
        ...state,
        monument: {}
      };
    }
    case LOAD_MONUMENT_SUCCESS: {
      return {
        ...state,
        monument: action.result
      };
    }
    case LOAD_MONUMENT_FAIL: {
      return {
        ...state,
        monument: {}
      };
    }

    case CHANGE_COORDINATES: {
      const data = action.data;
      const monument = cloneDeep(state.monument);
      monument.properties.lat = data.lat;
      monument.properties.lng = data.lng;
      return {
        ...state,
        monument
      };
    }

    case UPDATE_MONUMENT: {
      return {
        ...state,
        updating: true
      };
    }
    case UPDATE_MONUMENT_SUCCESS: {
      return {
        ...state,
        updating: false
      };
    }
    case UPDATE_MONUMENT_FAIL: {
      return {
        ...state,
        updating: false
      };
    }

    case UPLOAD_MONUMENT_MEDIA: {
      const files = action.data.files;
      const monument = cloneDeep(state.monument);

      files.map(file => {
        if (~file.type.indexOf('image')) {
          monument.properties.images.push({});
        } else if (~file.type.indexOf('video')) {
          monument.properties.videos.push({});
        }
        return null;
      });

      return {
        ...state,
        monument
      };
    }
    case UPLOAD_MONUMENT_MEDIA_SUCCESS: {
      const monument = cloneDeep(state.monument);
      monument.properties.images = action.result.images;
      monument.properties.videos = action.result.videos;
      return {
        ...state,
        monument
      };
    }
    case UPLOAD_MONUMENT_MEDIA_FAIL: {
      return state;
    }

    case REMOVE_MONUMENT_MEDIA: {
      const media = action.data;
      const monument = cloneDeep(state.monument);
      if (media.type === 'image') {
        const i = findIndex(monument.properties.images, (item) => item.id === media.id);
        monument.properties.images[i] = {};
      } else if (media.type === 'video') {
        const i = findIndex(monument.properties.videos, (item) => item.id === media.id);
        monument.properties.videos[i] = {};
      }
      return {
        ...state,
        monument
      };
    }
    case REMOVE_MONUMENT_MEDIA_SUCCESS: {
      const monument = cloneDeep(state.monument);
      monument.properties.images = action.result.images;
      monument.properties.videos = action.result.videos;
      return {
        ...state,
        monument
      };
    }
    case REMOVE_MONUMENT_MEDIA_FAIL: {
      return state;
    }

    case REMOVE_MONUMENT: {
      return state;
    }
    case REMOVE_MONUMENT_SUCCESS: {
      return state;
    }
    case REMOVE_MONUMENT_FAIL: {
      return state;
    }

    case CREATE_MONUMENT: {
      return {
        ...state,
        monument: {},
        newMonument: {}
      };
    }
    case CREATE_MONUMENT_SUCCESS: {
      return {
        ...state,
        monument: {},
        newMonument: action.result,
      };
    }
    case CREATE_MONUMENT_FAIL: {
      return {
        ...state,
        monument: {},
        newMonument: {},
      };
    }

    case PREPARE_CREATE_MONUMENT: {
      return {
        ...state,
        monument: {},
        newMonument: {}
      };
    }
    case PREPARE_CREATE_MONUMENT_SUCCESS: {
      return {
        ...state,
        monument: action.result
      };
    }
    case PREPARE_CREATE_MONUMENT_FAIL: {
      return {
        ...state,
        monument: {}
      };
    }

    case CHANGE_ORDER_MONUMENT_MEDIA: {
      const monument = cloneDeep(state.monument);
      monument.properties.images = monument.properties.images.map(() => ({}));
      return {
        ...state,
        monument
      };
    }
    case CHANGE_ORDER_MONUMENT_MEDIA_SUCCESS: {
      const monument = cloneDeep(state.monument);
      monument.properties.images = action.result.images;
      monument.properties.videos = action.result.videos;
      return {
        ...state,
        monument
      };
    }
    case CHANGE_ORDER_MONUMENT_MEDIA_FAIL: {
      return state;
    }

    default: {
      return state;
    }
  }
}

export const toggleMap = () => ({
  type: TOGGLE_MAP
});

export const getMonuments = (data) => ({
  types: [LOAD_MONUMENTS, LOAD_MONUMENTS_SUCCESS, LOAD_MONUMENTS_FAIL],
  promise: resources.getMonuments(data)
});

export const getAllMonuments = () => ({
  types: [LOAD_ALL_MONUMENTS, LOAD_ALL_MONUMENTS_SUCCESS, LOAD_ALL_MONUMENTS_FAIL],
  promise: resources.getAllMonuments()
});

export const getMonument = (data) => ({
  types: [LOAD_MONUMENT, LOAD_MONUMENT_SUCCESS, LOAD_MONUMENT_FAIL],
  promise: resources.getMonument(data)
});

export const changeCoordinates = (data) => ({
  type: CHANGE_COORDINATES,
  data: {
    ...data
  }
});

export const updateMonument = (data) => ({
  types: [UPDATE_MONUMENT, UPDATE_MONUMENT_SUCCESS, UPDATE_MONUMENT_FAIL],
  promise: resources.updateMonument(data)
});

export const uploadMonumentMedia = (data) => ({
  types: [UPLOAD_MONUMENT_MEDIA, UPLOAD_MONUMENT_MEDIA_SUCCESS, UPLOAD_MONUMENT_MEDIA_FAIL],
  promise: resources.uploadMonumentMedia(data),
  data
});

export const onChangeSortFiles = (data) => ({
  types: [CHANGE_ORDER_MONUMENT_MEDIA, CHANGE_ORDER_MONUMENT_MEDIA_SUCCESS, CHANGE_ORDER_MONUMENT_MEDIA_FAIL],
  promise: resources.changeSortFiles(data),
  data
});

export const removeMonumentMedia = (data) => ({
  types: [REMOVE_MONUMENT_MEDIA, REMOVE_MONUMENT_MEDIA_SUCCESS, REMOVE_MONUMENT_MEDIA_FAIL],
  promise: resources.removeMonumentMedia(data),
  data
});

export const removeMonument = (data) => ({
  types: [REMOVE_MONUMENT, REMOVE_MONUMENT_SUCCESS, REMOVE_MONUMENT_FAIL],
  promise: resources.removeMonument(data),
});

export const createMonument = (data) => ({
  types: [CREATE_MONUMENT, CREATE_MONUMENT_SUCCESS, CREATE_MONUMENT_FAIL],
  promise: resources.createMonument(data),
});

export const getDataForNewMonument = (data) => ({
  types: [PREPARE_CREATE_MONUMENT, PREPARE_CREATE_MONUMENT_SUCCESS, PREPARE_CREATE_MONUMENT_FAIL],
  promise: resources.getDataForNewMonument(data),
});
