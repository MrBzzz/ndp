import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import auth from 'reducers/auth';
import monuments from 'reducers/monuments';
import revisions from 'reducers/revisions';
import filters from 'reducers/filters';
import lightbox from 'reducers/lightbox';
import users from 'reducers/users';

const rootReducer = combineReducers({
  users,
  lightbox,
  routing,
  form,
  monuments,
  revisions,
  filters,
  auth
});

export default rootReducer;
