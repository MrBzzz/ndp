import { cloneDeep, findIndex } from 'lodash';
import resources from 'helpers/resources/revisions';

const LOAD_REVISIONS = 'ndp/revision/LOAD_REVISIONS';
const LOAD_REVISIONS_SUCCESS = 'ndp/revision/LOAD_REVISIONS_SUCCESS';
const LOAD_REVISIONS_FAIL = 'ndp/revision/LOAD_REVISIONS_FAIL';

const LOAD_REVISION = 'ndp/revision/LOAD_REVISION';
const LOAD_REVISION_SUCCESS = 'ndp/revision/LOAD_REVISION_SUCCESS';
const LOAD_REVISION_FAIL = 'ndp/revision/LOAD_REVISION_FAIL';

const PREPARE_CREATE_REVISIONS = 'ndp/revision/PREPARE_CREATE_REVISIONS';
const PREPARE_CREATE_REVISIONS_SUCCESS = 'ndp/revision/PREPARE_CREATE_REVISIONS_SUCCESS';
const PREPARE_CREATE_REVISIONS_FAIL = 'ndp/revision/PREPARE_CREATE_REVISIONS_FAIL';

const UPDATE_REVISION = 'ndp/revision/UPDATE_REVISION';
const UPDATE_REVISION_SUCCESS = 'ndp/revision/UPDATE_REVISION_SUCCESS';
const UPDATE_REVISION_FAIL = 'ndp/revision/UPDATE_REVISION_FAIL';

const CREATE_REVISION = 'ndp/revision/CREATE_REVISION';
const CREATE_REVISION_SUCCESS = 'ndp/revision/CREATE_REVISION_SUCCESS';
const CREATE_REVISION_FAIL = 'ndp/revision/CREATE_REVISION_FAIL';

const REMOVE_REVISION = 'ndp/revision/REMOVE_REVISION';
const REMOVE_REVISION_SUCCESS = 'ndp/revision/REMOVE_REVISION_SUCCESS';
const REMOVE_REVISION_FAIL = 'ndp/revision/REMOVE_REVISION_FAIL';

const UPLOAD_REVISION_MEDIA = 'ndp/revision/UPLOAD_REVISION_MEDIA';
const UPLOAD_REVISION_MEDIA_SUCCESS = 'ndp/revision/UPLOAD_REVISION_MEDIA_SUCCESS';
const UPLOAD_REVISION_MEDIA_FAIL = 'ndp/revision/UPLOAD_REVISION_MEDIA_FAIL';

const REMOVE_REVISION_MEDIA = 'ndp/revision/REMOVE_REVISION_MEDIA';
const REMOVE_REVISION_MEDIA_SUCCESS = 'ndp/revision/REMOVE_REVISION_MEDIA_SUCCESS';
const REMOVE_REVISION_MEDIA_FAIL = 'ndp/revision/REMOVE_REVISION_MEDIA_FAIL';

const MOVE_REVISION_MEDIA = 'ndp/revision/MOVE_REVISION_MEDIA';
const MOVE_REVISION_MEDIA_SUCCESS = 'ndp/revision/MOVE_REVISION_MEDIA_SUCCESS';
const MOVE_REVISION_MEDIA_FAIL = 'ndp/revision/MOVE_REVISION_MEDIA_FAIL';

const CHANGE_REVISION_COORDINATES = 'ndp/revision/CHANGE_REVISION_COORDINATES';

const MOVE_REVISION_COORDINATES = 'ndp/revision/MOVE_REVISION_COORDINATES';
const MOVE_REVISION_COORDINATES_SUCCESS = 'ndp/revision/MOVE_REVISION_COORDINATES_SUCCESS';
const MOVE_REVISION_COORDINATES_FAIL = 'ndp/revision/MOVE_REVISION_COORDINATES_FAIL';

const initialState = {
  revisions: {},
  revision: {},
  updating: false,
  copy: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case LOAD_REVISIONS: {
      return {
        ...state,
        revisions: {},
        copy: false,
      };
    }
    case LOAD_REVISIONS_SUCCESS: {
      return {
        ...state,
        revisions: action.result,
        copy: false,
      };
    }
    case LOAD_REVISIONS_FAIL: {
      return {
        ...state,
        data: {},
      };
    }

    case MOVE_REVISION_COORDINATES: {
      return {
        ...state,
        copy: false,
      }
    }
    case MOVE_REVISION_COORDINATES_SUCCESS: {
      return {
        ...state,
        copy: true,
      }
    }
    case MOVE_REVISION_COORDINATES_FAIL: {
      return {
        ...state,
        copy: false,
      }
    }


    case LOAD_REVISION: {
      return {
        ...state,
        revision: {}
      };
    }
    case LOAD_REVISION_SUCCESS: {
      return {
        ...state,
        revision: action.result
      };
    }
    case LOAD_REVISION_FAIL: {
      return {
        ...state,
        revision: {}
      };
    }

    case UPDATE_REVISION: {
      return {
        ...state,
        updating: true
      };
    }
    case UPDATE_REVISION_SUCCESS: {
      return {
        ...state,
        updating: false
      };
    }
    case UPDATE_REVISION_FAIL: {
      return {
        ...state,
        updating: false
      };
    }

    case CHANGE_REVISION_COORDINATES: {
      const data = action.data;
      const result = { ...state };
      const properties = result.revision.properties;
      result.revision.properties = {
        ...properties,
        ...data
      };
      return result;
    }

    case UPLOAD_REVISION_MEDIA: {
      const files = action.data.files;
      const revision = cloneDeep(state.revision);

      files.map(file => {
        if (~file.type.indexOf('image')) {
          revision.properties.images.push({});
        } else if (~file.type.indexOf('video')) {
          revision.properties.videos.push({});
        }
        return null;
      });

      return {
        ...state,
        revision
      };
    }
    case UPLOAD_REVISION_MEDIA_SUCCESS: {
      const revision = cloneDeep(state.revision);
      revision.properties.images = action.result.images;
      revision.properties.videos = action.result.videos;
      return {
        ...state,
        revision
      };
    }
    case UPLOAD_REVISION_MEDIA_FAIL: {
      return state;
    }

    case CREATE_REVISION: {
      return {
        ...state,
        revision: {},
        updating: true
      };
    }
    case CREATE_REVISION_SUCCESS: {
      return {
        ...state,
        revision: {},
        updating: false
      };
    }
    case CREATE_REVISION_FAIL: {
      return {
        ...state,
        revision: {},
        updating: false
      };
    }

    case REMOVE_REVISION_MEDIA: {
      const media = action.data;
      const revision = cloneDeep(state.revision);

      if (media.type === 'image') {
        const i = findIndex(revision.properties.images, (item) => item.id === media.id);
        revision.properties.images[i] = {};
      } else if (media.type === 'video') {
        const i = findIndex(revision.properties.videos, (item) => item.id === media.id);
        revision.properties.videos[i] = {};
      }

      return {
        ...state,
        revision
      };
    }
    case REMOVE_REVISION_MEDIA_SUCCESS: {
      const revision = cloneDeep(state.revision);
      revision.properties.images = action.result.images;
      revision.properties.videos = action.result.videos;
      return {
        ...state,
        revision
      };
    }
    case REMOVE_REVISION_MEDIA_FAIL: {
      return state;
    }

    case MOVE_REVISION_MEDIA: {
      const media = action.data;
      const revision = cloneDeep(state.revision);

      if (media.type === 'image') {
        const i = findIndex(revision.properties.images, (item) => item.id === media.id);
        revision.properties.images[i] = {};
      } else if (media.type === 'video') {
        const i = findIndex(revision.properties.videos, (item) => item.id === media.id);
        revision.properties.videos[i] = {};
      }

      return {
        ...state,
        revision
      };
    }
    case MOVE_REVISION_MEDIA_SUCCESS: {
      const revision = cloneDeep(state.revision);
      revision.properties.images = action.result.images;
      revision.properties.videos = action.result.videos;
      revision.properties.monument_object.images = action.result.monument_object.images;
      revision.properties.monument_object.videos = action.result.monument_object.videos;
      return {
        ...state,
        revision
      };
    }
    case MOVE_REVISION_MEDIA_FAIL: {
      return state;
    }

    case PREPARE_CREATE_REVISIONS: {
      return {
        ...state,
        revision: {}
      };
    }
    case PREPARE_CREATE_REVISIONS_SUCCESS: {
      return {
        ...state,
        revision: action.result
      };
    }
    case PREPARE_CREATE_REVISIONS_FAIL: {
      return state;
    }

    case REMOVE_REVISION: {
      return state;
    }
    case REMOVE_REVISION_SUCCESS: {
      return state;
    }
    case REMOVE_REVISION_FAIL: {
      return state;
    }

    default: {
      return state;
    }
  }
}

export const getRevisions = (data) => ({
  types: [LOAD_REVISIONS, LOAD_REVISIONS_SUCCESS, LOAD_REVISIONS_FAIL],
  promise: resources.getRevisions(data)
});

export const getRevision = (data) => ({
  types: [LOAD_REVISION, LOAD_REVISION_SUCCESS, LOAD_REVISION_FAIL],
  promise: resources.getRevision(data)
});

export const updateRevision = (data) => ({
  types: [UPDATE_REVISION, UPDATE_REVISION_SUCCESS, UPDATE_REVISION_FAIL],
  promise: resources.updateRevision(data)
});

export const changeRevisionCoordinates = (data) => ({
  type: CHANGE_REVISION_COORDINATES,
  data: {
    ...data
  }
});

export const uploadRevisionMedia = (data) => ({
  types: [UPLOAD_REVISION_MEDIA, UPLOAD_REVISION_MEDIA_SUCCESS, UPLOAD_REVISION_MEDIA_FAIL],
  promise: resources.uploadRevisionMedia(data),
  data
});

export const removeRevisionMedia = (data) => ({
  types: [REMOVE_REVISION_MEDIA, REMOVE_REVISION_MEDIA_SUCCESS, REMOVE_REVISION_MEDIA_FAIL],
  promise: resources.removeRevisionMedia(data),
  data
});

export const moveRevisionMedia = (data) => ({
  types: [MOVE_REVISION_MEDIA, MOVE_REVISION_MEDIA_SUCCESS, MOVE_REVISION_MEDIA_FAIL],
  promise: resources.moveRevisionMedia(data),
  data
});

export const getDataForNewRevision = (data) => ({
  types: [PREPARE_CREATE_REVISIONS, PREPARE_CREATE_REVISIONS_SUCCESS, PREPARE_CREATE_REVISIONS_FAIL],
  promise: resources.getDataForNewRevision(data),
});

export const createRevision = (data) => ({
  types: [CREATE_REVISION, CREATE_REVISION_SUCCESS, CREATE_REVISION_FAIL],
  promise: resources.createRevision(data),
});

export const removeRevision = (data) => ({
  types: [REMOVE_REVISION, REMOVE_REVISION_SUCCESS, REMOVE_REVISION_FAIL],
  promise: resources.removeRevision(data),
});

export const moveRevisionCoordinate = (data) => ({
  types: [MOVE_REVISION_COORDINATES, MOVE_REVISION_COORDINATES_SUCCESS, MOVE_REVISION_COORDINATES_FAIL],
  promise: resources.moveRevisionCoordinate(data),
});
