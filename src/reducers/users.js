import resources from 'helpers/resources/users';

const LOAD_USERS = 'ndp/users/LOAD_USERS';
const LOAD_USERS_SUCCESS = 'ndp/users/LOAD_USERS_SUCCESS';
const LOAD_USERS_FAIL = 'ndp/users/LOAD_USERS_FAIL';

const LOAD_USER = 'ndp/users/LOAD_USER';
const LOAD_USER_SUCCESS = 'ndp/users/LOAD_USER_SUCCESS';
const LOAD_USER_FAIL = 'ndp/users/LOAD_USER_FAIL';

const ACTIVATE_USER = 'ndp/users/ACTIVATE_USER';
const ACTIVATE_USER_SUCCESS = 'ndp/users/ACTIVATE_USER_SUCCESS';
const ACTIVATE_USER_FAIL = 'ndp/users/ACTIVATE_USER_FAIL';

const DEACTIVATE_USER = 'ndp/users/DEACTIVATE_USER';
const DEACTIVATE_USER_SUCCESS = 'ndp/users/DEACTIVATE_USER_SUCCESS';
const DEACTIVATE_USER_FAIL = 'ndp/users/DEACTIVATE_USER_FAIL';

const CHANGE_USER = 'ndp/users/CHANGE_USER';
const CHANGE_USER_SUCCESS = 'ndp/users/CHANGE_USER_SUCCESS';
const CHANGE_USER_FAIL = 'ndp/users/CHANGE_USER_FAIL';

const initialState = {
  users: {},
  user: {},
  updating: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case LOAD_USERS: {
      return {
        ...state,
        users: {}
      };
    }
    case LOAD_USERS_SUCCESS: {
      return {
        ...state,
        users: action.result
      };
    }
    case LOAD_USERS_FAIL: {
      return {
        ...state,
        users: {}
      };
    }

    case LOAD_USER: {
      return {
        ...state,
        user: {},
        users: {}
      };
    }
    case LOAD_USER_SUCCESS: {
      return {
        ...state,
        user: action.result,
        users: {},
      };
    }
    case LOAD_USER_FAIL: {
      return {
        ...state,
        user: {},
        users: {},
      };
    }

    case ACTIVATE_USER: {
      return {
        ...state,
        user: {}
      };
    }
    case ACTIVATE_USER_SUCCESS: {
      return {
        ...state,
        user: action.result,
      };
    }
    case ACTIVATE_USER_FAIL: {
      return {
        ...state,
        user: {}
      };
    }

    case CHANGE_USER: {
      return {
        ...state,
        updating: true
      };
    }
    case CHANGE_USER_SUCCESS: {
      return {
        ...state,
        updating: false
      };
    }
    case CHANGE_USER_FAIL: {
      return {
        ...state,
        updating: false
      };
    }

    case DEACTIVATE_USER: {
      return state;
    }
    case DEACTIVATE_USER_SUCCESS: {
      return state;
    }
    case DEACTIVATE_USER_FAIL: {
      return state;
    }

    default: {
      return state;
    }
  }
}

export const getUsers = (data) => ({
  types: [LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_FAIL],
  promise: resources.getUsers(data)
});

export const getUser = (data) => ({
  types: [LOAD_USER, LOAD_USER_SUCCESS, LOAD_USER_FAIL],
  promise: resources.getUser(data)
});

export const activateUser = (data) => ({
  types: [ACTIVATE_USER, ACTIVATE_USER_SUCCESS, ACTIVATE_USER_FAIL],
  promise: resources.activateUser(data)
});

export const changeUser = (data) => ({
  types: [CHANGE_USER, CHANGE_USER_SUCCESS, CHANGE_USER_FAIL],
  promise: resources.changeUser(data)
});

export const deactivateUser = (data) => ({
  types: [DEACTIVATE_USER, DEACTIVATE_USER_SUCCESS, DEACTIVATE_USER_FAIL],
  promise: resources.activateUser(data)
});
