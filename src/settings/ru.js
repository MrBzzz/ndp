export default {
  fld: {
    bool: {
      yes: 'Да',
      no: 'Нет'
    }
  },
  breadcrumbs: {
    main: 'Главная',
    revisions: 'Осмотры',
    edit: 'Редактирование',
    add_revision: 'Создание осмотра',
    add_monument: 'Создание памятника'
  },
  tooltip: {
    add_revision: 'Вы не можете создать новый осмотр пока не закрыт текущий.',
  },
  modal: {
    move_media: {
      title: 'Перенести файл',
      text: 'Вы дейстивительно хотите перенести файл?'
    },
    change_sort: {
      title: 'Изменить главную фотографию?',
      text: 'Изменить главную фотографию?'
    },
    remove_media: {
      title: 'Удалить файл',
      text: 'Вы дейстивительно хотите удалить файл?'
    },
    remove_monument: {
      title: 'Удалить памятник',
      text: 'Вы дейстивительно хотите удалить памятник?'
    },
    remove_revision: {
      title: 'Удалить осмотр',
      text: 'Вы дейстивительно хотите удалить осмотр?'
    },
    resolve_revision: {
      title: 'Завершить осмотр',
      text: 'Вы дейстивительно хотите завершить осмотр?'
    },
  },
  buttons: {
    change: 'Изменить',
    move: 'Перенести',
    create: 'Создать',
    edit: 'Редактировать',
    save: 'Сохранить',
    remove: 'Удалить',
    show: 'Показать',
    cancel: 'Отмена',
    resolve: 'Завершить',
    add_revision: 'Добавить осмотр',
    show_map: 'Показать карту',
    hide_map: 'Скрыть карту',
  },
  history: {
    create: 'Дата создания',
    edit: 'Дата редактирования',
    title: 'История',
    comment: 'Комментарий',
    text: 'Дополнительная информация',
    assigned_user: 'Пользователь назначен',
    user: 'Создатель',
    status: 'Статус осмотра',
    monument_status: 'Статус cостояние памятника',
    from: 'изменилось с',
    to: 'на',
  },
  user: {
    username: 'Имя пользователя',
    email: 'Адрес электронной почты',
    first_name: 'Имя',
    last_name: 'Фамилия',
    phone: 'Телефон',
    organization: 'Организация',
    position: 'Должность',
    is_ballansholder: 'Балансодержатель',
    is_moderator: 'Модератор',
    is_superuser: 'Суперпользователь',
    is_master: 'Руководитель',
    is_volunteer: 'Волонтер',
    group: 'Группы:',
    type: 'Тип пользователя:',
    monument: 'Памятники',
    revision: 'Осмотры',
    work: 'Работы',
    write: 'Написать',
    stats: {
      monument: {
        total: {
          alt: 'Количество памятников созданных пользователем'
        },
        good: {
          alt: 'В плохом состоянии',
          title: 'удовл.',
        },
        bad: {
          alt: 'В хорошем состоянии',
          title: 'неудовл.',
        },
        unknown: {
          alt: 'В неизвестном состоянии',
          title: 'не уточнено',
        },
      },
      revision: {
        new: 'новые',
        in_work: 'в работе',
        done: 'завершены',
        owner: 'Количество осмотров созданных пользователем',
        assigned: {
          total: 'Осмотры в которых был исполнителем.',
          pending: 'Новые осмотры',
          assigned: 'Осмотры в работе',
          closed: 'Завершенные осмотры',
        }
      }
    }
  },
  media: {
    title: 'Файлы'
  },
  images: {
    placeholdit: 'Нет фотографии'
  },
  title: 'Народный Дозор Памяти',
  navbar: {
    logo: 'НДП',
    links: {
      monuments: 'Памятники',
      revisions: 'Осмотры',
      monuments_map: 'Карта',
      works: 'Работы',
      users: 'Пользователи'
    }
  },
  pagination: {
    first: 'Первая стриница',
    last: 'Последняя страница',
  },
  signin: {
    title: 'Народный Дозор Памяти',
    placeholders: {
      username: 'Имя пользователя',
      password: 'Пароль'
    },
    btn: {
      login: 'Войти'
    },
    errors: {
      403: 'Неправильно указано имя пользователя или пароль'
    }
  },
  revisions: {
    title: 'Осмотры'
  },
  works: {
    title: 'Работы'
  },
  revision: {
    change: 'Изменен: ',
    assigned: 'Назначен: ',
    created: 'Создан: ',
    watch: 'Обект осматривался',
    notWatch: 'Обект не осматривался'
  },
  filter: {
    empty_option: '--',
    labels: {
      from: 'c',
      to: 'по',
      is_active: 'Сотояние',
      search: 'Поиск',
      kind: 'Тип объекта',
      user_kind: 'Тип',
      revision_status: 'Состояние',
      monument_status: 'Состояние',
      repair_status: 'Статус работы',
      status: 'Статус работы',
      location: 'Локация',
      geoportal: 'Публикация на геопортале',
      user: 'Пользователь',
      groups: 'Группа',
    },
    placeholders: {
      from: 'c',
      to: 'по',
      is_active: 'Сотояние',
      groups: 'Группа',
      search: 'Поиск',
      kind: 'Тип объекта',
      user_kind: 'Тип',
      revision_status: 'Состояние',
      monument_status: 'Состояние',
      repair_status: 'Статус работы',
      status: 'Статус',
      location: 'Локация',
      user: 'Пользователя',
      geoportal: 'Публикация на геопортале'
    },
    geoportal: {
      published: 'Да',
      unpublished: 'Нет'
    }
  },
  tables: {
    variants: {
      yes: 'Да',
      no: 'Нет',
    },
    revision_monument_object_revisions_total_count: 'Всего осмотров',
    revision_monument_object_name: 'Осматриваемый объект',
    revision_monument_object_kind: 'Тип осматриваемого объекта',
    revision_monument_status: 'Состояние памятника',
    revision_status: 'Статус осмотра',
    revision_updated: 'Дата осмотра',
    revision_user: 'Создатель',
    revision_assigned_user: 'Назначено',
    revision_comment_last: 'Комментарий',
    revision_text: 'Дополнительная информация',
    monument_id: 'ID',
    monument_current_state_status: 'Состояние памятника',
    monument_name: 'Название',
    monument_created: 'Создано',
    monument_updated: 'Обновлено',
    monument_code: 'Код',
    monument_is_published: 'Опубликовано',
    monument_is_published_on_geoportal: 'Опубликовано на геопортале',
    monument_location: 'Локация',
    monument_kind: 'Тип',
    monument_address: 'Адрес',
    monument_address_more: 'Дополнительное описание местонахождения',
    monument_number_of_dead: 'Кол-во захороненных',
    monument_number_of_dead_known: 'Кол-во известных захороненных',
    monument_number_of_dead_unknown: 'Кол-во неизвестных захороненных',
    monument_build_date: 'Дата постройки',
    monument_history_info: 'Краткая историческая справка',
    monument_owner: 'Собственник',
    monument_owner_address: 'Адрес собственника',
    monument_responsible_person_1: 'Контактное лицо 1',
    monument_responsible_person_phone_1: 'Тел. 1',
    monument_responsible_person_email_1: 'E-mail 1',
    monument_responsible_person_2: 'Контактное лицо 2',
    monument_responsible_person_phone_2: 'Тел. 2',
    monument_responsible_person_email_2: 'E-mail 2',
  },
  form: {
    status: {
      label: 'Статус осмотра',
      placeholder: 'Статус осмотра'
    },
    monument_status: {
      label: 'Состояние памятника',
      placeholder: 'Состояние памятника'
    },
    user: {
      label: 'Создатель',
      placeholder: 'Создатель'
    },
    assigned_user: {
      label: 'Назначено',
      placeholder: 'Назначено'
    },
    geometry: {
      label: 'Координаты',
      placeholder: 'Координаты'
    },
    text: {
      label: 'Дополнительная информация	',
      placeholder: 'Дополнительная информация	'
    },
    comment_last: {
      label: 'Комментарий',
      placeholder: 'Комментарий'
    }
  },
  forms: {
    monument: {
      current_state_status: {
        label: 'Состояние памятника',
        placeholder: 'Состояние памятника'
      },
      geometry: {
        label: 'Координаты',
        placeholder: 'Координаты'
      },
      id: {
        label: 'ID',
        placeholder: 'ID'
      },
      name: {
        label: 'Название',
        placeholder: 'Название'
      },
      formatted_created: {
        label: 'Создано',
        placeholder: 'Создано'
      },
      formatted_updated: {
        label: 'Обновлено',
        placeholder: 'Обновлено'
      },
      code: {
        label: 'Код',
        placeholder: 'Код'
      },
      is_published: {
        label: 'Опубликовано',
        placeholder: 'Опубликовано'
      },
      is_published_on_geoportal: {
        label: 'Опубликовано на геопортале',
        placeholder: 'Опубликовано на геопортале'
      },
      location: {
        label: 'Локация',
        placeholder: 'Локация'
      },
      kind: {
        label: 'Тип',
        placeholder: 'Тип'
      },
      address: {
        label: 'Адрес',
        placeholder: 'Адрес'
      },
      address_more: {
        label: 'Дополнительное описание местонахождения',
        placeholder: 'Дополнительное описание местонахождения'
      },
      number_of_dead: {
        label: 'Кол-во захороненных',
        placeholder: 'Кол-во захороненных'
      },
      number_of_dead_known: {
        label: 'Кол-во известных захороненных',
        placeholder: 'Кол-во известных захороненных'
      },
      number_of_dead_unknown: {
        label: 'Кол-во неизвестных захороненных',
        placeholder: 'Кол-во неизвестных захороненных'
      },
      build_date: {
        label: 'Дата постройки',
        placeholder: 'Дата постройки'
      },
      history_info: {
        label: 'Краткая историческая справка',
        placeholder: 'Краткая историческая справка'
      },
      owner: {
        label: 'Собственник',
        placeholder: 'Собственник'
      },
      owner_address: {
        label: 'Адрес собственника',
        placeholder: 'Адрес собственника'
      },
      responsible_person_1: {
        label: 'Контактное лицо 1',
        placeholder: 'Контактное лицо 1'
      },
      responsible_person_phone_1: {
        label: 'Тел. 1',
        placeholder: 'Тел. 1'
      },
      responsible_person_email_1: {
        label: 'E-mail 1',
        placeholder: 'E-mail 1'
      },
      responsible_person_2: {
        label: 'Контактное лицо 2',
        placeholder: 'Контактное лицо 2'
      },
      responsible_person_phone_2: {
        label: 'Тел. 2',
        placeholder: 'Тел. 2'
      },
      responsible_person_email_2: {
        label: 'E-mail 2',
        placeholder: 'E-mail 2'
      },
    },
    user: {
      username: {
        label: 'Имя пользователя',
        placeholder: 'Имя пользователя',
      },
      email: {
        label: 'Электронный адресс',
        placeholder: 'Электронный адресс',
      },
      first_name: {
        label: 'Имя',
        placeholder: 'Имя',
      },
      last_name: {
        label: 'Фамилия',
        placeholder: 'Фамилия',
      },
      types: {
        label: 'Тип пользователя',
        placeholder: 'Тип пользователя',
      },
      locations: {
        label: 'Локация',
        placeholder: 'Локация',
      },
      position: {
        label: 'Должность',
        placeholder: 'Должность'
      },
      organization: {
        label: 'Организация',
        placeholder: 'Организация'
      },
      phone: {
        label: 'Телефон',
        placeholder: 'Телефон'
      },
    }
  },
  users: {
    title: 'Пользователи',
  },
  monuments: {
    title: 'Памятники',
    empty: 'По вашему запросу ничего не нашлось'
  }
};
