import markerIcon from 'leaflet/dist/images/marker-icon.png';
import markerShdow from 'leaflet/dist/images/marker-shadow.png';
import markerIcon2x from 'leaflet/dist/images/marker-icon-2x.png';

export const date = 'DD.MM.YYYY';
export const dateTimeServer = 'YYYY-MM-DD HH:mm';
export const dateTime = 'DD.MM.YYYY HH:mm';
export const special_kinds = [1, 4, 11, 12];
export const mapSettings = {
  sleep: true,
  sleepTime: 750,
  wakeTime: 750,
  sleepNote: false,
  hoverToWake: true,
  sleepOpacity: 0.9,
  center: [63.025882, 100.564249],
  zoom: 3
};

export const tileUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';

export const iconSettings = {
  iconUrl: markerIcon,
  iconRetinaUrl: markerIcon2x,
  shadowUrl: markerShdow,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
};

export const mediaSettings = {
  thumbnail_name: '100x100',
  src_name: '600'
};

export default {
  special_kinds,
  mediaSettings,
  mapSettings,
  tileUrl,
  iconSettings,
  formats: {
    date,
    dateTime,
    dateTimeServer
  }
};
