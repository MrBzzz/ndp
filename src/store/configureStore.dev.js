import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import createMiddleware from 'middleware/clientMiddleware';
import rootReducer from 'reducers/reducers';
import Api from 'helpers/Api';

const api = new Api();
const middleware = [createMiddleware(api)];

export default (initialState) => createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware, thunk)
);
