var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

var settings = require('./settings.js');

module.exports = {
  devtool: 'eval',
  entry: {
    main: [
      './src/index.jsx'
    ]
  },
  output: {
    path: path.join(__dirname, 'static'),
    filename: '[name]-[hash].js',
    publicPath: settings.publicPath
  },
  resolve: {
    root: path.join(__dirname, 'src'),
    extensions: ['', '.js', '.jsx'],
    modulesDirectories: [
      'node_modules',
      'src'
    ]
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, loaders: ['babel'], exclude: /node_modules/ },
      { test: /\.less$/, loader: ExtractTextPlugin.extract('style', 'css!postcss!less') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!postcss') },
      { test: /\.(jpe?g|png|gif)$/i, loaders: ['file'] },
      { test: /\.ico$/, loaders: ['file?name=[name].[ext]'] },
      { test: /\.svg$/, loaders: ['raw'] }
    ]
  },
  postcss: function () {
    return [
      autoprefixer({browsers: ['last 2 version']})
    ];
  },
  plugins: [
    new CleanPlugin(['static']),
    new ExtractTextPlugin('[name]-[chunkhash].css'),
    new HtmlWebpackPlugin({
      template: path.join('template', 'index.ejs'),
      title: settings.title,
      description: settings.description
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
};
