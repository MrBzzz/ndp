var path = require('path');
var webpack = require('webpack');
var express = require('express');
var proxy = require('proxy-middleware');
var url = require('url');

var settings = require('./settings.js');
var webpackConfig = require('./webpack.dev.config.js');

const compiler = webpack(webpackConfig);
const serverOptions = {
  publicPath: settings.publicPath,
  inline: true,
  quiet: false,
  noInfo: true,
  lazy: false,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  stats: {
    colors: true
  }
};

const app = new express();

app.use(settings.url.login, proxy(url.parse(settings.api + settings.url.login)));
app.use(settings.url.logout, proxy(url.parse(settings.api + settings.url.logout)));
app.use(settings.url.api, proxy(url.parse(settings.api + settings.url.api)));
app.use(settings.url.media, proxy(url.parse(settings.api + settings.url.media)));
app.use(settings.url.register, proxy(url.parse(settings.api + settings.url.register)));
app.use(settings.url.static, proxy(url.parse(settings.api + settings.url.static)));

app.use(require('webpack-hot-middleware')(compiler));
app.use(require('webpack-dev-middleware')(compiler, serverOptions));

app.use('*', (req, res, next) => {
  var filename = path.join(compiler.outputPath,'index.html');
  compiler.outputFileSystem.readFile(filename, (err, result) => {
    if (err) { return next(err); }
    res.set('content-type','text/html');
    res.send(result);
    res.end();
  });
});

app.listen(settings.port, (err) => {
  if (err) { console.error(err); return; }
  console.info('==> Webpack development server listening on port %s', settings.port);
});
